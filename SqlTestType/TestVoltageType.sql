INSERT INTO at_project_schema."testVoltage_type" (id, code, name) VALUES 
('44856915-8776-4d10-8b4f-d3088a9d5a04', 'InsulationResistance', 'Insulation resistance'),
('9c1711f2-16df-4191-9c0c-8abb536ddda6', 'VTRatio', 'VT Ratio'),
('03debd67-9beb-42ba-84a6-357a70ad3cb4', 'DcWindingRes', 'DC Winding Resistance'),
('af9462ca-a790-46c3-ba87-40a98825e71e', 'VTDfcap', 'VT DF & CAP'),
('747f6095-1b8a-4cc0-b063-e12d4912a1a4', 'GeneralInspection', 'General inspection')