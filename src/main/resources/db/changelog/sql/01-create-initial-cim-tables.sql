CREATE EXTENSION IF NOT EXISTS "uuid-ossp" with schema public;
SET search_path to at_project_schema;

/*Datatype*/
CREATE TABLE "voltage"
(
    "id"         UUID PRIMARY KEY default public.uuid_generate_v4() unique,
    "value"      float,
    "unit"       varchar(100),
    "multiplier" varchar(100)
) TABLESPACE pg_default;

CREATE TABLE "angle_degree"
(
    "id"         UUID PRIMARY KEY default public.uuid_generate_v4() unique,
    "value"      float,
    "unit"       varchar(100),
    "multiplier" varchar(100)
) TABLESPACE pg_default;

CREATE TABLE "per_cent"
(
    "id"         UUID PRIMARY KEY default public.uuid_generate_v4() unique,
    "value"      float,
    "unit"       varchar(100),
    "multiplier" varchar(100)
) TABLESPACE pg_default;

CREATE TABLE "current_flow"
(
    "id"         UUID PRIMARY KEY default public.uuid_generate_v4() unique,
    "value"      float,
    "unit"       varchar(100),
    "multiplier" varchar(100)
) TABLESPACE pg_default;

CREATE TABLE "frequency"
(
    "id"         UUID PRIMARY KEY default public.uuid_generate_v4() unique,
    "value"      float,
    "unit"       varchar(100),
    "multiplier" varchar(100)
) TABLESPACE pg_default;

CREATE TABLE "apparent_power"
(
    "id"         UUID PRIMARY KEY default public.uuid_generate_v4() unique,
    "value"      float,
    "unit"       varchar(100),
    "multiplier" varchar(100)
) TABLESPACE pg_default;

CREATE TABLE "weight"
(
    "id"         UUID PRIMARY KEY default public.uuid_generate_v4() unique,
    "value"      float,
    "unit"       varchar(100),
    "multiplier" varchar(100)
) TABLESPACE pg_default;

CREATE TABLE "temperature"
(
    "id"         UUID PRIMARY KEY default public.uuid_generate_v4() unique,
    "value"      float,
    "unit"       varchar(100),
    "multiplier" varchar(100)
) TABLESPACE pg_default;

CREATE TABLE "impedance"
(
    "id"         UUID PRIMARY KEY default public.uuid_generate_v4() unique,
    "value"      float,
    "unit"       varchar(100)     default 'ohm',
    "multiplier" varchar(100)
) TABLESPACE pg_default;

CREATE TABLE "kilo_active_power"
(
    "id"         UUID PRIMARY KEY default public.uuid_generate_v4() unique,
    "value"      float,
    "unit"       varchar(100)     default 'W',
    "multiplier" varchar(100)     default 'k'
) TABLESPACE pg_default;

CREATE TABLE "resistance"
(
    "id"         UUID PRIMARY KEY default public.uuid_generate_v4() unique,
    "value"      float,
    "unit"       varchar(100)     default 'ohm',
    "multiplier" varchar(100)
) TABLESPACE pg_default;

CREATE TABLE "money"
(
    "id"         UUID PRIMARY KEY default public.uuid_generate_v4() unique,
    "value"      decimal,
    "unit"       varchar(100),
    "multiplier" varchar(100)
) TABLESPACE pg_default;

/*Compound*/
CREATE TABLE "status"
(
    "id"        UUID PRIMARY KEY default public.uuid_generate_v4() unique,
    "value"     varchar(255),
    "date_time" timestamp,
    "remark"    varchar(255),
    "reason"    varchar(255)
) TABLESPACE pg_default;

CREATE TABLE "street_detail"
(
    "id"                 UUID PRIMARY KEY default public.uuid_generate_v4() unique,
    "number"             varchar(255),
    "name"               varchar(255),
    "suffix"             varchar(255),
    "prefix"             varchar(255),
    "type"               varchar(255),
    "code"               varchar(255),
    "building_name"      varchar(255),
    "suite_number"       varchar(255),
    "address_general"    varchar(255),
    "within_town_limits" boolean
) TABLESPACE pg_default;

CREATE TABLE "acceptance_test"
(
    "id"        UUID PRIMARY KEY default public.uuid_generate_v4() unique,
    "type"      varchar(255),
    "success"   boolean,
    "date_time" timestamp
) TABLESPACE pg_default;

CREATE TABLE "life_cycle_date"
(
    "id"                UUID PRIMARY KEY default public.uuid_generate_v4() unique,
    "manufactured_date" Date,
    "purchase"          Date,
    "received_date"     Date,
    "installation_date" Date,
    "removal_date"      Date,
    "retired_date"      Date
) TABLESPACE pg_default;

CREATE TABLE "telephone_number"
(
    "id"           UUID PRIMARY KEY default public.uuid_generate_v4() unique,
    "country_code" varchar(255),
    "area_code"    varchar(255),
    "city_code"    varchar(255),
    "local_number" varchar(255),
    "extension"    varchar(255)
) TABLESPACE pg_default;

CREATE TABLE "electronic_address"
(
    "id"       UUID PRIMARY KEY default public.uuid_generate_v4() unique,
    "lan"      varchar(255),
    "mac"      varchar(255),
    "email1"   varchar(255),
    "email2"   varchar(255),
    "radio"    varchar(255),
    "user_id"  varchar(255),
    "password" varchar(255)
) TABLESPACE pg_default;

CREATE TABLE "town_detail"
(
    "id"                UUID PRIMARY KEY default public.uuid_generate_v4() unique,
    "code"              varchar(255),
    "section"           varchar(255),
    "name"              varchar(255),
    "state_or_province" varchar(255),
    "country"           varchar(255)
) TABLESPACE pg_default;

CREATE TABLE "street_address"
(
    "id"            UUID PRIMARY KEY default public.uuid_generate_v4() unique,
    "street_detail" UUID REFERENCES "street_detail" ("id"),
    "town_detail"   UUID REFERENCES "town_detail" ("id"),
    "status"        UUID REFERENCES "status" ("id")
) TABLESPACE pg_default;

/*Object*/
CREATE TABLE "identified_object"
(
    "alias_name"  varchar(255),
    "description" varchar(255),
    "mrid"        UUID PRIMARY KEY default public.uuid_generate_v4() unique,
    "name"        varchar(255),
    "created_by"  uuid                                       not null,
    "created_on"  TIMESTAMP        default CURRENT_TIMESTAMP not null,
    "updated_by"  uuid,
    "updated_on"  TIMESTAMP,
    "is_deleted"  BOOL             default 'f'               not null
) TABLESPACE pg_default;
--TranformerTest
CREATE TABLE "transformer_test"
(
    "base_power"  UUID REFERENCES "apparent_power" ("id"),
    "temperature" UUID REFERENCES "temperature" ("id")
) inherits ("identified_object")
  TABLESPACE pg_default;
ALTER TABLE "transformer_test"
    ADD PRIMARY KEY ("mrid");

CREATE TABLE "short_circuit_test"
(
    "energised_end_step"     Integer,
    "grounded_end_step"      Integer,
    "leakage_impedance"      UUID REFERENCES "impedance" ("id"),
    "leakage_impedance_zero" UUID REFERENCES "impedance" ("id"),
    "loss"                   UUID REFERENCES "kilo_active_power" ("id"),
    "loss_zero"              UUID REFERENCES "kilo_active_power" ("id")
) inherits ("transformer_test")
  TABLESPACE pg_default;
ALTER TABLE "short_circuit_test"
    ADD PRIMARY KEY ("mrid");

CREATE TABLE "open_circuit_test"
(
    "energised_end_step"    Integer,
    "open_end_step"         Integer,
    "energised_end_voltage" UUID REFERENCES "voltage" ("id"),
    "open_end_voltage"      UUID REFERENCES "voltage" ("id"),
    "phase_shift"           UUID REFERENCES "angle_degree" ("id")
) inherits ("transformer_test")
  TABLESPACE pg_default;
ALTER TABLE "open_circuit_test"
    ADD PRIMARY KEY ("mrid");

CREATE TABLE "no_load_test"
(
    "exciting_current"      UUID REFERENCES "per_cent" ("id"),
    "exciting_current_zero" UUID REFERENCES "per_cent" ("id"),
    "loss"                  UUID REFERENCES "kilo_active_power" ("id"),
    "loss_zero"             UUID REFERENCES "kilo_active_power" ("id"),
    "energised_end_voltage" UUID REFERENCES "voltage" ("id")
) inherits ("transformer_test")
  TABLESPACE pg_default;
ALTER TABLE "no_load_test"
    ADD PRIMARY KEY ("mrid");

--Asset
CREATE TABLE "asset_info"
(
) inherits ("identified_object")
  TABLESPACE pg_default;
ALTER TABLE "asset_info"
    ADD PRIMARY KEY ("mrid");

CREATE TABLE "asset_model"
(
) inherits ("identified_object")
  TABLESPACE pg_default;
ALTER TABLE "asset_model"
    ADD PRIMARY KEY ("mrid");

CREATE TABLE "manufacturer"
(
) inherits ("identified_object")
  TABLESPACE pg_default;
ALTER TABLE "manufacturer"
    ADD PRIMARY KEY ("mrid");

CREATE TABLE "product_asset_model"
(
    "model_number"            varchar(255),
    "model_version"           varchar(255),
    "corporate_standard_kind" varchar(255),
    "usage_kind"              varchar(255),
    "weight_total"            UUID REFERENCES "weight" ("id")
) inherits ("asset_model")
  TABLESPACE pg_default;
ALTER TABLE "product_asset_model"
    ADD PRIMARY KEY ("mrid");

CREATE TABLE "transformer_end_info"
(
    "end_number"        Integer,
    "phase_angle_clock" Integer,
    "connection_kind"   varchar(255),
    "r"                 UUID REFERENCES "resistance" ("id"),
    "rated_u"           UUID REFERENCES "voltage" ("id"),
    "insulation_u"      UUID REFERENCES "voltage" ("id"),
    "rated_s"           UUID REFERENCES "apparent_power" ("id"),
    "emergency_s"       UUID REFERENCES "apparent_power" ("id"),
    "short_term_s"      UUID REFERENCES "apparent_power" ("id")
) inherits ("asset_info")
  TABLESPACE pg_default;
ALTER TABLE "transformer_end_info"
    ADD PRIMARY KEY ("mrid");


CREATE TABLE "position_point"
(
    "sequence_number" Integer,
    "x_position"      varchar(255),
    "y_position"      varchar(255),
    "z_position"      varchar(255)
) inherits ("identified_object")
  TABLESPACE pg_default;
ALTER TABLE "position_point"
    ADD PRIMARY KEY ("mrid");

CREATE TABLE "power_transformer_info"
(
) inherits ("asset_info")
  TABLESPACE pg_default;
ALTER TABLE "power_transformer_info"
    ADD PRIMARY KEY ("mrid");

CREATE TABLE "transformer_tank_info"
(
) inherits ("asset_info")
  TABLESPACE pg_default;
ALTER TABLE "transformer_tank_info"
    ADD PRIMARY KEY ("mrid");

CREATE TABLE "asset"
(
    "type"                 varchar(255),
    "utc_number"           varchar(255),
    "serial_number"        varchar(255),
    "lot_number"           varchar(255),
    "purchase_price"       UUID REFERENCES "money"("id"),
    "critical"             boolean,
    "electronic_address"   UUID REFERENCES "electronic_address" ("id"),
    "life_cycle"           UUID REFERENCES "life_cycle_date" ("id"),
    "acceptance_test"      UUID REFERENCES "acceptance_test" ("id"),
    "initial_condition"    varchar(255),
    "initial_loss_of_life" UUID REFERENCES "per_cent" ("id"),
    "status"               UUID REFERENCES "status" ("id")
) inherits ("identified_object")
  TABLESPACE pg_default;
ALTER TABLE "asset"
    ADD PRIMARY KEY ("mrid");

CREATE TABLE "activity_record"
(
    "created_date_time" timestamp,
    "type"              varchar(255),
    "reason"            varchar(255),
    "severity"          varchar(255),
    "status"            UUID REFERENCES "status" ("id")
) inherits ("identified_object")
  TABLESPACE pg_default;
ALTER TABLE "activity_record"
    ADD PRIMARY KEY ("mrid");

CREATE TABLE "power_system_resource"
(
) inherits ("identified_object")
  TABLESPACE pg_default;
ALTER TABLE "power_system_resource"
    ADD PRIMARY KEY ("mrid");

CREATE TABLE "configuration_event"
(
    "effective_date_time" timestamp,
    "modified_by"         varchar(255),
    "remark"              varchar(255)
) inherits ("activity_record")
  TABLESPACE pg_default;
ALTER TABLE "configuration_event"
    ADD PRIMARY KEY ("mrid");

CREATE TABLE "coordinate_system"
(
    "crs_urn" varchar(255)
) inherits ("identified_object")
  TABLESPACE pg_default;
ALTER TABLE "coordinate_system"
    ADD PRIMARY KEY ("mrid");

CREATE TABLE "tap_changer_info"
(
    "is_tcul"                boolean,
    "pt_ratio"               float,
    "ct_radio"               float,
    "ct_rating"              UUID REFERENCES "current_flow" ("id"),
    "neutral_u"              UUID REFERENCES "voltage" ("id"),
    "neutral_step"           Integer,
    "high_step"              Integer,
    "low_step"               Integer,
    "step_voltage_increment" UUID REFERENCES "per_cent" ("id"),
    "step_phase_increment"   UUID REFERENCES "angle_degree" ("id"),
    "rated_voltage"          UUID REFERENCES "voltage" ("id"),
    "rated_apparent_power"   UUID REFERENCES "apparent_power" ("id"),
    "rated_current"          UUID REFERENCES "current_flow" ("id"),
    "bil"                    UUID REFERENCES "voltage" ("id"),
    "frequency"              UUID REFERENCES "frequency" ("id")
) inherits ("asset_info")
  TABLESPACE pg_default;
ALTER TABLE "tap_changer_info"
    ADD PRIMARY KEY ("mrid");


CREATE TABLE "location"
(
    "type"               varchar(255),
    "main_address"       UUID REFERENCES "street_address" ("id"),
    "secondary_address"  UUID REFERENCES "street_address" ("id"),
    "phone1"             UUID REFERENCES "telephone_number" ("id"),
    "phone2"             UUID REFERENCES "telephone_number" ("id"),
    "electronic_address" UUID REFERENCES "electronic_address" ("id"),
    "geo_info_reference" varchar(255),
    "direction"          varchar(255),
    "status"             UUID REFERENCES "status"("id")
) inherits ("identified_object")
  TABLESPACE pg_default;
ALTER TABLE "location"
    ADD PRIMARY KEY ("mrid");




