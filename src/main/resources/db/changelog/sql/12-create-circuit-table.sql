CREATE TABLE "testCircuit_type"
(
    "id"   UUID default public.uuid_generate_v4(),
    "code" TEXT,
    "name" TEXT,
    PRIMARY KEY ("id")
);

CREATE TABLE "circuit_breaker"
(
    "id"                 UUID NOT NULL,
    "location_id"        UUID NOT NULL default public.uuid_generate_v4(),
    "properties"         TEXT,
    "circuit_breaker"    TEXT,
    "ratings"            TEXT,
    "contact_sys"        TEXT,
    "others"             TEXT,
    "operating"          TEXT,
    "assessment_limits"  TEXT,
    "extend"             TEXT,
    "asset"              TEXT,
    "asset_type"         TEXT,
    "serial_no"          TEXT,
    "manufacturer"       TEXT,
    "manufacturer_type"  TEXT,
    "manufacturing_year" TEXT,
    "asset_system_code"  TEXT,
    "apparatus_id"       TEXT,
    "feeder"             TEXT,
    "created_by"         UUID,
    "created_on"         timestamp,
    "updated_by"         UUID,
    "updated_on"         timestamp,
    "locked"             boolean       default false,
    "locked_by"          UUID          default NULL,
    "collabs"            TEXT          default '',
    "is_deleted"         boolean       default false,
    FOREIGN KEY ("location_id") REFERENCES "location" ("mrid") ON DELETE CASCADE,
    PRIMARY KEY ("id")
);

CREATE TABLE "jobdata"
(
    "id"                   UUID NOT NULL,
    "asset_id"             UUID,
    "name"                 TEXT,
    "work_order"           TEXT,
    "creation_date"        TEXT,
    "execution_date"       TEXT,
    "tested_by"            TEXT,
    "approved_by"          TEXT,
    "approval_date"        TEXT,
    "summary"              TEXT,
    "ambient_condition"    TEXT,
    "testing_method"       TEXT,
    "standard"             TEXT,
    "average_health_index" REAL,
    "worst_health_index"   REAL,
    "created_by"           UUID,
    "created_on"           timestamp,
    "updated_by"           UUID,
    "updated_on"           timestamp,
    "locked"               boolean default false,
    "locked_by"            UUID    default NULL,
    "collabs"              TEXT    default '',
    "is_deleted"         boolean       default false,
    PRIMARY KEY ("id"),
    FOREIGN KEY ("asset_id") REFERENCES "circuit_breaker" ("id") ON DELETE CASCADE
);

CREATE TABLE "testdatas"
(
    "id"                  UUID NOT NULL,
    "type_id"             UUID NOT NULL,
    "job_id"              UUID NOT NULL,
    "name"                TEXT,
    "data"                TEXT,
    "average_score"       REAL,
    "worst_score"         REAL,
    "total_average_score" REAL,
    "total_worst_score"   REAL,
    "weighting_factor"    REAL,
    "average_score_df"    REAL,
    "average_score_c"     REAL,
    "worst_score_df"      REAL,
    "worst_score_c"       REAL,
    "weighting_factor_df" REAL,
    "weighting_factor_c"  REAL,
    "created_by"          UUID,
    "created_on"          timestamp,
    "updated_by"          UUID,
    "updated_on"          timestamp,
    "locked"              boolean default false,
    "locked_by"           UUID    default NULL,
    "collabs"             TEXT    default '',
    "is_deleted"         boolean       default false,
    FOREIGN KEY ("type_id") REFERENCES "testCircuit_type" ("id") ON DELETE CASCADE,
    FOREIGN KEY ("job_id") REFERENCES "jobdata" ("id") ON DELETE CASCADE,
    PRIMARY KEY ("id")
);

