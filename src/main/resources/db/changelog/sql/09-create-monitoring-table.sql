SET
search_path TO at_project_schema;
CREATE
EXTENSION IF NOT EXISTS "uuid-ossp" with schema public;

CREATE TABLE "monitoring"
(
    "id"                  UUID      default public.uuid_generate_v4(),
    "asset_mrid"          UUID REFERENCES "asset" ("mrid") on delete cascade,
    "ageing_insulation"   TEXT,
    "moisture_insulation" TEXT,
    "bushings_online"     TEXT,
    "patital_discharge"   TEXT,
    "dga"   TEXT,

    "bushing_df_worst"    TEXT,
    "bushing_df_average"  TEXT,
    "bushing_c_worst"     TEXT,
    "bushing_c_average"   TEXT,
    "condition_mois"      TEXT,
    "health_index"        TEXT,
    "weight_bushing_df"   TEXT,
    "weight_bushing_c"    TEXT,
    "weight_mois"         TEXT,

    "created_by"          uuid                                not null,
    "created_on"          TIMESTAMP default CURRENT_TIMESTAMP not null,
    "updated_by"          uuid,
    "updated_on"          TIMESTAMP,
    "is_deleted"          BOOL      default 'f'               not null,
    PRIMARY KEY ("id")
) TABLESPACE pg_default;

