SET search_path TO at_project_schema;
CREATE EXTENSION IF NOT EXISTS "uuid-ossp" with schema public;

CREATE TABLE IF NOT EXISTS "fmeca"
(
    "id"              UUID      default public.uuid_generate_v4(),
    "table_fmeca"     TEXT,
    "table_calculate" TEXT,
    "total"           TEXT,

    "created_by"      uuid                                not null,
    "created_on"      TIMESTAMP default CURRENT_TIMESTAMP not null,
    "updated_by"      uuid,
    "updated_on"      TIMESTAMP,
    "is_deleted"      BOOL      default 'f'               not null,
    PRIMARY KEY ("id")
) TABLESPACE pg_default;

ALTER TABLE "job"
ADD COLUMN IF NOT EXISTS "average_health_index" float default 0,
ADD COLUMN IF NOT EXISTS "worst_health_index" float default 0;

ALTER TABLE "test"
ADD COLUMN IF NOT EXISTS "average_score" float default 0,
ADD COLUMN IF NOT EXISTS "worst_score" float default 0,
ADD COLUMN IF NOT EXISTS "worst_score_df" float default 0,
ADD COLUMN IF NOT EXISTS "worst_score_c" float default 0,
ADD COLUMN IF NOT EXISTS "average_score_df" float default 0,
ADD COLUMN IF NOT EXISTS "average_score_c" float default 0,
ADD COLUMN IF NOT EXISTS "create_at_client" bigint default 0;

