package vn.automationandtesting.atproject.controller.circuit;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import vn.automationandtesting.atproject.config.auth.BearerContextHolder;
import vn.automationandtesting.atproject.controller.dto.ResponseObject;
import vn.automationandtesting.atproject.controller.dto.circuit.CircuitDto;
import vn.automationandtesting.atproject.controller.dto.circuit.JobCircuitDto;
import vn.automationandtesting.atproject.controller.dto.mapper.circuit.CircuitMapper;
import vn.automationandtesting.atproject.controller.dto.mapper.circuit.JobCircuitMapper;
import vn.automationandtesting.atproject.entity.circuit.Circuit;
import vn.automationandtesting.atproject.entity.circuit.Jobdata;
import vn.automationandtesting.atproject.service.circuit.CircuitService;
import vn.automationandtesting.atproject.service.circuit.JobCircuitService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

@Controller
@RequestMapping("${api.prefix}")
public class JobCircuitController {

    @Autowired
    private JobCircuitService service;

    @Autowired
    private CircuitService AssetService;

    @Autowired
    private CircuitMapper AssetMapper;

    @Autowired
    private JobCircuitMapper mapper;

    @GetMapping("/circuit/job/findAllJobByAssetId/{asset_id}")
    public ResponseEntity<?> findAllJobByAssetId(@PathVariable String asset_id) {
        UUID id = UUID.fromString(asset_id);
        List<JobCircuitDto> dtoList = service.findAllJobByAssetId(id);
        ResponseObject responseObject = new ResponseObject(true, "Get by asset id", dtoList);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/circuit/job/findJobById/{id}")
    public ResponseEntity<?> findJobById(@PathVariable String id) {
        UUID uuid = UUID.fromString(id);
        List<JobCircuitDto> dtoList = service.findJobById(uuid);
        ResponseObject responseObject = new ResponseObject(true, "Get by ID", dtoList);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/circuit/job/findJobByName/{name}")
    public ResponseEntity<?> findJobByName(@PathVariable String name) {
        List<JobCircuitDto> dtoList = service.findJobByName(name);
        ResponseObject responseObject = new ResponseObject(true, "Get by name", dtoList);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/circuit/job/findJobByNameAndAsset/{name}/{asset_id}")
    public ResponseEntity<?> findJobByNameAndAsset(@PathVariable(name = "name") String name, @PathVariable(name = "asset_id") String asset_id) {
        UUID id = UUID.fromString(asset_id);
        List<JobCircuitDto> dtoList = service.findJobByNameAndAsset(name, id);
        ResponseObject responseObject = new ResponseObject(true, "Get by name and asset id", dtoList);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/circuit/job/findAll")
    public ResponseEntity<?> findAll() {
        List<JobCircuitDto> dtoList = service.findAll();
        ResponseObject responseObject = new ResponseObject(true, "Get all", dtoList);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/circuit/job/findByAssetIdAndCollab/{assetId}")
    public ResponseEntity<?> findByLocationIdAndCollab(@PathVariable String assetId) {
        String userId = BearerContextHolder.getContext().getUserId();
        UUID asset_id = UUID.fromString(assetId);
        List<JobCircuitDto> jobDtoList = service.findByAssetIdAndCollabsContaining(asset_id, userId);
        ResponseObject responseObject = new ResponseObject(true, "Get job by asset id and collab", jobDtoList);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @PostMapping("/circuit/job/lock/{sign}/{id}")
    public ResponseEntity<?> lockAsset(@PathVariable(name = "sign") boolean sign, @PathVariable(name = "id") String id) {
        JobCircuitDto job = service.findJobById(UUID.fromString(id)).get(0);
        List<Jobdata> assetItem = new ArrayList<>();
        String userId = BearerContextHolder.getContext().getUserId();
        if(sign) {
            job.setLocked(true);
            job.setLocked_by(UUID.fromString(userId));
            Jobdata item = mapper.JobDtoToJob(job);
            item.setCollabs(job.getCollabs());
            assetItem.add(item);
            service.saveAll(assetItem);
        } else {
            if(job.getLocked_by().toString().equals(userId)) {
                job.setLocked(false);
                job.setLocked_by(UUID.fromString("00000000-0000-0000-0000-000000000000"));
                Jobdata item = mapper.JobDtoToJob(job);
                item.setCollabs(job.getCollabs());
                assetItem.add(item);
                service.saveAll(assetItem);
            }
        }
        ResponseObject responseObject = new ResponseObject(true, "sign lock job", true);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @PostMapping("/circuit/job/share/{jobId}")
    public ResponseEntity<?> share(@RequestBody List<String> userIds, @PathVariable String jobId) {
        JobCircuitDto jobDto = service.findJobById(UUID.fromString(jobId)).get(0);
        CircuitDto assetDto = AssetService.findAssetById(jobDto.getAsset_id()).get(0);
        String collabAsset = assetDto.getCollabs();
        List<String> collabAssetData = new ArrayList<>();
        if(collabAsset != null) {
            collabAssetData = Arrays.asList(collabAsset.split(","));
            collabAssetData = new ArrayList<>(collabAssetData);
        }
        for (String user : userIds) {
            if(!collabAssetData.contains(user)) {
                collabAssetData.add(user);
            }
        }
        collabAsset = String.join(",", collabAssetData);
        List<Circuit> assets = new ArrayList<>();
        Circuit asset = AssetMapper.circuitDtoToCircuit(assetDto);
        asset.setCollabs(collabAsset);
        assets.add(asset);
        AssetService.saveAll(assets);

        String collab = jobDto.getCollabs();
        List<String> collabData = new ArrayList<>();
        if(collab != null) {
            collabData = Arrays.asList(collab.split(","));
            collabData = new ArrayList<>(collabData);
        }
        for (String user : userIds) {
            if(!collabData.contains(user)) {
                collabData.add(user);
            }
        }
        collab = String.join(",", collabData);
        List<Jobdata> jobs = new ArrayList<>();
        Jobdata job = mapper.JobDtoToJob(jobDto);
        job.setCollabs(collab);
        jobs.add(job);
        service.saveAll(jobs);
        ResponseObject responseObject = new ResponseObject(true, "share job", true);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @PostMapping("/circuit/job/save")
    public ResponseEntity<?> insertJob(@RequestBody List<JobCircuitDto> jobDtoList) {
        List<Jobdata> jobs = new ArrayList<>();
        for(JobCircuitDto item : jobDtoList) {
            List<JobCircuitDto> itemDtoList = service.findJobById(item.getId());
            Jobdata job = new Jobdata();
            if(itemDtoList.size() == 0) {
                job = mapper.JobDtoToJob(item);
            } else {
                job = mapper.copyJobDtoToJob(itemDtoList.get(0), job);
                job = mapper.copyJobDtoToJob(item, job, "createdOn", "createdBy", "locked", "collabs");
            }
            System.out.println(job.getAverage_health_index());
            if(!job.isLocked()) {
                jobs.add(job);
            }
        }
        if(jobs.size() != 0) {
            service.saveAll(jobs);
        }
        ResponseObject responseObject = new ResponseObject(true, "save circuit", true);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @PostMapping("/circuit/job/delete")
    public ResponseEntity<?> deleteJob(@RequestBody List<JobCircuitDto> jobDtoList) {
        List<Jobdata> jobs = new ArrayList<>();
        for(JobCircuitDto item : jobDtoList) {
            List<JobCircuitDto> itemDto = service.findJobById(item.getId());
            if(!itemDto.get(0).isLocked()) {
                Jobdata job = mapper.JobDtoToJob(item);
                jobs.add(job);
            }
        }
        if(jobs.size() != 0) {
            service.deleteAll(jobs);
        }
        ResponseObject responseObject = new ResponseObject(true, "delete job", true);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }
}
