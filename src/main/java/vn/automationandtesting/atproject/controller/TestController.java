package vn.automationandtesting.atproject.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import vn.automationandtesting.atproject.controller.dto.ResponseObject;
import vn.automationandtesting.atproject.controller.dto.cim.TestDto;
import vn.automationandtesting.atproject.service.TestService;

import java.util.List;
import java.util.UUID;

/**
 * @author tund on 7/9/2022
 * @project at-project-server
 */
@RestController
@RequestMapping("${api.prefix}")
public class TestController {
    private final TestService testService;

    @Autowired
    public TestController(TestService testService) {
        this.testService = testService;
    }

    @GetMapping("/testsByAssetSerialNo/{assetSerialNo}")
    public ResponseEntity<?> findTestsByAssetSerialNo(@PathVariable String assetSerialNo) {
        List<TestDto> testDtoList = testService.findAllTestsByAssetSerialNo(assetSerialNo);
        ResponseObject responseObject = new ResponseObject(true, "Get tests by asset serial number", testDtoList);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/testsByJobName/{jobName}")
    public ResponseEntity<?> findTestsByJobName(@PathVariable String jobName) {
        List<TestDto> testDtoList = testService.findAllTestsByJobName(jobName);
        ResponseObject responseObject = new ResponseObject(true, "Get tests by job name", testDtoList);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/testsByCodeType/{codeType}")
    public ResponseEntity<?> findTestsByCodeType(@PathVariable String codeType) {
        List<TestDto> testDtoList = testService.findAllTestsByCodeType(codeType);
        ResponseObject responseObject = new ResponseObject(true, "Get tests by code type", testDtoList);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/tests/all")
    public ResponseEntity<?> getAllTests() {
        List<TestDto> testDtoList = testService.getAllTests();
        ResponseObject responseObject = new ResponseObject(true, "Get all tests", testDtoList);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @PostMapping("/tests")
    public ResponseEntity<?> createTest(@RequestBody TestDto testDto) {
        TestDto savedTestDto = testService.createNewTest(testDto);
        ResponseObject responseObject = new ResponseObject(true, "Create test", savedTestDto);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @PostMapping("/tests/calculate")
    public ResponseEntity<?> calculate(@RequestBody List<TestDto> listTestDto) {
        ResponseObject responseObject = testService.calculate(listTestDto);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @PutMapping("/tests/{testId}")
    public ResponseEntity<?> updateTest(@RequestBody TestDto testDto, @PathVariable String testId) {
        UUID id = UUID.fromString(testId);
        TestDto updatedTestDto = testService.updateTest(testDto, id);
        ResponseObject responseObject = new ResponseObject(true, "Update test", updatedTestDto);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @DeleteMapping("/tests/{testId}")
    public ResponseEntity<?> deleteTest(@PathVariable String testId) {
        UUID id = UUID.fromString(testId);
        testService.deleteTest(id);
        ResponseObject responseObject = new ResponseObject(true, "Delete successfully", null);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }
}
