package vn.automationandtesting.atproject.controller.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import vn.automationandtesting.atproject.controller.dto.cim.LocationDto;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ResourceFullDto {
    private List<LocationDto> listLocation;
    private List<AssetFullDto> listAsset;
    private List<JobFullDto> listJob;
    private LocationDto location;
    private AssetFullDto asset;
    private JobFullDto job;
}
