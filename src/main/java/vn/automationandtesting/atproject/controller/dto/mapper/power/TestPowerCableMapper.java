package vn.automationandtesting.atproject.controller.dto.mapper.power;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;
import vn.automationandtesting.atproject.controller.dto.power.TestPowerCableDto;
import vn.automationandtesting.atproject.entity.power.TestsPowerCable;

@Component
public class TestPowerCableMapper {
    public TestPowerCableDto testToTestDto(TestsPowerCable test) {
        TestPowerCableDto testDto = new TestPowerCableDto();
        BeanUtils.copyProperties(test, testDto);
        return testDto;
    }

    public TestsPowerCable testDtoToTest(TestPowerCableDto testDto) {
        TestsPowerCable test = new TestsPowerCable();
        BeanUtils.copyProperties(testDto, test);
        return test;
    }

    public TestsPowerCable copyTestDtoToTest(TestPowerCableDto testDto, TestsPowerCable test, String... ignoredPropertyName) {
        BeanUtils.copyProperties(testDto, test, ignoredPropertyName);
        return test;
    }
}
