package vn.automationandtesting.atproject.controller.dto.mapper.surge;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;
import vn.automationandtesting.atproject.controller.dto.surge.SurgeDto;
import vn.automationandtesting.atproject.entity.surge.Surge;

import java.util.Arrays;


@Component
public class SurgeMapper {
    public SurgeDto assetToAssetDto(Surge asset) {
        SurgeDto assetDto = new SurgeDto();
        BeanUtils.copyProperties(asset, assetDto);
        return assetDto;
    }

    public Surge assetDtoToAsset(SurgeDto assetDto) {
        Surge asset = new Surge();
        BeanUtils.copyProperties(assetDto, asset, "collabs");
        return asset;
    }

    public Surge copyAssetDtoToAsset(SurgeDto assetDto, Surge asset, String... ignoreProperties) {
        // ignoredPropertyNames.add("collabs");
        BeanUtils.copyProperties(assetDto, asset, ignoreProperties);
        if (assetDto.getId() != null && !Arrays.asList(ignoreProperties).contains("id")) {
            asset.setId(assetDto.getId());
        }
        if (assetDto.getLocation_id() != null && !Arrays.asList(ignoreProperties).contains("location_id")) {
            asset.setLocation_id(assetDto.getLocation_id());
        }
        asset.setAsset_type(assetDto.getAsset_type());
        asset.setSerial_no(assetDto.getSerial_no());
//        asset.setCollabs(String.join(",", assetDto.getCollabs()));
        return asset;
    }
}
