package vn.automationandtesting.atproject.controller.dto.mapper;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;
import vn.automationandtesting.atproject.controller.dto.cim.FmecaDto;
import vn.automationandtesting.atproject.entity.Fmeca;

/**
 * @author tund on 7/9/2022
 * @project at-project-server
 */
@Component
public class FmecaMapper {
    public FmecaDto fmceToFmceDto(Fmeca fmeca) {
        FmecaDto fmecaDto = new FmecaDto();
        BeanUtils.copyProperties(fmeca, fmecaDto);
        fmecaDto.setTable_calculate(fmeca.getTableCalculate());
        fmecaDto.setTable_fmeca(fmeca.getTableFmeca());
        return fmecaDto;
    }

    public Fmeca fmecaDtoToFmeca(FmecaDto fmecaDto) {
        Fmeca fmeca = new Fmeca();
        BeanUtils.copyProperties(fmecaDto, fmeca);
        fmeca.setTableCalculate(fmecaDto.getTable_calculate());
        fmeca.setTableFmeca(fmecaDto.getTable_fmeca());
        return fmeca;
    }
}
