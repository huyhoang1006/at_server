package vn.automationandtesting.atproject.controller.dto.cim;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.UUID;

/**
 * @author tridv on 2/9/2022
 * @project at-project-server
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class TapChangerDto {
    private UUID id;
    private UUID asset_id;
    private String mode;
    private String serial_no;
    private String manufacturer;
    private String manufacturer_type;
    private String winding;
    private String tap_scheme;
    private Integer no_of_taps;
    private String voltage_table;
}
