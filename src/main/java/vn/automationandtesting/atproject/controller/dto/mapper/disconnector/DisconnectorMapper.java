package vn.automationandtesting.atproject.controller.dto.mapper.disconnector;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;
import vn.automationandtesting.atproject.controller.dto.disconnector.DisconnectorDto;
import vn.automationandtesting.atproject.entity.disconnector.Disconnector;

import java.util.Arrays;


@Component
public class DisconnectorMapper {
    public DisconnectorDto assetToAssetDto(Disconnector asset) {
        DisconnectorDto assetDto = new DisconnectorDto();
        BeanUtils.copyProperties(asset, assetDto);
        return assetDto;
    }

    public Disconnector assetDtoToAsset(DisconnectorDto assetDto) {
        Disconnector asset = new Disconnector();
        BeanUtils.copyProperties(assetDto, asset, "collabs");
        return asset;
    }

    public Disconnector copyAssetDtoToAsset(DisconnectorDto assetDto, Disconnector asset, String... ignoreProperties) {
        // ignoredPropertyNames.add("collabs");
        BeanUtils.copyProperties(assetDto, asset, ignoreProperties);
        if (assetDto.getId() != null && !Arrays.asList(ignoreProperties).contains("id")) {
            asset.setId(assetDto.getId());
        }
        if (assetDto.getLocation_id() != null && !Arrays.asList(ignoreProperties).contains("location_id")) {
            asset.setLocation_id(assetDto.getLocation_id());
        }
        asset.setAsset_type(assetDto.getAsset_type());
        asset.setSerial_no(assetDto.getSerial_no());
//        asset.setCollabs(String.join(",", assetDto.getCollabs()));
        return asset;
    }
}
