package vn.automationandtesting.atproject.controller.dto.mapper.circuit;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;
import vn.automationandtesting.atproject.controller.dto.circuit.TestCircuitDto;
import vn.automationandtesting.atproject.entity.circuit.TestCircuit;

@Component
public class TestCircuitMapper {
    public TestCircuitDto testToTestDto(TestCircuit testCircuit) {
        TestCircuitDto testCircuitDto = new TestCircuitDto();
        BeanUtils.copyProperties(testCircuit, testCircuitDto);
        return testCircuitDto;
    }

    public TestCircuit testDtoToTest(TestCircuitDto testCircuitDto) {
        TestCircuit testCircuit = new TestCircuit();
        BeanUtils.copyProperties(testCircuitDto, testCircuit);
        return testCircuit;
    }

    public TestCircuit copyTestDtoToTest(TestCircuitDto testCircuitDto, TestCircuit testCircuit, String... ignoredPropertyName) {
        BeanUtils.copyProperties(testCircuitDto, testCircuit, ignoredPropertyName);
        return testCircuit;
    }
}
