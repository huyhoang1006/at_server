package vn.automationandtesting.atproject.controller.dto.request;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UserReqDto {
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private UUID id;
    private String username;
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String password;
    private String firstName;
    private String lastName;
    private String gender;
    private String email;
    private String phone;
    @JsonFormat(pattern = "yyyy/MM/dd")
    private LocalDate birthDate;
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private String role;
    //    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private List<String> groups = new ArrayList<>();
}
