package vn.automationandtesting.atproject.controller.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResponseObject {
    private boolean success;
    private String message;
    private Object data;

    public static ResponseObject returnWithData(Object data) {
        return new ResponseObject(true, null, data);
    }

    public static ResponseObject returnWithError(String message) {
        return new ResponseObject(false, message, null);
    }
}
