package vn.automationandtesting.atproject.controller.dto.mapper;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;
import vn.automationandtesting.atproject.controller.dto.AttachmentDto;
import vn.automationandtesting.atproject.entity.Attachment;

import java.util.UUID;

@Component
public class AttachmentMapper {
    public Attachment copyAttachmentDtoToAttachment(AttachmentDto attachmentDto, Attachment attachment, String... ignoredPropertyName) {
        BeanUtils.copyProperties(attachmentDto, attachment, ignoredPropertyName);
        return attachment;
    }
    public Attachment AttachmentDtoToAttachment(AttachmentDto attachmentDto) {
        Attachment attachment = new Attachment();
        attachment.setId_foreign(UUID.fromString(attachmentDto.getId_foreign()));
        attachment.setId(UUID.fromString(attachmentDto.getId()));
        attachment.setName(attachmentDto.getName());
        attachment.setType(attachmentDto.getType());
        attachment.setPath(attachmentDto.getPath());
        return attachment;
    }
    public AttachmentDto copyAttachmentToAttachmentDto(Attachment attachment, AttachmentDto attachmentDto, String... ignoredPropertyName) {
        BeanUtils.copyProperties(attachment, attachmentDto, ignoredPropertyName);
        return attachmentDto;
    }
    public AttachmentDto AttachmentToAttachmentDto(Attachment attachment) {
        AttachmentDto attachmentDto = new AttachmentDto();
        attachmentDto.setId(attachment.getId().toString());
        attachmentDto.setId_foreign(attachment.getId_foreign().toString());
        attachmentDto.setType(attachment.getType());
        attachmentDto.setPath(attachment.getPath());
        attachmentDto.setName(attachment.getName());
        return attachmentDto;
    }
}
