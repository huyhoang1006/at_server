package vn.automationandtesting.atproject.controller.dto.mapper.surge;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;
import vn.automationandtesting.atproject.controller.dto.surge.TestSurgeDto;
import vn.automationandtesting.atproject.entity.surge.TestsSurge;

@Component
public class TestSurgeMapper {
    public TestSurgeDto testToTestDto(TestsSurge test) {
        TestSurgeDto testDto = new TestSurgeDto();
        BeanUtils.copyProperties(test, testDto);
        return testDto;
    }

    public TestsSurge testDtoToTest(TestSurgeDto testDto) {
        TestsSurge test = new TestsSurge();
        BeanUtils.copyProperties(testDto, test);
        return test;
    }

    public TestsSurge copyTestDtoToTest(TestSurgeDto testDto, TestsSurge test, String... ignoredPropertyName) {
        BeanUtils.copyProperties(testDto, test, ignoredPropertyName);
        return test;
    }
}
