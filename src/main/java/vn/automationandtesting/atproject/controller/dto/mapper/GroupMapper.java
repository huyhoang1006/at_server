package vn.automationandtesting.atproject.controller.dto.mapper;

import org.springframework.stereotype.Component;
import vn.automationandtesting.atproject.controller.dto.GroupDto;
import vn.automationandtesting.atproject.entity.Group;

import java.util.HashSet;
import java.util.Set;

@Component
public class GroupMapper {

    public Group groupDtoToGroup(GroupDto groupDto) {
        Group group = new Group(groupDto.getId(), groupDto.getDescription(), groupDto.getGroupName(), new HashSet<>());
        return group;
    }

    public GroupDto groupToGroupDto(Group group) {
        Set<String> permissions = new HashSet<>();
        group.getGroupPermissions().forEach(groupPermission ->
                permissions.add(groupPermission.getPermission().getPermissionName().name())
        );
        GroupDto groupDto = new GroupDto(group.getId(), group.getDescription(), group.getGroupName(), permissions);
        return groupDto;
    }


}
