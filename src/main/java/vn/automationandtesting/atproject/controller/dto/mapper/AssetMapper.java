package vn.automationandtesting.atproject.controller.dto.mapper;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;
import vn.automationandtesting.atproject.controller.dto.cim.AssetDto;
import vn.automationandtesting.atproject.entity.cim.Asset;

import java.util.Arrays;
import java.util.stream.Collectors;

/**
 * @author tridv on 3/9/2022
 * @project at-project-server
 */
@Component
public class AssetMapper {
    public AssetDto assetToAssetDto(Asset asset) {
        AssetDto assetDto = new AssetDto();
        BeanUtils.copyProperties(asset, assetDto, "status", "collabs");
        assetDto.setId(asset.getMrid());
        assetDto.setLocation_id(asset.getLocation().getMrid());
        assetDto.setAsset_type(asset.getType());
        assetDto.setSerial_no(asset.getSerial_number());
        assetDto.setStatus(asset.getAsset_status());
        assetDto.setCollabs(Arrays.stream(asset.getCollabs().split(",")).collect(Collectors.toSet()));
        return assetDto;
    }

    public Asset assetDtoToAsset(AssetDto assetDto) {
        Asset asset = new Asset();
        BeanUtils.copyProperties(assetDto, asset, "status", "collabs");
        if (assetDto.getId() != null) {
            asset.setMrid(assetDto.getId());
        }
        if (assetDto.getLocation_id() != null) {
            asset.setLocation_id(assetDto.getLocation_id());
        }
        asset.setType(assetDto.getAsset_type());
        asset.setSerial_number(assetDto.getSerial_no());
        asset.setAsset_status(assetDto.getAsset_type());
        asset.setCollabs(String.join(",", assetDto.getCollabs()));
        return asset;
    }

    public Asset copyAssetDtoToAsset(AssetDto assetDto, Asset asset, String... ignoreProperties) {
        // ignoredPropertyNames.add("collabs");
        BeanUtils.copyProperties(assetDto, asset, ignoreProperties);
        if (assetDto.getId() != null && !Arrays.asList(ignoreProperties).contains("id")) {
            asset.setMrid(assetDto.getId());
        }
        if (assetDto.getLocation_id() != null && !Arrays.asList(ignoreProperties).contains("location_id")) {
            asset.setLocation_id(assetDto.getLocation_id());
        }
        asset.setType(assetDto.getAsset_type());
        asset.setSerial_number(assetDto.getSerial_no());
        asset.setAsset_status(assetDto.getAsset_type());
//        asset.setCollabs(String.join(",", assetDto.getCollabs()));
        return asset;
    }
}
