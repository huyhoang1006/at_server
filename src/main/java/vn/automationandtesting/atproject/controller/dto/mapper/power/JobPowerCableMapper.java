package vn.automationandtesting.atproject.controller.dto.mapper.power;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;
import vn.automationandtesting.atproject.controller.dto.power.JobPowerCableDto;
import vn.automationandtesting.atproject.entity.power.JobsPowerCable;

@Component
public class JobPowerCableMapper {
    public JobPowerCableDto JobToJobDto(JobsPowerCable job) {
        JobPowerCableDto jobDto = new JobPowerCableDto();
        BeanUtils.copyProperties(job, jobDto);
        return jobDto;
    }

    public JobsPowerCable JobDtoToJob(JobPowerCableDto jobDto) {
        JobsPowerCable job = new JobsPowerCable();
        BeanUtils.copyProperties(jobDto, job, "collabs");
        return job;
    }

    public JobsPowerCable copyJobDtoToJob(JobPowerCableDto jobDto, JobsPowerCable job, String... ignoredPropertyName) {
        BeanUtils.copyProperties(jobDto, job, ignoredPropertyName);
        job.setCollabs(String.join(",", job.getCollabs()));
        return job;
    }
}
