package vn.automationandtesting.atproject.controller.dto.surge;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.UUID;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class TestSurgeTypeDto {
    private UUID id;
    private String name;
    private String code;
}
