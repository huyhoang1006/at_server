package vn.automationandtesting.atproject.controller.dto.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author tridv on 21/9/2022
 * @project at-project-server
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class HealthIndexResponseDto {
    private float averageHealthIndex;
    private float worstHealthIndex;
}
