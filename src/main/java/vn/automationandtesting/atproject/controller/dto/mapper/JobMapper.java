package vn.automationandtesting.atproject.controller.dto.mapper;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;
import vn.automationandtesting.atproject.controller.dto.cim.JobDto;
import vn.automationandtesting.atproject.entity.cim.Job;

import java.util.Arrays;
import java.util.stream.Collectors;

/**
 * @author tund on 5/9/2022
 * @project at-project-server
 */
@Component
public class JobMapper {
    public JobDto jobToJobDto(Job job) {
        JobDto jobDto = new JobDto();
        BeanUtils.copyProperties(job, jobDto, "status", "collabs");
        jobDto.setAsset_id(job.getAsset().getMrid());
        jobDto.setCollabs(Arrays.stream(job.getCollabs().split(",")).collect(Collectors.toSet()));
        jobDto.setAverage_health_index(job.getAverageHealthIndex());
        jobDto.setWorst_health_index(job.getWorstHealthIndex());
        return jobDto;
    }

    public Job jobDtoToJob(JobDto jobDto) {
        Job job = new Job();
        BeanUtils.copyProperties(jobDto, job, "status", "collabs");
        job.setCollabs(String.join(",", jobDto.getCollabs()));
        job.setWorstHealthIndex(jobDto.getWorst_health_index());
        job.setAverageHealthIndex(jobDto.getAverage_health_index());
        return job;
    }

    public Job copyJobDtoToJob(JobDto jobDto, Job job, String... ignoredPropertyName) {
        BeanUtils.copyProperties(jobDto, job, ignoredPropertyName);
//        job.setCollabs(String.join(",", jobDto.getCollabs()));
        return job;
    }
}
