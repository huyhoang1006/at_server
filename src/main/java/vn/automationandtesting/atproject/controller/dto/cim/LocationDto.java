package vn.automationandtesting.atproject.controller.dto.cim;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

/**
 * @author tridv on 2/9/2022
 * @project at-project-server
 */
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class LocationDto {
    private UUID id;
    private UUID user_id;
    private String name;
    private String region;
    private Integer division;
    private String area;
    private String plant;
    private String address;
    private String city;
    private String state_province;
    private String postal_code;
    private String country;
    private String geo_coordinates;
    private String location_system_code;
    private String person_name;
    private String person_phone_no1;
    private String person_phone_no2;
    private String person_fax_no;
    private String person_email;
    private String comment;
    private String company_company;
    private String company_department;
    private String company_address;
    private String company_city;
    private String company_state_province;
    private String company_postal_code;
    private String company_country;
    private String company_phone_no;
    private String company_fax_no;
    private String company_email;
    private boolean locked = false;
    private String mode;
    private String refId;
    private Set<String> collabs = new HashSet<>();
}
