package vn.automationandtesting.atproject.controller.dto.mapper.current;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;
import vn.automationandtesting.atproject.controller.dto.circuit.JobCircuitDto;
import vn.automationandtesting.atproject.controller.dto.current.JobCurrentDto;
import vn.automationandtesting.atproject.entity.circuit.Jobdata;
import vn.automationandtesting.atproject.entity.current.JobsCurrent;

@Component
public class JobCurrentMapper {
    public JobCurrentDto JobToJobDto(JobsCurrent job) {
        JobCurrentDto jobDto = new JobCurrentDto();
        BeanUtils.copyProperties(job, jobDto);
        return jobDto;
    }

    public JobsCurrent JobDtoToJob(JobCurrentDto jobDto) {
        JobsCurrent job = new JobsCurrent();
        BeanUtils.copyProperties(jobDto, job, "collabs");
        return job;
    }

    public JobsCurrent copyJobDtoToJob(JobCurrentDto jobDto, JobsCurrent job, String... ignoredPropertyName) {
        BeanUtils.copyProperties(jobDto, job, ignoredPropertyName);
        job.setCollabs(String.join(",", job.getCollabs()));
        return job;
    }
}
