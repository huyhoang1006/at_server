package vn.automationandtesting.atproject.controller.dto.surge;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.sql.Timestamp;
import java.util.UUID;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class JobSurgeDto {
    private UUID id;
    private String name;
    private String work_order;
    private String creation_date;
    private String execution_date;
    private String tested_by;
    private String approved_by;
    private String approval_date;
    private String summary;
    private String ambient_condition;
    private String testing_method;
    private String standard;
    private Float average_health_index;
    private Float worst_health_index;
    private boolean locked;
    private UUID locked_by;
    private String collabs;
    private UUID asset_id;
    @JsonProperty("created_by")
    private UUID createdBy;
    @JsonProperty("created_on")
    private Timestamp createdOn;
    @JsonProperty("updated_by")
    private UUID updatedBy;
    @JsonProperty("updated_on")
    private Timestamp updatedOn;
}
