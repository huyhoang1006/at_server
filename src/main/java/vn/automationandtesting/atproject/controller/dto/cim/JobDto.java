package vn.automationandtesting.atproject.controller.dto.cim;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

/**
 * @author tridv on 2/9/2022
 * @project at-project-server
 */
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class JobDto {
    private UUID id;
    private UUID asset_id;
    private String name;
    private String work_order;
    private String creation_date;
    private String execution_date;
    private String tested_by;
    private String approved_by;
    private String approved_date;
    private String summary;
    private String ambient_condition;
    private String testing_method;
    private String standard;
    private boolean locked = false;
    private Set<String> collabs = new HashSet<>();
    private float average_health_index;
    private float worst_health_index;
}
