package vn.automationandtesting.atproject.controller.dto.mapper.power;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;
import vn.automationandtesting.atproject.controller.dto.power.PowerCableDto;
import vn.automationandtesting.atproject.entity.power.PowerCable;

import java.util.Arrays;


@Component
public class PowerCableMapper {
    public PowerCableDto assetToAssetDto(PowerCable asset) {
        PowerCableDto assetDto = new PowerCableDto();
        BeanUtils.copyProperties(asset, assetDto);
        return assetDto;
    }

    public PowerCable assetDtoToAsset(PowerCableDto assetDto) {
        PowerCable asset = new PowerCable();
        BeanUtils.copyProperties(assetDto, asset, "collabs");
        return asset;
    }

    public PowerCable copyAssetDtoToAsset(PowerCableDto assetDto, PowerCable asset, String... ignoreProperties) {
        // ignoredPropertyNames.add("collabs");
        BeanUtils.copyProperties(assetDto, asset, ignoreProperties);
        if (assetDto.getId() != null && !Arrays.asList(ignoreProperties).contains("id")) {
            asset.setId(assetDto.getId());
        }
        if (assetDto.getLocation_id() != null && !Arrays.asList(ignoreProperties).contains("location_id")) {
            asset.setLocation_id(assetDto.getLocation_id());
        }
        asset.setAsset_type(assetDto.getAsset_type());
        asset.setSerial_no(assetDto.getSerial_no());
//        asset.setCollabs(String.join(",", assetDto.getCollabs()));
        return asset;
    }
}
