package vn.automationandtesting.atproject.controller.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import vn.automationandtesting.atproject.controller.dto.cim.JobDto;
import vn.automationandtesting.atproject.controller.dto.cim.TestDto;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class JobFullDto {
    private JobDto job;
    private List<TestDto> listTest;
}
