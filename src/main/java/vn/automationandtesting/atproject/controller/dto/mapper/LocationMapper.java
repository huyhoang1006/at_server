package vn.automationandtesting.atproject.controller.dto.mapper;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;
import vn.automationandtesting.atproject.controller.dto.cim.LocationDto;
import vn.automationandtesting.atproject.entity.cim.Location;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author tridv on 5/9/2022
 * @project at-project-server
 */
@Component
public class LocationMapper {
    public Location locationDtoToLocation(LocationDto locationDto, String... ignoreProperties) {
        List<String> ignorePropertiesList = Arrays.asList(ignoreProperties);
        if (ignorePropertiesList == null)
            ignorePropertiesList.add("collabs");

        Location location = new Location();
        BeanUtils.copyProperties(locationDto, location, ignorePropertiesList.toArray(String[]::new));
        if (!Arrays.asList(ignoreProperties).contains("id")) {
            location.setMrid(locationDto.getId());
        }
        if (!Arrays.asList(ignoreProperties).contains("geo_info_reference")) {
            location.setGeo_info_reference(locationDto.getGeo_coordinates());
        }
        location.setCollabs(String.join(",", locationDto.getCollabs()));
        return location;
    }

    public LocationDto locationToLocationDto(Location location, String... ignoreProperties) {
        List<String> ignorePropertiesList = Arrays.asList(ignoreProperties);
        if (ignorePropertiesList == null)
            ignorePropertiesList.add("collabs");

        LocationDto locationDto = new LocationDto();
        BeanUtils.copyProperties(location, locationDto, ignorePropertiesList.toArray(String[]::new));
        if (!Arrays.asList(ignoreProperties).contains("mid")) {
            locationDto.setId(location.getMrid());
        }
        if (!Arrays.asList(ignoreProperties).contains("geo_coordinates")) {
            locationDto.setGeo_coordinates(location.getGeo_info_reference());
        }
        if (!Arrays.asList(ignoreProperties).contains("user_id")) {
            locationDto.setUser_id(location.getUser().getId());
        }
        locationDto.setCollabs(Arrays.stream(location.getCollabs().split(",")).collect(Collectors.toSet()));
        return locationDto;
    }

    public Location copyLocationDtoToLocation(LocationDto locationDto, Location location, String... ignoreProperties) {
        BeanUtils.copyProperties(locationDto, location, ignoreProperties);
        if (!Arrays.asList(ignoreProperties).contains("id")) {
            location.setMrid(locationDto.getId());
        }
        if (!Arrays.asList(ignoreProperties).contains("geo_info_reference")) {
            location.setGeo_info_reference(locationDto.getGeo_coordinates());
        }
//        location.setCollabs(String.join(",", locationDto.getCollabs()));
        return location;
    }
}
