package vn.automationandtesting.atproject.controller;

import io.jsonwebtoken.Claims;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.*;
import vn.automationandtesting.atproject.config.auth.BearerContextHolder;
import vn.automationandtesting.atproject.config.token.TokenUtils;
import vn.automationandtesting.atproject.config.user_auth.MyUserDetails;
import vn.automationandtesting.atproject.controller.dto.ChangePasswordReq;
import vn.automationandtesting.atproject.controller.dto.JwtResponse;
import vn.automationandtesting.atproject.controller.dto.ResponseObject;
import vn.automationandtesting.atproject.controller.dto.mapper.UserMapper;
import vn.automationandtesting.atproject.controller.dto.request.UserReqDto;
import vn.automationandtesting.atproject.entity.User;
import vn.automationandtesting.atproject.service.UserService;

import java.util.*;

@RestController
public class AuthenticationController {
    @Autowired
    private TokenUtils tokenUtils;
    @Autowired
    private UserService userService;
    @Autowired
    private UserMapper userMapper;

    @PutMapping(value = "/account/password")
    public ResponseEntity<?> changePassword(@RequestBody ChangePasswordReq changePasswordReq) {
        UUID userId = UUID.fromString(BearerContextHolder.getContext().getUserId());
        ResponseObject result = userService.changePassword(userId, changePasswordReq.getOldPassword(), changePasswordReq.getNewPassword());
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @PostMapping("/auth/token")
    public ResponseEntity<?> refreshToken(@RequestParam String refreshToken) {
        //TODO check valid refresh_token
        Claims claims = tokenUtils.getClaimsFromJwtToken(refreshToken);
        String username = claims.getSubject();
        User user = userService.getUserByUsername(username);
        if (user.getIsDeleted().booleanValue()) {
            throw new UsernameNotFoundException("User not found.");
        }

        Collection<GrantedAuthority> authorities = AuthorityUtils.commaSeparatedStringToAuthorityList(user.getRole().getRoleName().name());
        String[] groupArray = user.getGroups().split(",");
        List<String> groups = Arrays.asList(groupArray);
        MyUserDetails myUserDetails = new MyUserDetails(user.getUsername(), user.getPassword(), authorities, user.getId(), user.getRole().getRoleName().name(), groups, new HashSet<>());
        JwtResponse jwtResponse = tokenUtils.createJwtResponse(myUserDetails);
        ResponseObject responseObject = new ResponseObject(true, "Refresh access token", jwtResponse);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @PostMapping("/signup")
    private ResponseEntity<?> signup(@RequestBody UserReqDto userReqDto) {
        ResponseObject result = userService.saveNewUser(userReqDto);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

}
