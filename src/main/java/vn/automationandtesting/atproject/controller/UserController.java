package vn.automationandtesting.atproject.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import vn.automationandtesting.atproject.controller.dto.ResponseObject;
import vn.automationandtesting.atproject.controller.dto.mapper.UserMapper;
import vn.automationandtesting.atproject.controller.dto.request.UserReqDto;
import vn.automationandtesting.atproject.service.UserService;

import java.util.List;
import java.util.UUID;

/**
 * @author tridv on 1/9/2022
 * @project at-project-server
 */
@Controller
@RequestMapping("${api.prefix}")
public class UserController {
    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }
    @Autowired
    private UserMapper userMapper;

    @PostMapping("/users")
    public ResponseEntity<?> createUser(@RequestBody UserReqDto userReqDto) {
        ResponseObject responseObject = userService.saveNewUser(userReqDto);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/users")
    public ResponseEntity<?> getAllUsers() {
        List<UserReqDto> userReqDtoList = userService.getAllUsers();
        ResponseObject responseObject = new ResponseObject(true, "Get all users", userReqDtoList);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/users/{userId}")
    public ResponseEntity<?> getUserById(@PathVariable String userId) {
        UUID id = UUID.fromString(userId);
        UserReqDto userReqDto = userService.getUserById(id);
        ResponseObject responseObject = new ResponseObject(true, "Get user by id", userReqDto);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @PutMapping("/users/{userId}")
    public ResponseEntity<?> updateUser(@RequestBody UserReqDto userReqDto, @PathVariable String userId) {
        UUID id = UUID.fromString(userId);
        UserReqDto updatedUserReqDto = userService.updateUser(userReqDto, id);
        ResponseObject responseObject = new ResponseObject(true, "Update user", updatedUserReqDto);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @DeleteMapping("/users/{userId}")
    public ResponseEntity<?> getUser(@PathVariable String userId) {
        UUID id = UUID.fromString(userId);
        userService.deleteUserById(id);
        ResponseObject responseObject = new ResponseObject(true, "Delete user successfully", null);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @PutMapping("/users/{userId}/update-group")
    public ResponseEntity<?> updateUserGroups(@PathVariable("userId") String userId, @RequestParam List<String> groupNames) {
        UUID id = UUID.fromString(userId);
        UserReqDto updatedUserReqDto = userService.updateUserGroupsForAdmin(groupNames, id);
        ResponseObject responseObject = new ResponseObject(true, "Update user", updatedUserReqDto);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }
}
