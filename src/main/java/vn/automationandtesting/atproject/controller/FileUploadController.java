package vn.automationandtesting.atproject.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import vn.automationandtesting.atproject.controller.dto.ResponseObject;

import java.io.File;
import java.io.FileInputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Base64;

@Controller
@RequestMapping("${api.prefix}")
public class FileUploadController {
    String Working_Directory = System.getProperty("user.dir");
    String pathData = Working_Directory + System.getProperty("file.separator") + "FileUpload";
    @PostMapping("/file/upload")
    public ResponseEntity<?> uploadFile(@RequestParam("file") MultipartFile file) {
        try {
            byte[] bytes;
            bytes = file.getBytes();
            Files.write(Paths.get(pathData + System.getProperty("file.separator") + file.getOriginalFilename()), bytes);
            ResponseObject.returnWithData(true);
            return new ResponseEntity<>(ResponseObject.returnWithData(true), HttpStatus.OK);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED)
                    .body("Exception occurred for: " + file.getOriginalFilename() + "!");
        }
    }

    @GetMapping("/file/download/{fileName}")
    public ResponseEntity<?> download(@PathVariable String fileName) {
        try {
            String path = pathData + System.getProperty("file.separator") + fileName;

            File file = new File(path);
            FileInputStream dataFile = new FileInputStream(file);
            byte fileData[] = new byte[(int) file.length()];
            dataFile.read(fileData);
            String base64File = Base64.getEncoder().encodeToString(fileData);
            dataFile.close();
            return new ResponseEntity<>(ResponseObject.returnWithData(base64File), HttpStatus.OK);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED)
                    .body("Exception occurred for: " + fileName + "!");
        }
    }
}
