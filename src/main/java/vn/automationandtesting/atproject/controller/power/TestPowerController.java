package vn.automationandtesting.atproject.controller.power;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import vn.automationandtesting.atproject.controller.dto.ResponseObject;
import vn.automationandtesting.atproject.controller.dto.mapper.power.TestPowerCableMapper;
import vn.automationandtesting.atproject.controller.dto.power.TestPowerCableDto;
import vn.automationandtesting.atproject.entity.power.TestsPowerCable;
import vn.automationandtesting.atproject.service.power.TestPowerCableService;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Controller
@RequestMapping("${api.prefix}")
public class TestPowerController {
    @Autowired
    private TestPowerCableMapper mapper;

    @Autowired
    private TestPowerCableService service;

    @GetMapping("/power/test/findAllTestByJobId/{job_id}")
    public ResponseEntity<?> findAllTestByJobId(@PathVariable String job_id) {
        UUID id = UUID.fromString(job_id);
        List<TestPowerCableDto> dtoList = service.findAllTestByJobId(id);
        ResponseObject responseObject = new ResponseObject(true, "Get by job id", dtoList);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/power/test/findTestById/{id}")
    public ResponseEntity<?> findTestById(@PathVariable String id) {
        UUID indicate = UUID.fromString(id);
        List<TestPowerCableDto> dtoList = service.findTestById(indicate);
        ResponseObject responseObject = new ResponseObject(true, "Get by id", dtoList);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/power/test/findTestByType/{type_id}")
    public ResponseEntity<?> findTestByType(@PathVariable String type_id) {
        UUID typeId = UUID.fromString(type_id);
        List<TestPowerCableDto> dtoList = service.findTestByType(typeId);
        ResponseObject responseObject = new ResponseObject(true, "Get by type id", dtoList);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/power/test/findTestByTypeAndAsset/{type_id}/{asset_id}")
    public ResponseEntity<?> findTestByTypeAndAsset(@PathVariable(name = "type_id") String type_id, @PathVariable(name = "asset_id") String asset_id) {
        UUID typeId = UUID.fromString(type_id);
        UUID assetId = UUID.fromString(asset_id);
        List<TestPowerCableDto> dtoList = service.findTestByTypeAndAsset(typeId, assetId);
        ResponseObject responseObject = new ResponseObject(true, "Get by type id and asset id", dtoList);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @PostMapping("/power/test/insert")
    public ResponseEntity<?> insert(@RequestBody List<TestPowerCableDto> listTest) {
        UUID job_id = listTest.get(0).getJob_id();
        List<TestPowerCableDto> testDto = service.findAllTestByJobId(job_id);
        List<TestPowerCableDto> testDeleteDto = new ArrayList<>();

        if(testDto.size() != 0) {
            for (TestPowerCableDto item : testDto) {
                boolean sign = false;
                for (TestPowerCableDto element : listTest) {
                    if (item.getId().toString().equals(element.getId().toString())) {
                        sign = true;
                    }
                }
                if (!sign) {
                    testDeleteDto.add(item);
                }
            }
        }
        if(testDeleteDto.size() != 0) {
            List<TestsPowerCable> testDelete = new ArrayList<>();
            for(TestPowerCableDto item : testDeleteDto) {
                TestsPowerCable itemEntity = mapper.testDtoToTest(item);
                testDelete.add(itemEntity);
            }
            service.deleteAll(testDelete);
        }

        List<TestsPowerCable> tests = new ArrayList<>();
        for(TestPowerCableDto itemDto : listTest) {
            TestsPowerCable test = mapper.testDtoToTest(itemDto);
            tests.add(test);
        }
        service.saveAll(tests);
        ResponseObject responseObject = new ResponseObject(true, "save test", true);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @PostMapping("/power/test/delete")
    public ResponseEntity<?> delete(@RequestBody List<TestPowerCableDto> listTest) {
        List<TestsPowerCable> tests = new ArrayList<>();
        for(TestPowerCableDto itemDto : listTest) {
            TestsPowerCable test = mapper.testDtoToTest(itemDto);
            tests.add(test);
        }
        service.deleteAll(tests);
        ResponseObject responseObject = new ResponseObject(true, "delete test", true);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

}
