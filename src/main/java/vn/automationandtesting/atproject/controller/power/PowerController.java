package vn.automationandtesting.atproject.controller.power;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import vn.automationandtesting.atproject.config.auth.BearerContextHolder;
import vn.automationandtesting.atproject.controller.circuit.CircuitController;
import vn.automationandtesting.atproject.controller.dto.ResponseObject;
import vn.automationandtesting.atproject.controller.dto.mapper.power.PowerCableMapper;
import vn.automationandtesting.atproject.controller.dto.power.PowerCableDto;
import vn.automationandtesting.atproject.entity.power.PowerCable;
import vn.automationandtesting.atproject.service.power.PowerCableService;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Controller
@RequestMapping("${api.prefix}")
public class PowerController {
    Logger logger = LoggerFactory.getLogger(CircuitController.class);

    @Autowired
    private PowerCableService service;

    @Autowired
    private PowerCableMapper mapper;

    @GetMapping("/power/findById/{_id}")
    public ResponseEntity<?> getAssetByID(@PathVariable String _id) {
        UUID id = UUID.fromString(_id);
        List<PowerCableDto> dtoList = service.findAssetById(id);
        ResponseObject responseObject = new ResponseObject(true, "Get asset by ID", dtoList);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/power/findAll")
    public ResponseEntity<?> getAll() {
        List<PowerCableDto> dtoList = service.findAll();
        ResponseObject responseObject = new ResponseObject(true, "Get all asset", dtoList);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/power/findByLocationId/{locationId}")
    public ResponseEntity<?> findAllByLocationId(@PathVariable String locationId) {
        UUID location_Id = UUID.fromString(locationId);
        List<PowerCableDto> dtoList = service.findAllByLocationId(location_Id);
        ResponseObject responseObject = new ResponseObject(true, "Get asset by location id", dtoList);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/power/findByLocationIdAndCollab/{locationId}")
    public ResponseEntity<?> findByLocationIdAndCollab(@PathVariable String locationId) {
        String userId = BearerContextHolder.getContext().getUserId();
        UUID location_Id = UUID.fromString(locationId);
        List<PowerCableDto> dtoList = service.findByLocationIdAndCollabsContaining(location_Id, userId);
        ResponseObject responseObject = new ResponseObject(true, "Get asset by location id", dtoList);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/power/findBySerial/{serial}")
    public ResponseEntity<?> findBySerial(@PathVariable String serial) {
        List<PowerCableDto> dtoList = service.findBySerial(serial);
        ResponseObject responseObject = new ResponseObject(true, "Get asset serial", dtoList);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/power/findBySerial/{serial}/{locationId}")
    public ResponseEntity<?> findBySerialAndLocation(@PathVariable(name = "serial") String serial, @PathVariable(name = "locationId") String locationId) {
        UUID location_id = UUID.fromString(locationId);
        List<PowerCableDto> dtoList = service.findBySerialAndLocation(serial, location_id);
        ResponseObject responseObject = new ResponseObject(true, "Get asset by serial and location id", dtoList);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @PostMapping("/power/lock/{sign}/{id}")
    public ResponseEntity<?> lockAsset(@PathVariable(name = "sign") boolean sign, @PathVariable(name = "id") String id) {
        PowerCableDto assets = service.findAssetById(UUID.fromString(id)).get(0);
        List<PowerCable> assetItem = new ArrayList<>();
        String userId = BearerContextHolder.getContext().getUserId();
        if(sign) {
            assets.setLocked(true);
            assets.setLocked_by(UUID.fromString(userId));
            PowerCable item = mapper.assetDtoToAsset(assets);
            item.setCollabs(assets.getCollabs());
            assetItem.add(item);
            service.saveAll(assetItem);
        } else {
            if(assets.getLocked_by().toString().equals(userId)) {
                assets.setLocked(false);
                assets.setLocked_by(UUID.fromString("00000000-0000-0000-0000-000000000000"));
                PowerCable item = mapper.assetDtoToAsset(assets);
                item.setCollabs(assets.getCollabs());
                assetItem.add(item);
                service.saveAll(assetItem);
            }
        }

        ResponseObject responseObject = new ResponseObject(true, "sign lock asset", true);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @PostMapping("/power/save")
    public ResponseEntity<?> insertAsset(@RequestBody List<PowerCableDto> dtoList) {
        List<PowerCable> assets = new ArrayList<>();
        for(PowerCableDto items : dtoList) {
            List<PowerCableDto> item = service.findAssetById(items.getId());
            PowerCable asset = new PowerCable();
            if(item.size() == 0) {
                asset = mapper.assetDtoToAsset(items);
            } else {
                asset = mapper.copyAssetDtoToAsset(item.get(0), asset);
                asset = mapper.copyAssetDtoToAsset(items, asset, "createdOn", "createdBy", "locked", "collabs");
            }

            if(!asset.isLocked()) {
                assets.add(asset);
            }
        }
        if(assets.size() != 0) {
            service.saveAll(assets);
        }
        ResponseObject responseObject = new ResponseObject(true, "save asset", true);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @PostMapping("/power/delete")
    public ResponseEntity<?> deleteAsset(@RequestBody List<PowerCableDto> dtoList) {
        List<PowerCable> assets = new ArrayList<>();
        for(PowerCableDto items : dtoList) {
            List<PowerCableDto> item = service.findAssetById(items.getId());
            if(!item.get(0).isLocked()) {
                PowerCable asset = mapper.assetDtoToAsset(items);
                assets.add(asset);
            }
        }
        if(assets.size() != 0) {
            service.deleteAll(assets);
        }
        ResponseObject responseObject = new ResponseObject(true, "delete asset", true);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }
}
