package vn.automationandtesting.atproject.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import vn.automationandtesting.atproject.config.auth.BearerContextHolder;
import vn.automationandtesting.atproject.controller.dto.ListIdDto;
import vn.automationandtesting.atproject.controller.dto.ResourceFullDto;
import vn.automationandtesting.atproject.controller.dto.ResponseObject;
import vn.automationandtesting.atproject.controller.dto.cim.AssetDto;
import vn.automationandtesting.atproject.service.AssetService;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("${api.prefix}")
public class AssetController {
    Logger logger = LoggerFactory.getLogger(AssetController.class);


    private final AssetService assetService;

    @Autowired
    public AssetController(AssetService assetService) {
        this.assetService = assetService;
    }

//    @GetMapping("/assets")
//    public ResponseEntity<?> getAllAssets() {
//        List<AssetDto> assetDtoList = assetService.getAllAssets();
//        ResponseObject responseObject = new ResponseObject(true, "Get all assets", assetDtoList);
//        return new ResponseEntity<>(responseObject, HttpStatus.OK);
//    }

    @GetMapping("/assets/{assetId}")
    public ResponseEntity<?> getAssetByID(@PathVariable String assetId) {
        UUID id = UUID.fromString(assetId);
        AssetDto assetDto = assetService.getAssetByIdAndUserIdAndCollabId(id, UUID.fromString(BearerContextHolder.getContext().getUserId()));
        ResponseObject responseObject = new ResponseObject(true, "Get asset by ID", assetDto);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/assets/getAssetByLocation/{locationId}")
    public ResponseEntity<?> getAssetByLocationId(@PathVariable String locationId) {
        UUID id = UUID.fromString(locationId);
        List<AssetDto> assetDtoList = assetService.getAssetByLocationIdAndUserIdAndCollabId(id, UUID.fromString(BearerContextHolder.getContext().getUserId()));
        ResponseObject responseObject = new ResponseObject(true, "Get asset by location ID", assetDtoList);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @PostMapping("/assets")
    public ResponseEntity<?> createAsset(@RequestBody AssetDto assetDto) {
        AssetDto savedAssetDto = assetService.createNewAsset(assetDto);
        ResponseObject responseObject = new ResponseObject(true, "Create asset", savedAssetDto);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @PutMapping("/assets/{assetId}")
    public ResponseEntity<?> updateAsset(@RequestBody AssetDto assetDto, @PathVariable String assetId) {
        UUID id = UUID.fromString(assetId);
        AssetDto updatedAssetDto = assetService.updateAsset(assetDto, id);
        ResponseObject responseObject = new ResponseObject(true, "Update asset", updatedAssetDto);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @DeleteMapping("/assets/{assetId}")
    public ResponseEntity<?> deleteAsset(@PathVariable String assetId) {
        UUID id = UUID.fromString(assetId);
        assetService.deleteAsset(id);
        ResponseObject responseObject = new ResponseObject(true, "Delete successfully", null);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/assets")
    public ResponseEntity<?> searchAssets(
            @RequestParam(required = false, name = "location_id") String locationId,
            @RequestParam(required = false, name = "serial_no") String serialNo,
            Pageable pageable
    ) {
        List<AssetDto> assetDtos = assetService.searchAssets(locationId, serialNo, pageable);
        ResponseObject responseObject = new ResponseObject(true, "Search assets", assetDtos);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @PutMapping("/assets/lock")
    public ResponseEntity<?> lock(@RequestBody ListIdDto listIdDto) {
        ResponseObject responseObject = assetService.lock(true, listIdDto.getListId());
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @PutMapping("/assets/unlock")
    public ResponseEntity<?> unlock(@RequestBody ListIdDto listIdDto) {
        ResponseObject responseObject = assetService.lock(false, listIdDto.getListId());
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/assets/download")
    public ResponseEntity<?> download(@RequestParam List<UUID> listId) {
        ResponseObject responseObject = assetService.download(listId);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @PostMapping("/assets/upload")
    public ResponseEntity<?> upload(@RequestBody ResourceFullDto resourceFullDto) {
        ResponseObject responseObject = assetService.upload(resourceFullDto);
        logger.info(String.valueOf(resourceFullDto));
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @DeleteMapping("/assets/delete-multiple")
    public ResponseEntity<?> deleteMultiple(@RequestParam List<UUID> listId) {
        for (UUID id : listId) {
            try {
                assetService.deleteAsset(id);
            } catch (Exception e) {
                // TODO: handle exception
            }
        }
        ResponseObject responseObject = new ResponseObject(true, "Delete successfully", null);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }
}
