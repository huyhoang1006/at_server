package vn.automationandtesting.atproject.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import vn.automationandtesting.atproject.controller.dto.ListIdDto;
import vn.automationandtesting.atproject.controller.dto.ResourceFullDto;
import vn.automationandtesting.atproject.controller.dto.ResponseObject;
import vn.automationandtesting.atproject.controller.dto.cim.LocationDto;
import vn.automationandtesting.atproject.service.AttachmentService;
import vn.automationandtesting.atproject.service.LocationService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@RestController
@RequestMapping("${api.prefix}")
public class LocationController {
    private final LocationService locationService;

    @Autowired
    private AttachmentService attachmentService;

    @Autowired
    public LocationController(LocationService locationService) {
        this.locationService = locationService;
    }

    @PostMapping("/locations")
    public ResponseEntity<?> createLocation(@RequestBody LocationDto locationDto) {
        LocationDto savedLocationDto = locationService.createLocation(locationDto);
        ResponseObject responseObject = new ResponseObject(true, "Create location", savedLocationDto);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/locations/download")
    public ResponseEntity<?> download(@RequestParam List<UUID> listId) {
        ResponseObject responseObject = locationService.download(listId);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @PostMapping("/locations/upload")
    public ResponseEntity<?> upload(@RequestBody ResourceFullDto resourceFullDto) {
        ResponseObject responseObject = locationService.upload(resourceFullDto);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @PutMapping("/locations/lock")
    public ResponseEntity<?> lock(@RequestBody ListIdDto listIdDto) {
        ResponseObject responseObject = locationService.lock(true, listIdDto.getListId());
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @PutMapping("/locations/unlock")
    public ResponseEntity<?> unlock(@RequestBody ListIdDto listIdDto) {
        ResponseObject responseObject = locationService.lock(false, listIdDto.getListId());
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @PutMapping("/locations/{locationId}")
    public ResponseEntity<?> updateLocation(@PathVariable String locationId, @RequestBody LocationDto locationDto) {
        UUID id = UUID.fromString(locationId);
        LocationDto updatedLocationDto = locationService.updateLocation(id, locationDto);
        ResponseObject responseObject = new ResponseObject(true, "Create location", updatedLocationDto);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @DeleteMapping("/locations/{locationId}")
    public ResponseEntity<?> deleteLocation(@PathVariable String locationId) {
        UUID id = UUID.fromString(locationId);
        locationService.deleteLocation(id);
        ResponseObject responseObject = new ResponseObject(true, "Delete location successfully", null);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/locations/{locationId}")
    public ResponseEntity<?> getLocationById(@PathVariable String locationId) {
        UUID id = UUID.fromString(locationId);
        LocationDto locationDto = locationService.getLocationById(id);
        ResponseObject responseObject = new ResponseObject(true, "Get location by ID", locationDto);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @GetMapping("/locations")
    public ResponseEntity<?> searchLocations(
            @RequestParam(required = false, name = "user_id") String userId,
            Pageable pageable
    ) {
        UUID id = null;
        if (userId != null) {
            id = UUID.fromString(userId);
        }
        List<LocationDto> locationDtoList = locationService.searchLocations(id, pageable);
        ResponseObject responseObject = new ResponseObject(true, "Search locations", locationDtoList);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @DeleteMapping("/locations/delete-multiple")
    public ResponseEntity<?> deleteMultiple(@RequestParam List<UUID> listId) {
        for (UUID id : listId) {
            try {
                List<LocationDto> listIdDelete = new ArrayList<>();
                LocationDto location = locationService.getLocationById(id);
                if(location.getMode().equals("location")) {
                    List<LocationDto> listVoltage = locationService.getLocationByRefId(location.getId());
                    listIdDelete.add(location);
                    listIdDelete.addAll(listVoltage);
                    for(LocationDto locationDto : listVoltage) {
                        List<LocationDto> listFeeder = locationService.getLocationByRefId(locationDto.getId());
                        listIdDelete.addAll(listFeeder);
                    }
                } else if(location.getMode().equals("voltage")) {
                    List<LocationDto> listFeeder = locationService.getLocationByRefId(location.getId());
                    listIdDelete.add(location);
                    listIdDelete.addAll(listFeeder);
                } else {
                    listIdDelete.add(location);
                }
                listIdDelete.forEach(element -> {
                    System.out.println(element.getName());
                });
                if(!listIdDelete.isEmpty()) {
                    List<LocationDto> locationLock = listIdDelete.stream().filter(item -> item.isLocked() == true).collect(Collectors.toList());
                    if(locationLock.isEmpty()) {
                        List<UUID> listLocationId = listIdDelete.stream().map(temp -> temp.getId()).collect(Collectors.toList());
                        for(UUID dataId : listLocationId) {
                            locationService.deleteLocation(dataId);
                            attachmentService.deleteAttachmentById_foreign(dataId.toString());
                        }
                    }

                }
            } catch (Exception e) {
                System.out.println(e);
                // TODO: handle exception
            }
        }
        ResponseObject responseObject = new ResponseObject(true, "Delete successfully", null);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @PutMapping("/locations/{locationId}/updateCollabs")
    public ResponseEntity<?> updateJobCollabs(@PathVariable String locationId, @RequestBody ListIdDto listIdDto) {
        locationService.updateJobCollabs(locationId, listIdDto);
        ResponseObject responseObject = new ResponseObject(true, "Update collaborators successfully", null);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }
}
