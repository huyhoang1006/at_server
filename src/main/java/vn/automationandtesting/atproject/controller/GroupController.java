package vn.automationandtesting.atproject.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import vn.automationandtesting.atproject.controller.dto.GroupDto;
import vn.automationandtesting.atproject.controller.dto.ResponseObject;
import vn.automationandtesting.atproject.controller.dto.StatusMessage;
import vn.automationandtesting.atproject.service.GroupService;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("${api.prefix}")
public class GroupController {
    @Autowired
    private GroupService groupService;

    @GetMapping("/groups")
    public ResponseEntity<?> getGroups() {
        List<GroupDto> groupDtoList = groupService.getAllGroups();
        ResponseObject responseObject = new ResponseObject(true, "Get list of groups", groupDtoList);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @PostMapping("/groups")
    public ResponseEntity<?> createGroup(@RequestBody GroupDto groupDto) {
        GroupDto savedGroupDto = groupService.createNewGroup(groupDto);
        ResponseObject responseObject = new ResponseObject(true, "Successfully creating group", savedGroupDto);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @PutMapping("/groups/{groupID}")
    public ResponseEntity<?> updateGroup(@RequestBody GroupDto groupDto, @PathVariable("groupID") String groupID) {
        GroupDto updatedGroupDto = groupService.updateGroup(groupDto, UUID.fromString(groupID));
        ResponseObject responseObject = new ResponseObject(true, "Successfully updating group", updatedGroupDto);
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }

    @DeleteMapping("/groups/{groupID}")
    public ResponseEntity<?> deleteGroup(@PathVariable String groupID) {
        groupService.deleteGroup(groupID);
        ResponseObject responseObject =
                new ResponseObject(true, "Successfully deleting group", new StatusMessage("success", "Successfully deleted"));
        return new ResponseEntity<>(responseObject, HttpStatus.OK);
    }
}
