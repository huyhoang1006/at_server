package vn.automationandtesting.atproject.entity.power;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import vn.automationandtesting.atproject.entity.BaseEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.UUID;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor

@Table(name = "power_cable")
public class PowerCable extends BaseEntity {
    @Id
    @Column(unique = true, nullable = false)
    private UUID id;

    private String properties;
    @Column(name = "power_cable")
    private String powerCable;
    private String assessories;
    private String extend;
    private String asset;
    private String asset_type;
    private String serial_no;
    private String manufacturer;
    private String manufacturer_type;
    private String manufacturing_year;
    private String asset_system_code;
    private String apparatus_id;
    private String feeder;
    private boolean locked;
    private UUID locked_by;
    private String collabs;
    private UUID location_id;
}
