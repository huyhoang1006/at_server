package vn.automationandtesting.atproject.entity.cim;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import vn.automationandtesting.atproject.entity.BaseEntity;
import vn.automationandtesting.atproject.entity.User;

import javax.persistence.*;
import java.util.List;
import java.util.UUID;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor

@Table(name = "location")
public class Location extends BaseEntity {
    @Id
    @Column(unique = true, nullable = false)
    private UUID mrid;

    private String alias_name;
    private String description;
    private String name;
    private String type;
    private UUID main_address;
    private UUID secondary_address;
    private UUID phone1;
    private UUID phone2;
    private UUID electronic_address;
    private String geo_info_reference;
    private String direction;
    private UUID status;
    private UUID configuration_event;
    private UUID coordinate_system;
    private UUID position_point;
    private UUID power_system_resource;
    private UUID asset;
    private UUID asset_model;
    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;
    private String region;
    private Integer division;
    private String area;
    private String plant;
    private String address;
    private String city;
    private String state_province;
    private String postal_code;
    private String country;
    private String location_system_code;
    private String person_name;
    private String person_phone_no1;
    private String person_phone_no2;
    private String person_fax_no;
    private String person_email;
    private String comment;
    private String company_company;
    private String company_department;
    private String company_address;
    private String company_city;
    private String company_state_province;
    private String company_postal_code;
    private String company_country;
    private String company_phone_no;
    private String company_fax_no;
    private String company_email;
    private boolean locked = false;
    private UUID locked_by;
    private String collabs = "";
    private String mode;
    @Column(name = "ref_id")
    private String refId;

    @OneToMany(mappedBy = "asset")
    private List<Asset> assets;

    @Transient
    private UUID user_id;
}
