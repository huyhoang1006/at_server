package vn.automationandtesting.atproject.entity.cim;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import vn.automationandtesting.atproject.entity.BaseEntity;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Table(name = "tap_changer_info")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor

public class TapChanger extends BaseEntity {
    @Id
    @Column(unique = true, nullable = false)
    private UUID mrid;

    private String alias_name;
    private String description;
    private String name;
    private boolean is_tcul;
    private double pt_ratio;
    private double ct_radio;
    private UUID ct_rating;
    private UUID neutral_u;
    private Integer neutral_step;
    private Integer high_step;
    private Integer low_step;
    private UUID step_voltage_increment;
    private UUID step_phase_increment;
    private UUID rated_voltage;
    private UUID rated_apparent_power;
    private UUID rated_current;
    private UUID bil;
    private UUID frequency;
    private UUID power_system_resource;
    private UUID asset;
    private UUID asset_model;
    @OneToOne
    @JoinColumn(name = "asset_id")
    private Asset assets;
    private String mode;
    private String serial_no;
    private String manufacturer;
    private String manufacturer_type;
    private String winding;
    private String tap_scheme;
    private Integer no_of_taps;
    private String voltage_table;

}
