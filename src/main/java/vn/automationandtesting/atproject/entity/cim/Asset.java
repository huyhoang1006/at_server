package vn.automationandtesting.atproject.entity.cim;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import vn.automationandtesting.atproject.entity.BaseEntity;

import javax.persistence.*;
import java.util.List;
import java.util.UUID;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor

public class Asset extends BaseEntity {
    @Id
    @Column(unique = true, nullable = false)
    private UUID mrid;

    private String alias_name;
    private String description;
    private String name;
    private String type;
    private String utc_number;
    private String serial_number;
    private String lot_number;
    private UUID purchase_price;
    private boolean critical;
    //Reference to ElectronicAddress
    private UUID electronic_address;
    private UUID life_cycle;
    private UUID acceptance_test;
    private String initial_condition;
    private UUID initial_loss_of_life;
    private UUID status;
    private UUID activity_record;
    private UUID configuration_event;
    private UUID asset_info;
    @ManyToOne
    @JoinColumn(name = "location_id")
    private Location location;
    private String asset;
    private String manufacturer;
    private String manufacturer_type;
    private String manufacturing_year;
    private String asset_system_code;
    private String apparatus_id;
    private String feeder;
    private String date_of_warehouse_receipt;
    private String date_of_delivery;
    private String date_of_production_order;
    private String comment;
    private String phases;
    private String vector_group;
    private String vector_group_custom;
    private String unsupported_vector_group;
    private String rated_frequency;
    private String voltage_ratings;
    private String voltage_regulation;
    private String power_ratings;
    private String current_ratings;
    private String max_short_circuit_current_ka;
    private String max_short_circuit_current_s;
    private String ref_temp;
    private String prim_sec;
    private String prim_tert;
    private String sec_tert;
    private String base_power;
    private String base_voltage;
    private String zero_percent;
    private String category;
    private String asset_status;
    private String tank_type;
    private String insulation_medium;
    private String insulation_weight;
    private String insulation_volume;
    private String total_weight;
    private String winding;
    private String date_of_warehouse_delivery;
    private String progress;
    private String standard;
    private String oil_type;
    private String thermal_meter;
    private boolean locked = false;
    private UUID locked_by;
    private String collabs = "";
    private String extend;

    @OneToMany(mappedBy = "asset")
    private List<Job> jobs;
    @Transient
    private UUID location_id;
}
