package vn.automationandtesting.atproject.entity.cim;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import vn.automationandtesting.atproject.entity.BaseEntity;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor

public class Bushing extends BaseEntity {
    @Id
    @Column(unique = true, nullable = false)
    private UUID id;

    @OneToOne
    @JoinColumn(name = "asset_id", referencedColumnName = "mrid")
    private Asset asset;

    private String asset_type;
    private String serial_no;
    private String manufacturer;
    private String manufacturer_type;
    private String manufacturer_year;
    private String insull_level;
    private String voltage_gr;
    private String max_sys_voltage;
    private String rate_current;
    private String df_c1;
    private String cap_c1;
    private String df_c2;
    private String cap_c2;
    private String insulation_type;
}
