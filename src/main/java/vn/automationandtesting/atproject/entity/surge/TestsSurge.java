package vn.automationandtesting.atproject.entity.surge;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import vn.automationandtesting.atproject.entity.BaseEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.UUID;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor

@Table(name = "testssurge")
public class TestsSurge extends BaseEntity {
    @Id
    @Column(unique = true, nullable = false)
    private UUID id;

    private String name;
    private String data;
    private Float average_score;
    private Float worst_score;
    private Float total_average_score;
    private Float total_worst_score;
    private Float weighting_factor;
    private Float average_score_df;
    private Float average_score_c;
    private Float worst_score_df;
    private Float worst_score_c;
    private Float weighting_factor_df;
    private Float weighting_factor_c;
    private boolean locked;
    private UUID locked_by;
    private String collabs;
    private UUID type_id;
    private UUID job_id;

}
