package vn.automationandtesting.atproject.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import vn.automationandtesting.atproject.entity.enumm.PermissionEnum;

import javax.persistence.*;
import java.util.Set;
import java.util.UUID;

@Entity
@Table(name = "permission")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor

public class Permission extends BaseEntity{
    private static final long serialVersionUID = 1L;

    @Id
    @Column(unique = true, nullable = false)
    private UUID id;
    private String description;
    @Column(name = "permission_name", unique = true)
    @Enumerated(EnumType.STRING)
    private PermissionEnum permissionName;
    @OneToMany(mappedBy = "permission", cascade = CascadeType.REMOVE)
    private Set<GroupPermission> groupPermissions;
}
