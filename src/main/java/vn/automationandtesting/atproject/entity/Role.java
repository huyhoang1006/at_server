package vn.automationandtesting.atproject.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import vn.automationandtesting.atproject.entity.enumm.RoleEnum;

import javax.persistence.*;
import java.util.List;
import java.util.UUID;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor

public class Role extends BaseEntity {
    @Id
    @Column(unique = true, nullable = false)
    private UUID id;

    private String description;

    @Column(name = "role_name", unique = true)
    @Enumerated(EnumType.STRING)
    private RoleEnum roleName;

    @OneToMany(mappedBy = "role")
    private List<User> users;

}
