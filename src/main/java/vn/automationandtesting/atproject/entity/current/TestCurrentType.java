package vn.automationandtesting.atproject.entity.current;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.UUID;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor

@Table(name = "testcurrent_type")
public class TestCurrentType {
    @Id
    @Column(unique = true, nullable = false)
    private UUID id;

    private String name;
    private String code;

}
