package vn.automationandtesting.atproject.entity.circuit;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import vn.automationandtesting.atproject.entity.BaseEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.UUID;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor

@Table(name = "jobdata")
public class Jobdata extends BaseEntity {
    @Id
    @Column(unique = true, nullable = false)
    private UUID id;

    private String name;
    private String work_order;
    private String creation_date;
    private String execution_date;
    private String tested_by;
    private String approved_by;
    private String approval_date;
    private String summary;
    private String ambient_condition;
    private String testing_method;
    private String standard;
    private Float average_health_index;
    private Float worst_health_index;
    private boolean locked;
    private UUID locked_by;
    private String collabs;
    private UUID asset_id;
}
