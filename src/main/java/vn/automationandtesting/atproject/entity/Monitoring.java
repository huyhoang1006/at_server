package vn.automationandtesting.atproject.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import vn.automationandtesting.atproject.entity.cim.Asset;

import javax.persistence.*;
import java.util.UUID;

/**
 * @author tridv on 22/9/2022
 * @project at-project-server
 */
@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Monitoring extends BaseEntity {
    @Id
    private UUID id;
    @Column(name = "ageing_insulation")
    private String ageingInsulation;
    @Column(name = "moisture_insulation")
    private String moistureInsulation;
    @Column(name = "bushings_online")
    private String bushingsOnline;
    @Column(name = "patital_discharge")
    private String patitalDischarge;
    @Column(name = "dga")
    private String dga;


    @Column(name = "bushing_df_worst")
    private String bushing_df_worst;
    @Column(name = "bushing_df_average")
    private String bushing_df_average;
    @Column(name = "bushing_c_worst")
    private String bushing_c_worst;
    @Column(name = "bushing_c_average")
    private String bushing_c_average;
    @Column(name = "condition_mois")
    private String condition_mois;
    @Column(name = "health_index")
    private String health_index;
    @Column(name = "weight_bushing_df")
    private String weight_bushing_df;
    @Column(name = "weight_bushing_c")
    private String weight_bushing_c;
    @Column(name = "weight_mois")
    private String weight_mois;

    @ManyToOne(fetch = FetchType.EAGER)
    private Asset asset;
}
