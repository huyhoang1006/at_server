package vn.automationandtesting.atproject.config.user_auth;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public class MyUsernamePasswordAuthenticationProvider implements AuthenticationProvider {
    private final MyUserDetailsService myUserDetailService;
    private final PasswordEncoder passwordEncoder;

    @Autowired
    public MyUsernamePasswordAuthenticationProvider(MyUserDetailsService myUserDetailService, PasswordEncoder passwordEncoder) {
        this.myUserDetailService = myUserDetailService;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        UsernamePasswordAuthenticationToken authenticationToken = (UsernamePasswordAuthenticationToken) authentication;
        String username = (String) authentication.getPrincipal();
        MyUserDetails myUserDetails = (MyUserDetails) myUserDetailService.loadUserByUsername(username);

        String password = (String) authenticationToken.getCredentials();
        password.trim();
        if (!passwordEncoder.matches(password, myUserDetails.getPassword())) {
            throw new BadCredentialsException("Invalid username/password.");
        }

        return new UsernamePasswordAuthenticationToken(myUserDetails, password, myUserDetails.getAuthorities());
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return UsernamePasswordAuthenticationToken.class.equals(aClass);
    }
}
