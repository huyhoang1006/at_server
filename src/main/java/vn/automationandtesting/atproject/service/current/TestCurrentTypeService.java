package vn.automationandtesting.atproject.service.current;

import vn.automationandtesting.atproject.controller.dto.current.TestCurrentTypeDto;
import vn.automationandtesting.atproject.entity.current.TestCurrentType;

import java.util.List;

public interface TestCurrentTypeService {
    List<TestCurrentTypeDto> findAll();
    List<TestCurrentTypeDto> findTestTypeByCode(String code);
    List<TestCurrentTypeDto> findTestTypeById(String code);
    void saveAll(List<TestCurrentType> testsType);

    void deleteAll(List<TestCurrentType> testsType);
}
