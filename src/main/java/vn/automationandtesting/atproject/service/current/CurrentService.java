package vn.automationandtesting.atproject.service.current;

import vn.automationandtesting.atproject.controller.dto.circuit.CircuitDto;
import vn.automationandtesting.atproject.controller.dto.current.CurrentDto;
import vn.automationandtesting.atproject.entity.current.Current;

import java.util.List;
import java.util.UUID;

public interface CurrentService {
    List<CurrentDto> findAllByLocationId(UUID location_id);

    List<CurrentDto> findAssetById(UUID id);

    List<CurrentDto> findBySerial(String serial);

    List<CurrentDto> findBySerialAndLocation(String serial, UUID location_id);

    List<CurrentDto> findAll();

    List<CurrentDto> findByLocationIdAndCollabsContaining(UUID location_id, String userId);

    void saveAll(List<Current> currents);

    void deleteAll(List<Current> currents);
}
