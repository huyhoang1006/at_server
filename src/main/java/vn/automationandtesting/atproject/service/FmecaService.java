package vn.automationandtesting.atproject.service;

import vn.automationandtesting.atproject.controller.dto.ResponseObject;
import vn.automationandtesting.atproject.controller.dto.cim.FmecaDto;
import vn.automationandtesting.atproject.entity.Fmeca;

import java.util.UUID;

/**
 * @author tridv on 19/9/2022
 * @project at-project-server
 */
public interface FmecaService {
    FmecaDto createFmeca(FmecaDto fmecaDto);

    FmecaDto updateFmeca(UUID id, FmecaDto fmecaDto);

    Fmeca getFmecaTableById(UUID fmecaId);

    ResponseObject getOrInit();
}
