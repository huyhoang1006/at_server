package vn.automationandtesting.atproject.service.power;

import vn.automationandtesting.atproject.controller.dto.disconnector.JobDisconnectorDto;
import vn.automationandtesting.atproject.controller.dto.power.JobPowerCableDto;
import vn.automationandtesting.atproject.entity.power.JobsPowerCable;

import java.util.List;
import java.util.UUID;

public interface JobPowerCableService {

    List<JobPowerCableDto> findAllJobByAssetId(UUID asset_id);

    List<JobPowerCableDto> findJobById(UUID id);

    List<JobPowerCableDto> findJobByName(String name);

    List<JobPowerCableDto> findJobByNameAndAsset(String name, UUID asset_id);

    List<JobPowerCableDto> findAll();

    List<JobPowerCableDto> findByAssetIdAndCollabsContaining(UUID asset_id, String userId);

    void saveAll(List<JobsPowerCable> jobdataList);

    void deleteAll(List<JobsPowerCable> jobdataList);
}
