package vn.automationandtesting.atproject.service.power;

import vn.automationandtesting.atproject.controller.dto.disconnector.DisconnectorDto;
import vn.automationandtesting.atproject.controller.dto.power.PowerCableDto;
import vn.automationandtesting.atproject.entity.power.PowerCable;

import java.util.List;
import java.util.UUID;

public interface PowerCableService {
    List<PowerCableDto> findAllByLocationId(UUID location_id);

    List<PowerCableDto> findAssetById(UUID id);

    List<PowerCableDto> findBySerial(String serial);

    List<PowerCableDto> findBySerialAndLocation(String serial, UUID location_id);

    List<PowerCableDto> findAll();

    List<PowerCableDto> findByLocationIdAndCollabsContaining(UUID location_id, String userId);

    void saveAll(List<PowerCable> assets);

    void deleteAll(List<PowerCable> assets);
}
