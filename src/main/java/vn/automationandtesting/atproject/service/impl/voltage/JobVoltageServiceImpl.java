package vn.automationandtesting.atproject.service.impl.voltage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vn.automationandtesting.atproject.config.auth.BearerContextHolder;
import vn.automationandtesting.atproject.controller.dto.mapper.voltage.JobVoltageMapper;
import vn.automationandtesting.atproject.controller.dto.voltage.JobVoltageDto;
import vn.automationandtesting.atproject.entity.voltage.JobsVoltage;
import vn.automationandtesting.atproject.repository.voltage.JobVoltageRepository;
import vn.automationandtesting.atproject.service.voltage.JobVoltageService;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
public class JobVoltageServiceImpl implements JobVoltageService {

    @Autowired
    private JobVoltageRepository repository;

    @Autowired
    private JobVoltageMapper mapper;

    @Override
    public List<JobVoltageDto> findAllJobByAssetId(UUID asset_id) {
        String userId = BearerContextHolder.getContext().getUserId();
        UUID user_id = UUID.fromString(userId);
        List<JobsVoltage> jobs = repository.findAllJobByAssetId(asset_id, user_id, userId);
        List<JobVoltageDto> jobsDto = new ArrayList<>();
        if(jobs.size() != 0) {
            for(JobsVoltage item : jobs) {
                JobVoltageDto jobDto = mapper.JobToJobDto(item);
                jobsDto.add(jobDto);
            }
        }
        return jobsDto;
    }

    @Override
    public List<JobVoltageDto> findJobById(UUID id) {
        List<JobsVoltage> jobs = repository.findJobById(id);
        List<JobVoltageDto> jobsDto = new ArrayList<>();
        if(jobs.size() != 0) {
            for(JobsVoltage item : jobs) {
                JobVoltageDto jobDto = mapper.JobToJobDto(item);
                jobsDto.add(jobDto);
            }
        }
        return jobsDto;
    }

    @Override
    public List<JobVoltageDto> findJobByName(String name) {
        List<JobsVoltage> jobs = repository.findJobByName(name);
        List<JobVoltageDto> jobsDto = new ArrayList<>();
        if(jobs.size() != 0) {
            for(JobsVoltage item : jobs) {
                JobVoltageDto jobDto = mapper.JobToJobDto(item);
                jobsDto.add(jobDto);
            }
        }
        return jobsDto;
    }

    @Override
    public List<JobVoltageDto> findJobByNameAndAsset(String name, UUID asset_id) {
        List<JobsVoltage> jobs = repository.findJobByNameAndAsset(name, asset_id);
        List<JobVoltageDto> jobsDto = new ArrayList<>();
        if(jobs.size() != 0) {
            for(JobsVoltage item : jobs) {
                JobVoltageDto jobDto = mapper.JobToJobDto(item);
                jobsDto.add(jobDto);
            }
        }
        return jobsDto;
    }

    @Override
    public List<JobVoltageDto> findAll() {
        List<JobsVoltage> jobs = repository.findAll();
        List<JobVoltageDto> jobsDto = new ArrayList<>();
        if(jobs.size() != 0) {
            for(JobsVoltage item : jobs) {
                JobVoltageDto jobDto = mapper.JobToJobDto(item);
                jobsDto.add(jobDto);
            }
        }
        return jobsDto;
    }

    @Override
    public List<JobVoltageDto> findByAssetIdAndCollabsContaining(UUID asset_id, String userId) {
        List<JobsVoltage> jobs = repository.findByAssetIdAndCollabsContaining(asset_id, userId);
        List<JobVoltageDto> jobsDto = new ArrayList<>();
        if(jobs.size() != 0) {
            for(JobsVoltage item : jobs) {
                JobVoltageDto jobDto = mapper.JobToJobDto(item);
                jobsDto.add(jobDto);
            }
        }
        return jobsDto;
    }

    @Override
    public void saveAll(List<JobsVoltage> jobdataList) {
        repository.saveAll(jobdataList);
    }

    @Override
    public void deleteAll(List<JobsVoltage> jobdataList) {
        repository.deleteAll(jobdataList);
    }
}
