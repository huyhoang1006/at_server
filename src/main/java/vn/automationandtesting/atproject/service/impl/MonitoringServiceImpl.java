package vn.automationandtesting.atproject.service.impl;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.configurationprocessor.json.JSONException;
import org.springframework.boot.configurationprocessor.json.JSONObject;
import org.springframework.stereotype.Service;
import vn.automationandtesting.atproject.constant.Constants;
import vn.automationandtesting.atproject.controller.dto.MonitoringDto;
import vn.automationandtesting.atproject.entity.Fmeca;
import vn.automationandtesting.atproject.entity.Monitoring;
import vn.automationandtesting.atproject.entity.cim.Asset;
import vn.automationandtesting.atproject.exception.AssetNotFoundException;
import vn.automationandtesting.atproject.exception.InvalidTestException;
import vn.automationandtesting.atproject.exception.MonitoringNotFoundException;
import vn.automationandtesting.atproject.repository.AssetRepository;
import vn.automationandtesting.atproject.repository.FmecaRepository;
import vn.automationandtesting.atproject.repository.MonitoringRepository;
import vn.automationandtesting.atproject.service.MonitoringService;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * @author tridv on 22/9/2022
 * @project at-project-server
 */
@Service
public class MonitoringServiceImpl implements MonitoringService {
    private final MonitoringRepository monitoringRepository;
    private final AssetRepository assetRepository;
    @Autowired
    private FmecaRepository fmecaRepository;


    @Autowired
    public MonitoringServiceImpl(MonitoringRepository monitoringRepository, AssetRepository assetRepository) {
        this.monitoringRepository = monitoringRepository;
        this.assetRepository = assetRepository;
    }

    @Override
    public List<MonitoringDto> findMonitoringByAssetId(String assetId) {
        UUID assetIdUUID = UUID.fromString(assetId);
        Asset asset = new Asset();
        asset.setMrid(assetIdUUID);
        List<Monitoring> monitoringList = monitoringRepository.findByAssetAndIsDeleted(asset, false);
        List<MonitoringDto> monitoringDtoList = monitoringList.stream().map(monitoring -> {
            MonitoringDto monitoringDto = new MonitoringDto();
            BeanUtils.copyProperties(monitoring, monitoringDto);
            monitoringDto.setId(monitoring.getId().toString());
            monitoringDto.setAssetId(monitoring.getAsset().getMrid().toString());
            return monitoringDto;
        }).collect(Collectors.toList());
        return monitoringDtoList;
    }

    @Override
    public List<MonitoringDto> findMonitoringdescByAssetId(String assetId) {
        UUID assetIdUUID = UUID.fromString(assetId);
        Asset asset = new Asset();
        asset.setMrid(assetIdUUID);
        List<Monitoring> monitoringList = monitoringRepository.findByAssetAndIsDeletedDesc(asset, false);
        List<MonitoringDto> monitoringDtoList = monitoringList.stream().map(monitoring -> {
            MonitoringDto monitoringDto = new MonitoringDto();
            System.out.println(monitoring);
            BeanUtils.copyProperties(monitoring, monitoringDto);
            monitoringDto.setId(monitoring.getId().toString());
            monitoringDto.setAssetId(monitoring.getAsset().getMrid().toString());
            monitoringDto.setUpdate_by(monitoring.getUpdatedOn().toString());
            monitoringDto.setUpdated_on(monitoring.getUpdatedOn().toString());
            monitoringDto.setCreated_by(monitoring.getCreatedBy().toString());
            monitoringDto.setCreated_on(monitoring.getCreatedOn().toString());
            return monitoringDto;
        }).collect(Collectors.toList());
        return monitoringDtoList;
    }

    @Override
    public List<MonitoringDto> findLastMonitoringdescByAssetId(String assetId) {
        UUID assetIdUUID = UUID.fromString(assetId);
        Asset asset = new Asset();
        asset.setMrid(assetIdUUID);
        List<Monitoring> monitoringList = monitoringRepository.findLastByAssetAndIsDeletedDesc(asset, false);
        List<MonitoringDto> monitoringDtoList = monitoringList.stream().map(monitoring -> {
            MonitoringDto monitoringDto = new MonitoringDto();
            BeanUtils.copyProperties(monitoring, monitoringDto);
            monitoringDto.setId(monitoring.getId().toString());
            monitoringDto.setAssetId(monitoring.getAsset().getMrid().toString());
            monitoringDto.setUpdate_by(monitoring.getUpdatedOn().toString());
            monitoringDto.setUpdated_on(monitoring.getUpdatedOn().toString());
            monitoringDto.setCreated_by(monitoring.getCreatedBy().toString());
            monitoringDto.setCreated_on(monitoring.getCreatedOn().toString());
            return monitoringDto;
        }).collect(Collectors.toList());
        return monitoringDtoList;
    }

    @Override
    public List<MonitoringDto> getAllMonitoring() {
        List<Monitoring> monitoringList = monitoringRepository.findByIsDeleted(false);
        List<MonitoringDto> monitoringDtoList = monitoringList.stream().map(monitoring -> {
            MonitoringDto monitoringDto = new MonitoringDto();
            BeanUtils.copyProperties(monitoring, monitoringDto);
            monitoringDto.setId(monitoring.getId().toString());
            monitoringDto.setAssetId(monitoring.getAsset().getMrid().toString());
            return monitoringDto;
        }).collect(Collectors.toList());
        return monitoringDtoList;
    }

    @Override
    public MonitoringDto findMonitoringById(String id) {
        UUID monitoringId = UUID.fromString(id);
        Monitoring monitoring = monitoringRepository.findByIdAndIsDeleted(monitoringId, false);
        if (monitoring == null) {
            throw new MonitoringNotFoundException("Monitoring not found");
        }
        MonitoringDto monitoringDto = new MonitoringDto();
        BeanUtils.copyProperties(monitoring, monitoringDto);
        monitoringDto.setId(monitoring.getId().toString());
        monitoringDto.setAssetId(monitoring.getAsset().getMrid().toString());
        return monitoringDto;
    }

    @Override
    public MonitoringDto createMonitoring(MonitoringDto monitoringDto) {
        UUID assetId = UUID.fromString(monitoringDto.getAssetId());
        Asset asset = assetRepository.findByMridAndIsDeleted(assetId, false)
                .orElseThrow(() -> new AssetNotFoundException("Asset ID not existed"));

        Monitoring monitoring = new Monitoring();
        BeanUtils.copyProperties(monitoringDto, monitoring);
        if (monitoringDto.getId() != null) {
            monitoring.setId(UUID.fromString(monitoringDto.getId()));
        } else {
            monitoring.setId(UUID.randomUUID());
        }
        monitoring.setAsset(asset);
        monitoring = addMetaData(monitoring);
        Monitoring savedMonitoring = monitoringRepository.save(monitoring);

        MonitoringDto savedMonitoringDto = new MonitoringDto();
        BeanUtils.copyProperties(savedMonitoring, savedMonitoringDto);
        savedMonitoringDto.setId(savedMonitoring.getId().toString());
        monitoringDto.setAssetId(savedMonitoring.getAsset().getMrid().toString());
        return savedMonitoringDto;
    }

    @Override
    public MonitoringDto createMonitoring(JSONObject monitorObject) throws JSONException {

        UUID assetId = UUID.fromString(monitorObject.getString("asset_id"));
        Asset asset = assetRepository.findByMridAndIsDeleted(assetId, false)
                .orElseThrow(() -> new AssetNotFoundException("Asset ID not existed"));
        Monitoring monitoring = new Monitoring();
        MonitoringDto monitoringDto = new MonitoringDto();
        monitoringDto.setAssetId(assetId.toString());
        monitoringDto.setAgeingInsulation(monitorObject.getJSONObject("ageing_insulation").toString());
        monitoringDto.setBushingsOnline(monitorObject.getJSONObject("bushings_online").toString());
        monitoringDto.setPatitalDischarge(monitorObject.getJSONObject("patital_discharge").toString());
        monitoringDto.setMoistureInsulation(monitorObject.getJSONObject("moisture_insulation").toString());
        monitoringDto.setDga(monitorObject.getJSONObject("dga").toString());
        BeanUtils.copyProperties(monitoringDto, monitoring);
        if (monitoringDto.getId() != null) {
            monitoring.setId(UUID.fromString(monitoringDto.getId()));
        } else {
            monitoring.setId(UUID.randomUUID());
        }
        monitoring.setAsset(asset);
        monitoring = addMetaData(monitoring);
        Monitoring savedMonitoring = monitoringRepository.save(monitoring);

        MonitoringDto savedMonitoringDto = new MonitoringDto();
        BeanUtils.copyProperties(savedMonitoring, savedMonitoringDto);
        savedMonitoringDto.setId(savedMonitoring.getId().toString());
        monitoringDto.setAssetId(savedMonitoring.getAsset().getMrid().toString());
        return savedMonitoringDto;
    }

    @Override
    public MonitoringDto updateMonitoring(String id, MonitoringDto monitoringDto) {

        UUID monitoringId = UUID.fromString(id);
        Monitoring monitoring = monitoringRepository.findByIdAndIsDeleted(monitoringId, false);
        BeanUtils.copyProperties(monitoringDto, monitoring, "id");
        monitoring = addMetaData(monitoring);
        Monitoring updatedMonitoring = monitoringRepository.save(monitoring);

        MonitoringDto updatedMonitoringDto = new MonitoringDto();
        BeanUtils.copyProperties(updatedMonitoring, updatedMonitoringDto);
        updatedMonitoringDto.setId(updatedMonitoring.getId().toString());
        monitoringDto.setAssetId(updatedMonitoring.getAsset().getMrid().toString());
        return updatedMonitoringDto;
    }

    @Override
    public void deleteMonitoringById(String id) {
        UUID monitoringId = UUID.fromString(id);
        Monitoring monitoring = monitoringRepository.findByIdAndIsDeleted(monitoringId, false);
        if (monitoring == null) {
            throw new MonitoringNotFoundException("Monitoring not found");
        }
        monitoringRepository.delete(monitoring);
    }

    public Monitoring addMetaData(Monitoring monitoring) {
        Monitoring monitoring_temp = monitoring;
        try {
            JSONObject jsonObject = new JSONObject(monitoring_temp.getMoistureInsulation());
            //moip
            JSONObject cal = jsonObject.getJSONObject("cal");
            Float moip = handleNullValue(String.valueOf(cal.get("moip")));

            cal.put("ci", getStatus(moip, "moip"));
            jsonObject.put("cal", cal);

            monitoring.setMoistureInsulation(jsonObject.toString());

            //Bushing Online
            jsonObject = new JSONObject(monitoring_temp.getBushingsOnline());
            JSONObject deltaDF = jsonObject.getJSONObject("deltaDF");
            JSONObject hv1 = deltaDF.getJSONObject("hv_ph1");
            JSONObject hv2 = deltaDF.getJSONObject("hv_ph2");
            JSONObject hv3 = deltaDF.getJSONObject("hv_ph3");

            Float result = handleNullValue(String.valueOf(hv1.get("result")));
            hv1.put("ci", getStatus(result, "deltaDF"));
            deltaDF.put("hv_ph1", hv1);

            result = handleNullValue(String.valueOf(hv2.get("result")));
            hv2.put("ci", getStatus(result, "deltaDF"));
            deltaDF.put("hv_ph2", hv2);

            result = handleNullValue(String.valueOf(hv3.get("result")));
            hv3.put("ci", getStatus(result, "deltaDF"));
            deltaDF.put("hv_ph3", hv3);

            jsonObject.put("deltaDF", deltaDF);

            // Delta C
            JSONObject deltaC = jsonObject.getJSONObject("deltaC");
            hv1 = deltaC.getJSONObject("hv_ph1");
            hv2 = deltaC.getJSONObject("hv_ph2");
            hv3 = deltaC.getJSONObject("hv_ph3");

            result = handleNullValue(String.valueOf(hv1.get("result")));
            hv1.put("ci", getStatus(result, "deltaC"));
            deltaC.put("hv_ph1", hv1);

            result = handleNullValue(String.valueOf(hv2.get("result")));
            hv2.put("ci", getStatus(result, "deltaC"));
            deltaC.put("hv_ph2", hv2);

            result = handleNullValue(String.valueOf(hv3.get("result")));
            hv3.put("ci", getStatus(result, "deltaC"));
            deltaC.put("hv_ph3", hv3);

            jsonObject.put("deltaC", deltaC);

            monitoring_temp.setBushingsOnline(jsonObject.toString());
        } catch (JSONException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }

        List<Fmeca> listFmeca = fmecaRepository.findAll();
        if (listFmeca.size() != 0) {
            monitoring_temp = getHealthIndex(listFmeca.get(0).getTableCalculate(), monitoring_temp);
        }
        return monitoring_temp;
    }

    private String getStatus(Float value, String type) {
        if (type.equals("moip")) {
            if (value <= 1) {
                return "Good";
            } else if (value > 1 && value <= 2) {
                return "Fair";
            } else if (value > 2 && value <= 3) {
                return "Poor";
            } else if (value > 3) {
                return "Bad";
            } else {
                return "";
            }
        } else if (type.equals("deltaDF")) {
            if (value <= 1.3) {
                return "Good";
            } else if (value <= 2 && value > 1.3) {
                return "Fair";
            } else if (value <= 3 && value > 2) {
                return "Poor";
            } else if (value > 3) {
                return "Bad";
            } else {
                return "";
            }
        } else if (type.equals("deltaC")) {
            if (value <= 5) {
                return "Good";
            } else if (value <= 7 && value > 5) {
                return "Fair";
            } else if (value <= 10 && value > 7) {
                return "Poor";
            } else if (value > 10) {
                return "Bad";
            } else {
                return "";
            }
        } else {
            return "null";
        }
    }

    public Monitoring getHealthIndex(String fmecaTableCalculate, Monitoring mornitor) {
        try {
            JSONObject jsonObject = new JSONObject(fmecaTableCalculate);
            JSONObject json = new JSONObject(mornitor.getMoistureInsulation());
            JSONObject cal = json.getJSONObject("cal");
            String moip_ci = String.valueOf(cal.get("ci"));

            Float score = Float.valueOf(setValueToIndicatorCondition(moip_ci));
            if (score == -1) {
                score = Float.valueOf(0);
            }
            mornitor.setCondition_mois(String.valueOf(score));
            String moistureContent = jsonObject.getString(Constants.FMECA_TABLE_CALCULATION_MOISTURE_CONTENT);
            mornitor.setWeight_mois(moistureContent);

            json = new JSONObject(mornitor.getBushingsOnline());
            JSONObject deltaDF = json.getJSONObject("deltaDF");
            JSONObject hv1 = deltaDF.getJSONObject("hv_ph1");
            JSONObject hv2 = deltaDF.getJSONObject("hv_ph2");
            JSONObject hv3 = deltaDF.getJSONObject("hv_ph3");

            List<Float> score_df = new ArrayList<>();
            score_df.add(Float.valueOf(setValueToIndicatorCondition(hv1.getString("ci"))));
            score_df.add(Float.valueOf(setValueToIndicatorCondition(hv2.getString("ci"))));
            score_df.add(Float.valueOf(setValueToIndicatorCondition(hv3.getString("ci"))));

            score_df = score_df.stream().filter(aFloat -> aFloat != -1).collect(Collectors.toList());
            String bushingPfDf = jsonObject.getString(Constants.FMECA_TABLE_CALCULATION_BUSHING_PF_DF);
            mornitor.setWeight_bushing_df(bushingPfDf);
            Float worstDFScore = score_df == null || score_df.isEmpty() ? Float.valueOf(0) : Collections.min(score_df);
            Float averageDFScore = (float) score_df.stream().mapToDouble(f -> f).average().orElse(0);

            mornitor.setBushing_df_worst(String.valueOf(worstDFScore));
            mornitor.setBushing_df_average(String.valueOf(averageDFScore));

            json = new JSONObject(mornitor.getBushingsOnline());
            JSONObject deltaC = json.getJSONObject("deltaC");
            hv1 = deltaC.getJSONObject("hv_ph1");
            hv2 = deltaC.getJSONObject("hv_ph2");
            hv3 = deltaC.getJSONObject("hv_ph3");

            List<Float> score_c = new ArrayList<>();
            score_c.add(Float.valueOf(setValueToIndicatorCondition(hv1.getString("ci"))));
            score_c.add(Float.valueOf(setValueToIndicatorCondition(hv2.getString("ci"))));
            score_c.add(Float.valueOf(setValueToIndicatorCondition(hv3.getString("ci"))));
            score_c = score_df.stream().filter(aFloat -> aFloat != -1).collect(Collectors.toList());

            String bushingC = jsonObject.getString(Constants.FMECA_TABLE_CALCULATION_BUSHING_C);
            mornitor.setWeight_bushing_c(bushingC);
            Float worstCScore = score_c == null || score_c.isEmpty() ? Float.valueOf(0) : Collections.min(score_c);
            Float averageCScore = (float) score_c.stream().mapToDouble(f -> f).average().orElse(0);

            mornitor.setBushing_c_worst(String.valueOf(worstCScore));
            mornitor.setBushing_c_average(String.valueOf(averageCScore));


        } catch (JSONException e) {
            throw new InvalidTestException("Invalid fmeca data");
        }
        return mornitor;
    }

    private Float handleNullValue(@NotNull String value) {
        if (value.isEmpty()) {
            return Float.NaN;
        } else {
            return Float.valueOf(value);
        }
    }

    private int setValueToIndicatorCondition(String sign) {
        if (sign.equalsIgnoreCase("Good")) {
            return 3;
        } else if (sign.equalsIgnoreCase("Fair")) {
            return 2;
        } else if (sign.equalsIgnoreCase("Poor")) {
            return 1;
        } else if (sign.equalsIgnoreCase("Bad")) {
            return 0;
        } else {
            return -1;
        }
    }

}
