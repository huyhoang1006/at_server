package vn.automationandtesting.atproject.service.impl.disconnector;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vn.automationandtesting.atproject.config.auth.BearerContextHolder;
import vn.automationandtesting.atproject.controller.dto.disconnector.DisconnectorDto;
import vn.automationandtesting.atproject.controller.dto.mapper.disconnector.DisconnectorMapper;
import vn.automationandtesting.atproject.entity.disconnector.Disconnector;
import vn.automationandtesting.atproject.repository.disconnector.DisconnectorRepository;
import vn.automationandtesting.atproject.service.disconnector.DisconnectorService;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
public class DisconnectorServiceImpl implements DisconnectorService {

    @Autowired
    private DisconnectorRepository repository;

    @Autowired
    private DisconnectorMapper mapper;


    @Override
    public List<DisconnectorDto> findAllByLocationId(UUID location_id) {
        String userId = BearerContextHolder.getContext().getUserId();
        UUID user_id = UUID.fromString(userId);
        List<Disconnector> list = repository.findAllByLocationId(location_id, user_id, userId);
        List<DisconnectorDto> dtoList = new ArrayList<>();
        if(!list.isEmpty()) {
            for(Disconnector item : list) {
                DisconnectorDto itemDto = mapper.assetToAssetDto(item);
                dtoList.add(itemDto);
            }
        }
        return dtoList;
    }

    @Override
    public List<DisconnectorDto> findAssetById(UUID id) {
        List<Disconnector> list = repository.findAssetById(id);
        List<DisconnectorDto> dtoList = new ArrayList<>();
        if(!list.isEmpty()) {
            for(Disconnector item : list) {
                DisconnectorDto itemDto = mapper.assetToAssetDto(item);
                dtoList.add(itemDto);
            }
        }
        return dtoList;
    }

    @Override
    public List<DisconnectorDto> findBySerial(String serial) {
        List<Disconnector> list = repository.findBySerial(serial);
        List<DisconnectorDto> dtoList = new ArrayList<>();
        if(!list.isEmpty()) {
            for(Disconnector item : list) {
                DisconnectorDto itemDto = mapper.assetToAssetDto(item);
                dtoList.add(itemDto);
            }
        }
        return dtoList;
    }

    @Override
    public List<DisconnectorDto> findBySerialAndLocation(String serial, UUID location_id) {
        List<Disconnector> list = repository.findBySerialAndLocation(serial, location_id);
        List<DisconnectorDto> dtoList = new ArrayList<>();
        if(!list.isEmpty()) {
            for(Disconnector item : list) {
                DisconnectorDto itemDto = mapper.assetToAssetDto(item);
                dtoList.add(itemDto);
            }
        }
        return dtoList;
    }

    @Override
    public List<DisconnectorDto> findAll() {
        List<Disconnector> list = repository.findAll();
        List<DisconnectorDto> dtoList = new ArrayList<>();
        if(!list.isEmpty()) {
            for(Disconnector item : list) {
                DisconnectorDto itemDto = mapper.assetToAssetDto(item);
                dtoList.add(itemDto);
            }
        }
        return dtoList;
    }

    @Override
    public List<DisconnectorDto> findByLocationIdAndCollabsContaining(UUID location_id, String userId) {
        List<Disconnector> list = repository.findByLocationIdAndCollabsContaining(location_id, userId);
        List<DisconnectorDto> dtoList = new ArrayList<>();
        if(!list.isEmpty()) {
            for(Disconnector item : list) {
                DisconnectorDto itemDto = mapper.assetToAssetDto(item);
                dtoList.add(itemDto);
            }
        }
        return dtoList;
    }

    @Override
    public void saveAll(List<Disconnector> list) {
        repository.saveAll(list);
    }

    @Override
    public void deleteAll(List<Disconnector> list) {
        repository.deleteAll(list);
    }
}
