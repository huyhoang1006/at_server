package vn.automationandtesting.atproject.service.impl.voltage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vn.automationandtesting.atproject.controller.dto.mapper.voltage.TestVoltageMapper;
import vn.automationandtesting.atproject.controller.dto.voltage.TestVoltageDto;
import vn.automationandtesting.atproject.entity.voltage.TestsVoltage;
import vn.automationandtesting.atproject.repository.voltage.TestVoltageRepository;
import vn.automationandtesting.atproject.service.voltage.TestVoltageService;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
public class TestVoltageServiceImpl implements TestVoltageService {

    @Autowired
    private TestVoltageRepository repository;

    @Autowired
    private TestVoltageMapper mapper;


    @Override
    public List<TestVoltageDto> findAllTestByJobId(UUID job_id) {
        List<TestsVoltage> tests = repository.findAllTestByJobId(job_id);
        List<TestVoltageDto> testsDto = new ArrayList<>();
        if(tests.size() != 0) {
            for(TestsVoltage item : tests) {
                TestVoltageDto testDto = mapper.testToTestDto(item);
                testsDto.add(testDto);
            }
        }
        return testsDto;
    }

    @Override
    public List<TestVoltageDto> findTestById(UUID id) {
        List<TestsVoltage> tests = repository.findTestById(id);
        List<TestVoltageDto> testsDto = new ArrayList<>();
        if(tests.size() != 0) {
            for(TestsVoltage item : tests) {
                TestVoltageDto testDto = mapper.testToTestDto(item);
                testsDto.add(testDto);
            }
        }
        return testsDto;
    }

    @Override
    public List<TestVoltageDto> findTestByType(UUID type_id) {
        List<TestsVoltage> tests = repository.findTestByType(type_id);
        List<TestVoltageDto> testsDto = new ArrayList<>();
        if(tests.size() != 0) {
            for(TestsVoltage item : tests) {
                TestVoltageDto testDto = mapper.testToTestDto(item);
                testsDto.add(testDto);
            }
        }
        return testsDto;
    }

    @Override
    public List<TestVoltageDto> findTestByTypeAndAsset(UUID type_id, UUID asset_id) {
        List<TestsVoltage> tests = repository.findTestByTypeAndAsset(type_id, asset_id);
        List<TestVoltageDto> testsDto = new ArrayList<>();
        if(tests.size() != 0) {
            for(TestsVoltage item : tests) {
                TestVoltageDto testDto = mapper.testToTestDto(item);
                testsDto.add(testDto);
            }
        }
        return testsDto;
    }

    @Override
    public void saveAll(List<TestsVoltage> tests) {
        repository.saveAll(tests);
    }

    @Override
    public void deleteAll(List<TestsVoltage> tests) {
        repository.deleteAll(tests);
    }
}
