package vn.automationandtesting.atproject.service.impl.current;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vn.automationandtesting.atproject.config.auth.BearerContextHolder;
import vn.automationandtesting.atproject.controller.dto.current.CurrentDto;
import vn.automationandtesting.atproject.controller.dto.mapper.current.CurrentMapper;
import vn.automationandtesting.atproject.entity.current.Current;
import vn.automationandtesting.atproject.repository.current.CurrentRepository;
import vn.automationandtesting.atproject.service.current.CurrentService;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
public class CurrentServiceImpl implements CurrentService {

    @Autowired
    private CurrentRepository repository;

    @Autowired
    private CurrentMapper mapper;


    @Override
    public List<CurrentDto> findAllByLocationId(UUID location_id) {
        String userId = BearerContextHolder.getContext().getUserId();
        UUID user_id = UUID.fromString(userId);
        List<Current> list = repository.findAllByLocationId(location_id, user_id, userId);
        List<CurrentDto> dtoList = new ArrayList<>();
        if(!list.isEmpty()) {
            for(Current item : list) {
                CurrentDto itemDto = mapper.assetToAssetDto(item);
                dtoList.add(itemDto);
            }
        }
        return dtoList;
    }

    @Override
    public List<CurrentDto> findAssetById(UUID id) {
        List<Current> list = repository.findAssetById(id);
        List<CurrentDto> dtoList = new ArrayList<>();
        if(!list.isEmpty()) {
            for(Current item : list) {
                CurrentDto itemDto = mapper.assetToAssetDto(item);
                dtoList.add(itemDto);
            }
        }
        return dtoList;
    }

    @Override
    public List<CurrentDto> findBySerial(String serial) {
        List<Current> list = repository.findBySerial(serial);
        List<CurrentDto> dtoList = new ArrayList<>();
        if(!list.isEmpty()) {
            for(Current item : list) {
                CurrentDto itemDto = mapper.assetToAssetDto(item);
                dtoList.add(itemDto);
            }
        }
        return dtoList;
    }

    @Override
    public List<CurrentDto> findBySerialAndLocation(String serial, UUID location_id) {
        List<Current> list = repository.findBySerialAndLocation(serial, location_id);
        List<CurrentDto> dtoList = new ArrayList<>();
        if(!list.isEmpty()) {
            for(Current item : list) {
                CurrentDto itemDto = mapper.assetToAssetDto(item);
                dtoList.add(itemDto);
            }
        }
        return dtoList;
    }

    @Override
    public List<CurrentDto> findAll() {
        List<Current> list = repository.findAll();
        List<CurrentDto> dtoList = new ArrayList<>();
        if(!list.isEmpty()) {
            for(Current item : list) {
                CurrentDto itemDto = mapper.assetToAssetDto(item);
                dtoList.add(itemDto);
            }
        }
        return dtoList;
    }

    @Override
    public List<CurrentDto> findByLocationIdAndCollabsContaining(UUID location_id, String userId) {
        List<Current> list = repository.findByLocationIdAndCollabsContaining(location_id, userId);
        List<CurrentDto> dtoList = new ArrayList<>();
        if(!list.isEmpty()) {
            for(Current item : list) {
                CurrentDto itemDto = mapper.assetToAssetDto(item);
                dtoList.add(itemDto);
            }
        }
        return dtoList;
    }

    @Override
    public void saveAll(List<Current> list) {
        repository.saveAll(list);
    }

    @Override
    public void deleteAll(List<Current> list) {
        repository.deleteAll(list);
    }
}
