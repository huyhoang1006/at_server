package vn.automationandtesting.atproject.service.impl.voltage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vn.automationandtesting.atproject.config.auth.BearerContextHolder;
import vn.automationandtesting.atproject.controller.dto.mapper.voltage.VoltageMapper;
import vn.automationandtesting.atproject.controller.dto.voltage.VoltageDto;
import vn.automationandtesting.atproject.entity.voltage.Voltage;
import vn.automationandtesting.atproject.repository.voltage.VoltageRepository;
import vn.automationandtesting.atproject.service.voltage.VoltageService;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
public class VoltageServiceImpl implements VoltageService {

    @Autowired
    private VoltageRepository repository;

    @Autowired
    private VoltageMapper mapper;


    @Override
    public List<VoltageDto> findAllByLocationId(UUID location_id) {
        String userId = BearerContextHolder.getContext().getUserId();
        UUID user_id = UUID.fromString(userId);
        List<Voltage> list = repository.findAllByLocationId(location_id, user_id, userId);
        List<VoltageDto> dtoList = new ArrayList<>();
        if(!list.isEmpty()) {
            for(Voltage item : list) {
                VoltageDto itemDto = mapper.assetToAssetDto(item);
                dtoList.add(itemDto);
            }
        }
        return dtoList;
    }

    @Override
    public List<VoltageDto> findAssetById(UUID id) {
        List<Voltage> list = repository.findAssetById(id);
        List<VoltageDto> dtoList = new ArrayList<>();
        if(!list.isEmpty()) {
            for(Voltage item : list) {
                VoltageDto itemDto = mapper.assetToAssetDto(item);
                dtoList.add(itemDto);
            }
        }
        return dtoList;
    }

    @Override
    public List<VoltageDto> findBySerial(String serial) {
        List<Voltage> list = repository.findBySerial(serial);
        List<VoltageDto> dtoList = new ArrayList<>();
        if(!list.isEmpty()) {
            for(Voltage item : list) {
                VoltageDto itemDto = mapper.assetToAssetDto(item);
                dtoList.add(itemDto);
            }
        }
        return dtoList;
    }

    @Override
    public List<VoltageDto> findBySerialAndLocation(String serial, UUID location_id) {
        List<Voltage> list = repository.findBySerialAndLocation(serial, location_id);
        List<VoltageDto> dtoList = new ArrayList<>();
        if(!list.isEmpty()) {
            for(Voltage item : list) {
                VoltageDto itemDto = mapper.assetToAssetDto(item);
                dtoList.add(itemDto);
            }
        }
        return dtoList;
    }

    @Override
    public List<VoltageDto> findAll() {
        List<Voltage> list = repository.findAll();
        List<VoltageDto> dtoList = new ArrayList<>();
        if(!list.isEmpty()) {
            for(Voltage item : list) {
                VoltageDto itemDto = mapper.assetToAssetDto(item);
                dtoList.add(itemDto);
            }
        }
        return dtoList;
    }

    @Override
    public List<VoltageDto> findByLocationIdAndCollabsContaining(UUID location_id, String userId) {
        List<Voltage> list = repository.findByLocationIdAndCollabsContaining(location_id, userId);
        List<VoltageDto> dtoList = new ArrayList<>();
        if(!list.isEmpty()) {
            for(Voltage item : list) {
                VoltageDto itemDto = mapper.assetToAssetDto(item);
                dtoList.add(itemDto);
            }
        }
        return dtoList;
    }

    @Override
    public void saveAll(List<Voltage> list) {
        repository.saveAll(list);
    }

    @Override
    public void deleteAll(List<Voltage> list) {
        repository.deleteAll(list);
    }
}
