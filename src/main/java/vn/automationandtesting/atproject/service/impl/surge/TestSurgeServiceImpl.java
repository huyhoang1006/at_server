package vn.automationandtesting.atproject.service.impl.surge;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vn.automationandtesting.atproject.controller.dto.mapper.surge.TestSurgeMapper;
import vn.automationandtesting.atproject.controller.dto.surge.TestSurgeDto;
import vn.automationandtesting.atproject.entity.surge.TestsSurge;
import vn.automationandtesting.atproject.repository.surge.TestSurgeRepository;
import vn.automationandtesting.atproject.service.surge.TestSurgeService;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
public class TestSurgeServiceImpl implements TestSurgeService {

    @Autowired
    private TestSurgeRepository repository;

    @Autowired
    private TestSurgeMapper mapper;


    @Override
    public List<TestSurgeDto> findAllTestByJobId(UUID job_id) {
        List<TestsSurge> tests = repository.findAllTestByJobId(job_id);
        List<TestSurgeDto> testsDto = new ArrayList<>();
        if(tests.size() != 0) {
            for(TestsSurge item : tests) {
                TestSurgeDto testDto = mapper.testToTestDto(item);
                testsDto.add(testDto);
            }
        }
        return testsDto;
    }

    @Override
    public List<TestSurgeDto> findTestById(UUID id) {
        List<TestsSurge> tests = repository.findTestById(id);
        List<TestSurgeDto> testsDto = new ArrayList<>();
        if(tests.size() != 0) {
            for(TestsSurge item : tests) {
                TestSurgeDto testDto = mapper.testToTestDto(item);
                testsDto.add(testDto);
            }
        }
        return testsDto;
    }

    @Override
    public List<TestSurgeDto> findTestByType(UUID type_id) {
        List<TestsSurge> tests = repository.findTestByType(type_id);
        List<TestSurgeDto> testsDto = new ArrayList<>();
        if(tests.size() != 0) {
            for(TestsSurge item : tests) {
                TestSurgeDto testDto = mapper.testToTestDto(item);
                testsDto.add(testDto);
            }
        }
        return testsDto;
    }

    @Override
    public List<TestSurgeDto> findTestByTypeAndAsset(UUID type_id, UUID asset_id) {
        List<TestsSurge> tests = repository.findTestByTypeAndAsset(type_id, asset_id);
        List<TestSurgeDto> testsDto = new ArrayList<>();
        if(tests.size() != 0) {
            for(TestsSurge item : tests) {
                TestSurgeDto testDto = mapper.testToTestDto(item);
                testsDto.add(testDto);
            }
        }
        return testsDto;
    }

    @Override
    public void saveAll(List<TestsSurge> tests) {
        repository.saveAll(tests);
    }

    @Override
    public void deleteAll(List<TestsSurge> tests) {
        repository.deleteAll(tests);
    }
}
