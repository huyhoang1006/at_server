package vn.automationandtesting.atproject.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.configurationprocessor.json.JSONArray;
import org.springframework.boot.configurationprocessor.json.JSONException;
import org.springframework.boot.configurationprocessor.json.JSONObject;
import org.springframework.stereotype.Service;
import vn.automationandtesting.atproject.constant.Constants;
import vn.automationandtesting.atproject.controller.dto.ResponseObject;
import vn.automationandtesting.atproject.controller.dto.cim.TestDto;
import vn.automationandtesting.atproject.controller.dto.mapper.TestMapper;
import vn.automationandtesting.atproject.controller.dto.response.HealthIndexResponseDto;
import vn.automationandtesting.atproject.entity.Fmeca;
import vn.automationandtesting.atproject.entity.cim.Job;
import vn.automationandtesting.atproject.entity.cim.Test;
import vn.automationandtesting.atproject.entity.cim.TestType;
import vn.automationandtesting.atproject.entity.enumm.IndicatorEnum;
import vn.automationandtesting.atproject.entity.enumm.TestTypeEnum;
import vn.automationandtesting.atproject.exception.InvalidTestException;
import vn.automationandtesting.atproject.exception.JobNotFoundException;
import vn.automationandtesting.atproject.exception.TestNotFoundException;
import vn.automationandtesting.atproject.repository.FmecaRepository;
import vn.automationandtesting.atproject.repository.JobRepository;
import vn.automationandtesting.atproject.repository.TestRepository;
import vn.automationandtesting.atproject.repository.TestTypeRepository;
import vn.automationandtesting.atproject.service.TestService;

import java.util.*;
import java.util.stream.Collectors;

@Service
@Slf4j
public class TestServiceImpl implements TestService {

    @Autowired
    private TestRepository testRepository;

    @Autowired
    private TestMapper testMapper;

    @Autowired
    private JobRepository jobRepository;

    @Autowired
    private TestTypeRepository testTypeRepository;

    @Autowired
    private FmecaRepository fmecaRepository;

    @Override
    public TestDto createNewTest(TestDto testDto) {
        //calculate indicator
        String calculatedData = updateIndicatorStatus(testDto.getData());
        testDto.setData(calculatedData);

        Job job = jobRepository.findByIdAndIsDeleted(testDto.getJob_id(), false)
                .orElseThrow(() -> new TestNotFoundException("Job id not existed."));
        TestTypeEnum testTypeEnum = TestTypeEnum.valueOf(testDto.getTestTypeCode());
        TestType testType = testTypeRepository.findByCodeAndIsDeleted(testTypeEnum.name(), false);
        Test test = testMapper.testDtoToTest(testDto);
        if (testDto.getId() != null) {
            test.setId(testDto.getId());
        } else {
            test.setId(UUID.randomUUID());
        }
        test.setJob(job);
        test.setTestType(testType);
        //calculate score
        Map<String, Float> dictionary_worst = calculateWorstScore(test.getData());
        Map<String, Float> dictionary_average = calculateAverageScore(test.getData());

        Float worstScore = dictionary_worst.get("worstScore");
        Float worstDFScore = dictionary_worst.get("worstDFScore");
        Float worstCScore = dictionary_worst.get("worstCScore");

        Float averageScore = dictionary_average.get("averageScore");
        Float averageDFScore = dictionary_average.get("averageDFScore");
        Float averageCScore = dictionary_average.get("averageCScore");

        test.setAverageScore(averageScore);
        test.setAverageScoreDF(averageDFScore);
        test.setAverageScoreC(averageCScore);

        test.setWorstScore(worstScore);
        test.setWorstScoreDF(worstDFScore);
        test.setWorstScoreC(worstCScore);

        test = testRepository.save(test);
        return testMapper.testToTestDto(test);
    }

    @Override
    public void addMetaData(Test test) {
        // calculate indicator
        String calculatedData = updateIndicatorStatus(test.getData());
        test.setData(calculatedData);
        // calculate score
        Map<String, Float> dictionary_worst = calculateWorstScore(test.getData());
        Map<String, Float> dictionary_average = calculateAverageScore(test.getData());

        Float worstScore = dictionary_worst.get("worstScore");
        Float worstDFScore = dictionary_worst.get("worstDFScore");
        Float worstCScore = dictionary_worst.get("worstCScore");

        Float averageScore = dictionary_average.get("averageScore");
        Float averageDFScore = dictionary_average.get("averageDFScore");
        Float averageCScore = dictionary_average.get("averageCScore");

        test.setAverageScore(averageScore);
        test.setAverageScoreDF(averageDFScore);
        test.setAverageScoreC(averageCScore);

        test.setWorstScore(worstScore);
        test.setWorstScoreDF(worstDFScore);
        test.setWorstScoreC(worstCScore);
    }

    @Override
    public ResponseObject calculate(List<TestDto> listTestDto) {
        for (TestDto testDto : listTestDto) {
            String calculatedData = updateIndicatorStatus(testDto.getData());
            testDto.setData(calculatedData);
            // calculate score

            Map<String, Float> dictionary_worst = calculateWorstScore(testDto.getData());
            Map<String, Float> dictionary_average = calculateAverageScore(testDto.getData());

            Float worstScore = dictionary_worst.get("worstScore");
            Float worstDFScore = dictionary_worst.get("worstDFScore");
            Float worstCScore = dictionary_worst.get("worstCScore");

            Float averageScore = dictionary_average.get("averageScore");
            Float averageDFScore = dictionary_average.get("averageDFScore");
            Float averageCScore = dictionary_average.get("averageCScore");

            testDto.setAverage_score(averageScore);
            testDto.setAverage_score_df(averageDFScore);
            testDto.setAverage_score_c(averageCScore);

            testDto.setWorst_score(worstScore);
            testDto.setWorst_score_df(worstDFScore);
            testDto.setWorst_score_c(worstCScore);
        }

        List<Fmeca> listFmeca = fmecaRepository.findAll();
        if (listFmeca.size() == 0) {
            return ResponseObject.returnWithError("Please upload fmeca");
        }

        try {
            float averageHealthIndex = 0;
            float worstHealthIndex = 0;
            JSONObject jsonObject = new JSONObject(listFmeca.get(0).getTableCalculate());
            for (TestDto testDto : listTestDto) {
                float weightingFactor = Float
                        .parseFloat(jsonObject.getJSONObject(testDto.getTestTypeCode().toLowerCase())
                                .getString(Constants.WEIGHTING_FACTOR));
                testDto.setWeighting_factor(weightingFactor);

                Float totalAverageHealthIndex = weightingFactor * testDto.getAverage_score();
                averageHealthIndex += totalAverageHealthIndex;
                testDto.setTotal_average_score(totalAverageHealthIndex);

                Float totalWorstHealthIndex = weightingFactor * testDto.getWorst_score();
                worstHealthIndex += totalWorstHealthIndex;
                testDto.setTotal_worst_score(totalWorstHealthIndex);
            }

            return ResponseObject.returnWithData(new HealthIndexResponseDto(averageHealthIndex, worstHealthIndex));
        } catch (Exception e) {
            // TODO: handle exception
            return ResponseObject.returnWithError("Error");
        }
    }

    @Override
    public TestDto updateTest(TestDto testDto, UUID id) {
        //calculate indicator
        String calculatedData = updateIndicatorStatus(testDto.getData());
        testDto.setData(calculatedData);

        Test test = testRepository.findById(id)
                .orElseThrow(() -> new TestNotFoundException("Test id not existed"));
        test = testMapper.copyTestDtoToTest(testDto, test, "id");
        TestType testType = testTypeRepository.findByCodeAndIsDeleted(TestTypeEnum.valueOf(testDto.getTestTypeCode()).name(), false);
        test.setTestType(testType);
        Job job = jobRepository.findByIdAndIsDeleted(testDto.getJob_id(), false)
                .orElseThrow(() -> new JobNotFoundException("Job id not existed"));
        test.setJob(job);
        //calculate score
        Map<String, Float> dictionary_worst = calculateWorstScore(test.getData());
        Map<String, Float> dictionary_average = calculateAverageScore(test.getData());

        Float worstScore = dictionary_worst.get("worstScore");
        Float worstDFScore = dictionary_worst.get("worstDFScore");
        Float worstCScore = dictionary_worst.get("worstCScore");

        Float averageScore = dictionary_average.get("averageScore");
        Float averageDFScore = dictionary_average.get("averageDFScore");
        Float averageCScore = dictionary_average.get("averageCScore");

        test.setAverageScore(averageScore);
        test.setAverageScoreDF(averageDFScore);
        test.setAverageScoreC(averageCScore);

        test.setWorstScore(worstScore);
        test.setWorstScoreDF(worstDFScore);
        test.setWorstScoreC(worstCScore);

        Test updateTest = testRepository.save(test);
        return testMapper.testToTestDto(updateTest);
    }

    @Override
    public void deleteTest(UUID id) {
        Test test = testRepository.findByIdAndIsDeleted(id, false)
                .orElseThrow(() -> new TestNotFoundException("Test id not existed."));
        test.setIsDeleted(true);
        testRepository.delete(test);
    }

    @Override
    public List<TestDto> getAllTests() {
        List<TestDto> testDtoList = new ArrayList<>();
        List<Test> testList = testRepository.findAllByIsDeleted(false);
        for (Test test : testList) {
            TestDto testDto = testMapper.testToTestDto(test);
            testDtoList.add(testDto);
        }
        return testDtoList;
    }

    @Override
    public List<TestDto> findAllTestsByJobName(String jobName) {
        List<TestDto> testDtoList = new ArrayList<>();
        List<Test> testList = testRepository.findByJobName(jobName, false);
        for (Test test : testList) {
            TestDto testDto = testMapper.testToTestDto(test);
            testDtoList.add(testDto);
        }
        return testDtoList;
    }

    @Override
    public List<TestDto> findAllByJobId(String jobID) {
        List<TestDto> testDtoList = new ArrayList<>();
        List<Test> testList = testRepository.findAllByJobId(UUID.fromString(jobID));
        for (Test test : testList) {
            TestDto testDto = testMapper.testToTestDto(test);
            testDtoList.add(testDto);

        }
        return testDtoList;
    }

    @Override
    public List<TestDto> findAllTestsByCodeType(String codeType) {
        List<TestDto> testDtoList = new ArrayList<>();
        List<Test> testList = testRepository.findByCodeType(codeType, false);
        for (Test test : testList) {
            TestDto testDto = testMapper.testToTestDto(test);
            testDtoList.add(testDto);
        }
        return testDtoList;
    }

    @Override
    public List<TestDto> findAllTestsByAssetSerialNo(String serialNo) {
        List<TestDto> testDtoList = new ArrayList<>();
        List<Test> testList = testRepository.findByAssetSerialNo(serialNo, false);
        for (Test test : testList) {
            TestDto testDto = testMapper.testToTestDto(test);
            testDtoList.add(testDto);
        }
        return testDtoList;
    }

    private String updateIndicatorStatus(String data) {
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(data);
            if (TestTypeEnum.INSULATIONRESISTANCE.name().equalsIgnoreCase(jsonObject.getString(Constants.DATA_CODE_PARAM))) {
                JSONObject conditionIndicatorParam = jsonObject.getJSONObject(Constants.DATA_CONDITION_INDICATOR_SETTING_PARAM);

                Float good_kht_upper_limit = Float.valueOf((String) conditionIndicatorParam.getJSONObject(Constants.DATA_GOOD_PARAM).getJSONArray(Constants.DATA_KHT_PARAM).get(1));
                Float fair_kht_upper_limit = Float.valueOf((String) conditionIndicatorParam.getJSONObject(Constants.DATA_FAIR_PARAM).getJSONArray(Constants.DATA_KHT_PARAM).get(1));
                Float fair_kht_lower_limit = Float.valueOf((String) conditionIndicatorParam.getJSONObject(Constants.DATA_FAIR_PARAM).getJSONArray(Constants.DATA_KHT_PARAM).get(0));
                Float poor_kht_upper_limit = Float.valueOf((String) conditionIndicatorParam.getJSONObject(Constants.DATA_POOR_PARAM).getJSONArray(Constants.DATA_KHT_PARAM).get(1));
                Float poor_kht_lower_limit = Float.valueOf((String) conditionIndicatorParam.getJSONObject(Constants.DATA_POOR_PARAM).getJSONArray(Constants.DATA_KHT_PARAM).get(0));
                Float bad_kht_lower_limit = Float.valueOf((String) conditionIndicatorParam.getJSONObject(Constants.DATA_BAD_PARAM).getJSONArray(Constants.DATA_KHT_PARAM).get(0));

                Float good_r60_hve_upper_limit = Float.valueOf((String) conditionIndicatorParam.getJSONObject(Constants.DATA_GOOD_PARAM).getJSONArray(Constants.DATA_R60S_HVE_PARAM).get(1));
                Float fair_r60_hve_upper_limit = Float.valueOf((String) conditionIndicatorParam.getJSONObject(Constants.DATA_FAIR_PARAM).getJSONArray(Constants.DATA_R60S_HVE_PARAM).get(1));
                Float fair_r60_hve_lower_limit = Float.valueOf((String) conditionIndicatorParam.getJSONObject(Constants.DATA_FAIR_PARAM).getJSONArray(Constants.DATA_R60S_HVE_PARAM).get(0));
                Float poor_r60_hve_upper_limit = Float.valueOf((String) conditionIndicatorParam.getJSONObject(Constants.DATA_POOR_PARAM).getJSONArray(Constants.DATA_R60S_HVE_PARAM).get(1));
                Float poor_r60_hve_lower_limit = Float.valueOf((String) conditionIndicatorParam.getJSONObject(Constants.DATA_POOR_PARAM).getJSONArray(Constants.DATA_R60S_HVE_PARAM).get(0));
                Float bad_r60_hve_lower_limit = Float.valueOf((String) conditionIndicatorParam.getJSONObject(Constants.DATA_BAD_PARAM).getJSONArray(Constants.DATA_R60S_HVE_PARAM).get(0));

                Float good_r60_lve_upper_limit = Float.valueOf((String) conditionIndicatorParam.getJSONObject(Constants.DATA_GOOD_PARAM).getJSONArray(Constants.DATA_R60S_LVE_PARAM).get(1));
                Float fair_r60_lve_upper_limit = Float.valueOf((String) conditionIndicatorParam.getJSONObject(Constants.DATA_FAIR_PARAM).getJSONArray(Constants.DATA_R60S_LVE_PARAM).get(1));
                Float fair_r60_lve_lower_limit = Float.valueOf((String) conditionIndicatorParam.getJSONObject(Constants.DATA_FAIR_PARAM).getJSONArray(Constants.DATA_R60S_LVE_PARAM).get(0));
                Float poor_r60_lve_upper_limit = Float.valueOf((String) conditionIndicatorParam.getJSONObject(Constants.DATA_POOR_PARAM).getJSONArray(Constants.DATA_R60S_LVE_PARAM).get(1));
                Float poor_r60_lve_lower_limit = Float.valueOf((String) conditionIndicatorParam.getJSONObject(Constants.DATA_POOR_PARAM).getJSONArray(Constants.DATA_R60S_LVE_PARAM).get(0));
                Float bad_r60_lve_lower_limit = Float.valueOf((String) conditionIndicatorParam.getJSONObject(Constants.DATA_BAD_PARAM).getJSONArray(Constants.DATA_R60S_LVE_PARAM).get(0));

                JSONArray tableJsonArray = jsonObject.getJSONArray(Constants.DATA_TABLE_PARAM);

                updateIndicatorStatusInsulationRes(tableJsonArray,
                        good_kht_upper_limit, fair_kht_upper_limit, fair_kht_lower_limit, poor_kht_upper_limit, poor_kht_lower_limit, bad_kht_lower_limit,
                        good_r60_hve_upper_limit, fair_r60_hve_upper_limit, fair_r60_hve_lower_limit, poor_r60_hve_upper_limit, poor_r60_hve_lower_limit, bad_r60_hve_lower_limit,
                        good_r60_lve_upper_limit, fair_r60_lve_upper_limit, fair_r60_lve_lower_limit, poor_r60_lve_upper_limit, poor_r60_lve_lower_limit, bad_r60_lve_lower_limit,
                        Constants.TABLE_CONDITION_INDICATOR_PARAM, Constants.DATA_KHT_PARAM, Constants.DATA_R60S_PARAM);

            } else if (TestTypeEnum.WINDINGDFCAP.name().equalsIgnoreCase(jsonObject.getString(Constants.DATA_CODE_PARAM))) {
                //condition_indicator_df
                JSONObject conditionIndicatorDFParam = jsonObject.getJSONObject(Constants.DATA_CONDITION_INDICATOR_DF_PARAM);

                Float good_df_upper_limit = Float.valueOf((String) conditionIndicatorDFParam.getJSONObject(Constants.DATA_GOOD_PARAM).getJSONArray(Constants.DATA_DF_MEAS_PARAM).get(0));
                Float fair_df_lower_limit = Float.valueOf((String) conditionIndicatorDFParam.getJSONObject(Constants.DATA_FAIR_PARAM).getJSONArray(Constants.DATA_DF_MEAS_PARAM).get(0));
                Float fair_df_upper_limit = Float.valueOf((String) conditionIndicatorDFParam.getJSONObject(Constants.DATA_FAIR_PARAM).getJSONArray(Constants.DATA_DF_MEAS_PARAM).get(1));
                Float poor_df_lower_limit = Float.valueOf((String) conditionIndicatorDFParam.getJSONObject(Constants.DATA_POOR_PARAM).getJSONArray(Constants.DATA_DF_MEAS_PARAM).get(0));
                Float poor_df_upper_limit = Float.valueOf((String) conditionIndicatorDFParam.getJSONObject(Constants.DATA_POOR_PARAM).getJSONArray(Constants.DATA_DF_MEAS_PARAM).get(1));
                Float bad_df_lower_limit = Float.valueOf((String) conditionIndicatorDFParam.getJSONObject(Constants.DATA_BAD_PARAM).getJSONArray(Constants.DATA_DF_MEAS_PARAM).get(1));

                //condition_indicator_c
                JSONObject conditionIndicatorCParam = jsonObject.getJSONObject(Constants.DATA_CONDITION_INDICATOR_C_PARAM);

                Float good_c_upper_limit = Float.valueOf((String) conditionIndicatorCParam.getJSONObject(Constants.DATA_GOOD_PARAM).getJSONArray(Constants.DATA_TRI_C_MEAS_PARAM).get(0));
                Float fair_c_lower_limit = Float.valueOf((String) conditionIndicatorCParam.getJSONObject(Constants.DATA_FAIR_PARAM).getJSONArray(Constants.DATA_TRI_C_MEAS_PARAM).get(0));
                Float fair_c_upper_limit = Float.valueOf((String) conditionIndicatorCParam.getJSONObject(Constants.DATA_FAIR_PARAM).getJSONArray(Constants.DATA_TRI_C_MEAS_PARAM).get(1));
                Float poor_c_lower_limit = Float.valueOf((String) conditionIndicatorCParam.getJSONObject(Constants.DATA_POOR_PARAM).getJSONArray(Constants.DATA_TRI_C_MEAS_PARAM).get(0));
                Float poor_c_upper_limit = Float.valueOf((String) conditionIndicatorCParam.getJSONObject(Constants.DATA_POOR_PARAM).getJSONArray(Constants.DATA_TRI_C_MEAS_PARAM).get(1));
                Float bad_c_lower_limit = Float.valueOf((String) conditionIndicatorCParam.getJSONObject(Constants.DATA_BAD_PARAM).getJSONArray(Constants.DATA_TRI_C_MEAS_PARAM).get(1));

                JSONArray tableJsonArray = jsonObject.getJSONArray(Constants.DATA_TABLE_PARAM);

                updateTableIndicatorByLowerAndUpperLimit(tableJsonArray, good_df_upper_limit, fair_df_lower_limit,
                        fair_df_upper_limit, poor_df_lower_limit, poor_df_upper_limit, bad_df_lower_limit,
                        Constants.TABLE_CONDITION_INDICATOR_DF_PARAM, Constants.TABLE_DF_MEAS_PARAM);

                updateTableIndicatorByLowerAndUpperLimit(tableJsonArray, good_c_upper_limit, fair_c_lower_limit,
                        fair_c_upper_limit, poor_c_lower_limit, poor_c_upper_limit, bad_c_lower_limit,
                        Constants.TABLE_CONDITION_INDICATOR_C_PARAM, Constants.TABLE_C_MEAS_BUSHING_PARAM);

            } else if (TestTypeEnum.BUSHINGPRIMC1.name().equalsIgnoreCase(jsonObject.getString(Constants.DATA_CODE_PARAM))) {
                JSONArray tableJsonArray = jsonObject.getJSONArray(Constants.DATA_TABLE_PARAM);

                //condition_indicator_c
                JSONObject conditionIndicatorCParam = jsonObject.getJSONObject(Constants.DATA_CONDITION_INDICATOR_C_PARAM);
                Float good_c_upper_limit = Float.valueOf((String) conditionIndicatorCParam.getJSONObject(Constants.DATA_GOOD_PARAM).getJSONArray(Constants.DATA_TRI_C_MEAS_PARAM).get(0));
                Float fair_c_lower_limit = Float.valueOf((String) conditionIndicatorCParam.getJSONObject(Constants.DATA_FAIR_PARAM).getJSONArray(Constants.DATA_TRI_C_MEAS_PARAM).get(0));
                Float fair_c_upper_limit = Float.valueOf((String) conditionIndicatorCParam.getJSONObject(Constants.DATA_FAIR_PARAM).getJSONArray(Constants.DATA_TRI_C_MEAS_PARAM).get(1));
                Float poor_c_lower_limit = Float.valueOf((String) conditionIndicatorCParam.getJSONObject(Constants.DATA_POOR_PARAM).getJSONArray(Constants.DATA_TRI_C_MEAS_PARAM).get(0));
                Float poor_c_upper_limit = Float.valueOf((String) conditionIndicatorCParam.getJSONObject(Constants.DATA_POOR_PARAM).getJSONArray(Constants.DATA_TRI_C_MEAS_PARAM).get(1));
                Float bad_c_lower_limit = Float.valueOf((String) conditionIndicatorCParam.getJSONObject(Constants.DATA_BAD_PARAM).getJSONArray(Constants.DATA_TRI_C_MEAS_PARAM).get(1));

                updateTableIndicatorByLowerAndUpperLimit(tableJsonArray, good_c_upper_limit, fair_c_lower_limit,
                        fair_c_upper_limit, poor_c_lower_limit, poor_c_upper_limit, bad_c_lower_limit,
                        Constants.TABLE_CONDITION_INDICATOR_C_PARAM, Constants.TABLE_C_MEAS_BUSHING_PARAM);

                //condition_indicator_df
                updateTableIndicatorBushingPrimDF(jsonObject, tableJsonArray);
            } else if (TestTypeEnum.BUSHINGPRIMC2.name().equalsIgnoreCase(jsonObject.getString(Constants.DATA_CODE_PARAM))) {
                JSONArray tableJsonArray = jsonObject.getJSONArray(Constants.DATA_TABLE_PARAM);

                //condition_indicator_c
                JSONObject conditionIndicatorCParam = jsonObject.getJSONObject(Constants.DATA_CONDITION_INDICATOR_C_PARAM);
                Float good_c_upper_limit = Float.valueOf((String) conditionIndicatorCParam.getJSONObject(Constants.DATA_GOOD_PARAM).getJSONArray(Constants.DATA_TRI_C_MEAS_PARAM).get(0));
                Float fair_c_lower_limit = Float.valueOf((String) conditionIndicatorCParam.getJSONObject(Constants.DATA_FAIR_PARAM).getJSONArray(Constants.DATA_TRI_C_MEAS_PARAM).get(0));
                Float fair_c_upper_limit = Float.valueOf((String) conditionIndicatorCParam.getJSONObject(Constants.DATA_FAIR_PARAM).getJSONArray(Constants.DATA_TRI_C_MEAS_PARAM).get(1));
                Float poor_c_lower_limit = Float.valueOf((String) conditionIndicatorCParam.getJSONObject(Constants.DATA_POOR_PARAM).getJSONArray(Constants.DATA_TRI_C_MEAS_PARAM).get(0));
                Float poor_c_upper_limit = Float.valueOf((String) conditionIndicatorCParam.getJSONObject(Constants.DATA_POOR_PARAM).getJSONArray(Constants.DATA_TRI_C_MEAS_PARAM).get(1));
                Float bad_c_lower_limit = Float.valueOf((String) conditionIndicatorCParam.getJSONObject(Constants.DATA_BAD_PARAM).getJSONArray(Constants.DATA_TRI_C_MEAS_PARAM).get(1));

                updateTableIndicatorByLowerAndUpperLimit(tableJsonArray, good_c_upper_limit, fair_c_lower_limit,
                        fair_c_upper_limit, poor_c_lower_limit, poor_c_upper_limit, bad_c_lower_limit,
                        Constants.TABLE_CONDITION_INDICATOR_C_PARAM, Constants.TABLE_C_MEAS_BUSHING_PARAM);

                //condition_indicator_df
                updateTableIndicatorBushingPrimDF(jsonObject, tableJsonArray);

            } else if (TestTypeEnum.DCWINDINGPRIM.name().equalsIgnoreCase(jsonObject.getString(Constants.DATA_CODE_PARAM))) {
                JSONArray tableJsonArray = jsonObject.getJSONArray(Constants.DATA_TABLE_PARAM);
                updateTableIndicatorDCWindingResistance(jsonObject, tableJsonArray);
            } else if (TestTypeEnum.DCWINDINGSEC.name().equalsIgnoreCase(jsonObject.getString(Constants.DATA_CODE_PARAM))) {
                JSONArray tableJsonArray = jsonObject.getJSONArray(Constants.DATA_TABLE_PARAM);
                updateTableIndicatorDCWindingResistance(jsonObject, tableJsonArray);
            } else if (TestTypeEnum.EXCITINGCURRENT.name().equalsIgnoreCase(jsonObject.getString(Constants.DATA_CODE_PARAM))) {
                JSONObject conditionIndicatorSettingParam = jsonObject.getJSONObject(Constants.DATA_CONDITION_INDICATOR_SETTING_PARAM);

                Float good_upper_limit = Float.valueOf((String) conditionIndicatorSettingParam.getJSONObject(Constants.DATA_GOOD_PARAM).getJSONArray(Constants.DATA_DEV_PER_PARAM).get(0));
                Float fair_lower_limit = Float.valueOf((String) conditionIndicatorSettingParam.getJSONObject(Constants.DATA_FAIR_PARAM).getJSONArray(Constants.DATA_DEV_PER_PARAM).get(0));
                Float fair_upper_limit = Float.valueOf((String) conditionIndicatorSettingParam.getJSONObject(Constants.DATA_FAIR_PARAM).getJSONArray(Constants.DATA_DEV_PER_PARAM).get(1));
                Float poor_lower_limit = Float.valueOf((String) conditionIndicatorSettingParam.getJSONObject(Constants.DATA_POOR_PARAM).getJSONArray(Constants.DATA_DEV_PER_PARAM).get(0));
                Float poor_upper_limit = Float.valueOf((String) conditionIndicatorSettingParam.getJSONObject(Constants.DATA_POOR_PARAM).getJSONArray(Constants.DATA_DEV_PER_PARAM).get(1));
                Float bad_lower_limit = Float.valueOf((String) conditionIndicatorSettingParam.getJSONObject(Constants.DATA_BAD_PARAM).getJSONArray(Constants.DATA_DEV_PER_PARAM).get(1));

                JSONArray tableJsonArray = jsonObject.getJSONArray(Constants.DATA_TABLE_PARAM);
                updateTableIndicatorByLowerAndUpperLimit(tableJsonArray, good_upper_limit, fair_lower_limit,
                        fair_upper_limit, poor_lower_limit, poor_upper_limit, bad_lower_limit,
                        Constants.TABLE_CONDITION_INDICATOR_PARAM, Constants.TABLE_DEV_PER_PARAM);

            } else if (TestTypeEnum.RATIOPRIMSEC.name().equalsIgnoreCase(jsonObject.getString(Constants.DATA_CODE_PARAM))) {
                JSONObject conditionIndicatorSettingParam = jsonObject.getJSONObject(Constants.DATA_CONDITION_INDICATOR_SETTING_PARAM);

                Float good_upper_limit = Float.valueOf((String) conditionIndicatorSettingParam.getJSONObject(Constants.DATA_GOOD_PARAM).getJSONArray(Constants.DATA_RATIO_DEV_PARAM).get(0));
                Float fair_lower_limit = Float.valueOf((String) conditionIndicatorSettingParam.getJSONObject(Constants.DATA_FAIR_PARAM).getJSONArray(Constants.DATA_RATIO_DEV_PARAM).get(0));
                Float fair_upper_limit = Float.valueOf((String) conditionIndicatorSettingParam.getJSONObject(Constants.DATA_FAIR_PARAM).getJSONArray(Constants.DATA_RATIO_DEV_PARAM).get(1));
                Float poor_lower_limit = Float.valueOf((String) conditionIndicatorSettingParam.getJSONObject(Constants.DATA_POOR_PARAM).getJSONArray(Constants.DATA_RATIO_DEV_PARAM).get(0));
                Float poor_upper_limit = Float.valueOf((String) conditionIndicatorSettingParam.getJSONObject(Constants.DATA_POOR_PARAM).getJSONArray(Constants.DATA_RATIO_DEV_PARAM).get(1));
                Float bad_lower_limit = Float.valueOf((String) conditionIndicatorSettingParam.getJSONObject(Constants.DATA_BAD_PARAM).getJSONArray(Constants.DATA_RATIO_DEV_PARAM).get(1));

                JSONArray tableJsonArray = jsonObject.getJSONArray(Constants.DATA_TABLE_PARAM);
                updateTableIndicatorByLowerAndUpperLimit(tableJsonArray, good_upper_limit, fair_lower_limit,
                        fair_upper_limit, poor_lower_limit, poor_upper_limit, bad_lower_limit,
                        Constants.TABLE_CONDITION_INDICATOR_PARAM, Constants.TABLE_RATIO_DEV_PARAM);
            } else if (TestTypeEnum.DGA.name().equalsIgnoreCase(jsonObject.getString(Constants.DATA_CODE_PARAM))) {
                Float h2Value = handleNullValue(jsonObject.getString(Constants.DATA_H2_PARAM));
                Float ch4Value = handleNullValue(jsonObject.getString(Constants.DATA_CH4_PARAM));
                Float c2h2Value = handleNullValue(jsonObject.getString(Constants.DATA_C2H2_PARAM));
                Float c2h4Value = handleNullValue(jsonObject.getString(Constants.DATA_C2H4_PARAM));
                Float c2h6Value = handleNullValue(jsonObject.getString(Constants.DATA_C2H6_PARAM));
                Float coValue = handleNullValue(jsonObject.getString(Constants.DATA_CO_PARAM));
                Float tdcgValue = handleNullValue(jsonObject.getString(Constants.DATA_CO2_PARAM));
                if (checkBadIndicator(jsonObject, h2Value, ch4Value, c2h2Value, c2h4Value, c2h6Value, coValue, tdcgValue)
                        || checkPoorIndicator(jsonObject, h2Value, ch4Value, c2h2Value, c2h4Value, c2h6Value, coValue, tdcgValue)
                        || checkFairIndicator(jsonObject, h2Value, ch4Value, c2h2Value, c2h4Value, c2h6Value, coValue, tdcgValue)
                        || checkGoodIndicator(jsonObject, h2Value, ch4Value, c2h2Value, c2h4Value, c2h6Value, coValue, tdcgValue))
                    ;

            } else if (TestTypeEnum.MEASUREMENTOFOIL.name().equalsIgnoreCase(jsonObject.getString(Constants.DATA_CODE_PARAM))) {
                JSONObject conditionIndicatorSettingParam = jsonObject.getJSONObject(Constants.DATA_CONDITION_INDICATOR_SETTING_PARAM);

                Float good_upper_limit = Float.valueOf((String) conditionIndicatorSettingParam.getJSONObject(Constants.DATA_GOOD_PARAM).getJSONArray(Constants.DATA_BREAKDOWN_VOLTAGE_PARAM).get(1));
                Float fair_lower_limit = Float.valueOf((String) conditionIndicatorSettingParam.getJSONObject(Constants.DATA_FAIR_PARAM).getJSONArray(Constants.DATA_BREAKDOWN_VOLTAGE_PARAM).get(0));
                Float fair_upper_limit = Float.valueOf((String) conditionIndicatorSettingParam.getJSONObject(Constants.DATA_FAIR_PARAM).getJSONArray(Constants.DATA_BREAKDOWN_VOLTAGE_PARAM).get(1));
                Float poor_lower_limit = Float.valueOf((String) conditionIndicatorSettingParam.getJSONObject(Constants.DATA_POOR_PARAM).getJSONArray(Constants.DATA_BREAKDOWN_VOLTAGE_PARAM).get(0));
                Float poor_upper_limit = Float.valueOf((String) conditionIndicatorSettingParam.getJSONObject(Constants.DATA_POOR_PARAM).getJSONArray(Constants.DATA_BREAKDOWN_VOLTAGE_PARAM).get(1));
                Float bad_lower_limit = Float.valueOf((String) conditionIndicatorSettingParam.getJSONObject(Constants.DATA_BAD_PARAM).getJSONArray(Constants.DATA_BREAKDOWN_VOLTAGE_PARAM).get(0));

                Float result = handleNullValue(jsonObject.getString(Constants.DATA_RESULT_PARAM));
                if (result < bad_lower_limit) {
                    jsonObject.put(Constants.DATA_CONDITION_INDICATOR_PARAM, IndicatorEnum.Bad.name());
                } else if (result > poor_lower_limit && result < poor_upper_limit) {
                    jsonObject.put(Constants.DATA_CONDITION_INDICATOR_PARAM, IndicatorEnum.Poor.name());
                } else if (result > fair_lower_limit && result < fair_upper_limit) {
                    jsonObject.put(Constants.DATA_CONDITION_INDICATOR_PARAM, IndicatorEnum.Fair.name());
                }
                if (result >= good_upper_limit) {
                    jsonObject.put(Constants.DATA_CONDITION_INDICATOR_PARAM, IndicatorEnum.Good.name());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error(e.toString());
            throw new InvalidTestException("Invalid test data");
        }
        return jsonObject == null ? null : jsonObject.toString();
    }

    private boolean checkGoodIndicator(JSONObject jsonObject, Float h2Value, Float ch4Value, Float c2h2Value, Float c2h4Value, Float c2h6Value, Float coValue, Float tdcgValue) throws JSONException {
        JSONObject conditionIndicatorSettingParam = jsonObject.getJSONObject(Constants.DATA_CONDITION_INDICATOR_SETTING_PARAM);

        Float h2_good_limit = Float.valueOf((Integer) conditionIndicatorSettingParam.getJSONObject(Constants.DATA_GOOD_PARAM).getJSONArray(Constants.DATA_H2_PARAM).get(0));
        Float c2h2_good_limit = Float.valueOf((Integer) conditionIndicatorSettingParam.getJSONObject(Constants.DATA_GOOD_PARAM).getJSONArray(Constants.DATA_C2H2_PARAM).get(0));
        Float c2h4_good_limit = Float.valueOf((Integer) conditionIndicatorSettingParam.getJSONObject(Constants.DATA_GOOD_PARAM).getJSONArray(Constants.DATA_C2H4_PARAM).get(0));
        Float c2h6_good_limit = Float.valueOf((Integer) conditionIndicatorSettingParam.getJSONObject(Constants.DATA_GOOD_PARAM).getJSONArray(Constants.DATA_C2H6_PARAM).get(0));
        Float ch4_good_limit = Float.valueOf((Integer) conditionIndicatorSettingParam.getJSONObject(Constants.DATA_GOOD_PARAM).getJSONArray(Constants.DATA_CH4_PARAM).get(0));
        Float co_good_limit = Float.valueOf((Integer) conditionIndicatorSettingParam.getJSONObject(Constants.DATA_GOOD_PARAM).getJSONArray(Constants.DATA_CO_PARAM).get(0));
        Float tdcg_good_limit = Float.valueOf((Integer) conditionIndicatorSettingParam.getJSONObject(Constants.DATA_GOOD_PARAM).getJSONArray(Constants.DATA_TDCG_PARAM).get(0));
        if (h2Value <= h2_good_limit
                || ch4Value <= ch4_good_limit
                || c2h2Value <= c2h2_good_limit
                || c2h4Value <= c2h4_good_limit
                || c2h6Value <= c2h6_good_limit
                || coValue <= co_good_limit
                || tdcgValue <= tdcg_good_limit
        ) {
            jsonObject.put(Constants.DATA_CONDITION_INDICATOR_PARAM, IndicatorEnum.Good.name());
            jsonObject.put(Constants.DATA_STATUS_PARAM, "Condition 1");
            return true;
        }
        return false;
    }

    private boolean checkFairIndicator(JSONObject jsonObject, Float h2Value, Float ch4Value, Float c2h2Value, Float c2h4Value, Float c2h6Value, Float coValue, Float tdcgValue) throws JSONException {
        JSONObject conditionIndicatorSettingParam = jsonObject.getJSONObject(Constants.DATA_CONDITION_INDICATOR_SETTING_PARAM);

        Float h2_fair_lower_limit = Float.valueOf((Integer) conditionIndicatorSettingParam.getJSONObject(Constants.DATA_FAIR_PARAM).getJSONArray(Constants.DATA_H2_PARAM).get(0));
        Float c2h2_fair_lower_limit = Float.valueOf((Integer) conditionIndicatorSettingParam.getJSONObject(Constants.DATA_FAIR_PARAM).getJSONArray(Constants.DATA_C2H2_PARAM).get(0));
        Float c2h4_fair_lower_limit = Float.valueOf((Integer) conditionIndicatorSettingParam.getJSONObject(Constants.DATA_FAIR_PARAM).getJSONArray(Constants.DATA_C2H4_PARAM).get(0));
        Float c2h6_fair_lower_limit = Float.valueOf((Integer) conditionIndicatorSettingParam.getJSONObject(Constants.DATA_FAIR_PARAM).getJSONArray(Constants.DATA_C2H6_PARAM).get(0));
        Float ch4_fair_lower_limit = Float.valueOf((Integer) conditionIndicatorSettingParam.getJSONObject(Constants.DATA_FAIR_PARAM).getJSONArray(Constants.DATA_CH4_PARAM).get(0));
        Float co_fair_lower_limit = Float.valueOf((Integer) conditionIndicatorSettingParam.getJSONObject(Constants.DATA_FAIR_PARAM).getJSONArray(Constants.DATA_CO_PARAM).get(0));
        Float tdcg_fair_lower_limit = Float.valueOf((Integer) conditionIndicatorSettingParam.getJSONObject(Constants.DATA_FAIR_PARAM).getJSONArray(Constants.DATA_TDCG_PARAM).get(0));

        Float h2_fair_upper_limit = Float.valueOf((Integer) conditionIndicatorSettingParam.getJSONObject(Constants.DATA_FAIR_PARAM).getJSONArray(Constants.DATA_H2_PARAM).get(1));
        Float c2h2_fair_upper_limit = Float.valueOf((Integer) conditionIndicatorSettingParam.getJSONObject(Constants.DATA_FAIR_PARAM).getJSONArray(Constants.DATA_C2H2_PARAM).get(1));
        Float c2h4_fair_upper_limit = Float.valueOf((Integer) conditionIndicatorSettingParam.getJSONObject(Constants.DATA_FAIR_PARAM).getJSONArray(Constants.DATA_C2H4_PARAM).get(1));
        Float c2h6_fair_upper_limit = Float.valueOf((Integer) conditionIndicatorSettingParam.getJSONObject(Constants.DATA_FAIR_PARAM).getJSONArray(Constants.DATA_C2H6_PARAM).get(1));
        Float ch4_fair_upper_limit = Float.valueOf((Integer) conditionIndicatorSettingParam.getJSONObject(Constants.DATA_FAIR_PARAM).getJSONArray(Constants.DATA_CH4_PARAM).get(1));
        Float co_fair_upper_limit = Float.valueOf((Integer) conditionIndicatorSettingParam.getJSONObject(Constants.DATA_FAIR_PARAM).getJSONArray(Constants.DATA_CO_PARAM).get(1));
        Float tdcg_fair_upper_limit = Float.valueOf((Integer) conditionIndicatorSettingParam.getJSONObject(Constants.DATA_FAIR_PARAM).getJSONArray(Constants.DATA_TDCG_PARAM).get(1));

        if (h2Value >= h2_fair_lower_limit && h2Value <= h2_fair_upper_limit
                || ch4Value >= ch4_fair_lower_limit && ch4Value <= ch4_fair_upper_limit
                || c2h2Value >= c2h2_fair_lower_limit && c2h2Value <= c2h2_fair_upper_limit
                || c2h4Value >= c2h4_fair_lower_limit && c2h4Value <= c2h4_fair_upper_limit
                || c2h6Value >= c2h6_fair_lower_limit && c2h6Value <= c2h6_fair_upper_limit
                || coValue >= co_fair_lower_limit && coValue <= co_fair_upper_limit
                || tdcgValue >= tdcg_fair_lower_limit && tdcgValue <= tdcg_fair_upper_limit
        ) {
            jsonObject.put(Constants.DATA_CONDITION_INDICATOR_PARAM, IndicatorEnum.Fair.name());
            jsonObject.put(Constants.DATA_STATUS_PARAM, "Condition 2");
            return true;
        }
        return false;
    }

    private boolean checkPoorIndicator(JSONObject jsonObject, Float h2Value, Float ch4Value, Float c2h2Value, Float c2h4Value, Float c2h6Value, Float coValue, Float tdcgValue) throws JSONException {
        JSONObject conditionIndicatorSettingParam = jsonObject.getJSONObject(Constants.DATA_CONDITION_INDICATOR_SETTING_PARAM);

        Float h2_poor_lower_limit = Float.valueOf((Integer) conditionIndicatorSettingParam.getJSONObject(Constants.DATA_POOR_PARAM).getJSONArray(Constants.DATA_H2_PARAM).get(0));
        Float c2h2_poor_lower_limit = Float.valueOf((Integer) conditionIndicatorSettingParam.getJSONObject(Constants.DATA_POOR_PARAM).getJSONArray(Constants.DATA_C2H2_PARAM).get(0));
        Float c2h4_poor_lower_limit = Float.valueOf((Integer) conditionIndicatorSettingParam.getJSONObject(Constants.DATA_POOR_PARAM).getJSONArray(Constants.DATA_C2H4_PARAM).get(0));
        Float c2h6_poor_lower_limit = Float.valueOf((Integer) conditionIndicatorSettingParam.getJSONObject(Constants.DATA_POOR_PARAM).getJSONArray(Constants.DATA_C2H6_PARAM).get(0));
        Float ch4_poor_lower_limit = Float.valueOf((Integer) conditionIndicatorSettingParam.getJSONObject(Constants.DATA_POOR_PARAM).getJSONArray(Constants.DATA_CH4_PARAM).get(0));
        Float co_poor_lower_limit = Float.valueOf((Integer) conditionIndicatorSettingParam.getJSONObject(Constants.DATA_POOR_PARAM).getJSONArray(Constants.DATA_CO_PARAM).get(0));
        Float tdcg_poor_lower_limit = Float.valueOf((Integer) conditionIndicatorSettingParam.getJSONObject(Constants.DATA_POOR_PARAM).getJSONArray(Constants.DATA_TDCG_PARAM).get(0));

        Float h2_poor_upper_limit = Float.valueOf((Integer) conditionIndicatorSettingParam.getJSONObject(Constants.DATA_POOR_PARAM).getJSONArray(Constants.DATA_H2_PARAM).get(1));
        Float c2h2_poor_upper_limit = Float.valueOf((Integer) conditionIndicatorSettingParam.getJSONObject(Constants.DATA_POOR_PARAM).getJSONArray(Constants.DATA_C2H2_PARAM).get(1));
        Float c2h4_poor_upper_limit = Float.valueOf((Integer) conditionIndicatorSettingParam.getJSONObject(Constants.DATA_POOR_PARAM).getJSONArray(Constants.DATA_C2H4_PARAM).get(1));
        Float c2h6_poor_upper_limit = Float.valueOf((Integer) conditionIndicatorSettingParam.getJSONObject(Constants.DATA_POOR_PARAM).getJSONArray(Constants.DATA_C2H6_PARAM).get(1));
        Float ch4_poor_upper_limit = Float.valueOf((Integer) conditionIndicatorSettingParam.getJSONObject(Constants.DATA_POOR_PARAM).getJSONArray(Constants.DATA_CH4_PARAM).get(1));
        Float co_poor_upper_limit = Float.valueOf((Integer) conditionIndicatorSettingParam.getJSONObject(Constants.DATA_POOR_PARAM).getJSONArray(Constants.DATA_CO_PARAM).get(1));
        Float tdcg_poor_upper_limit = Float.valueOf((Integer) conditionIndicatorSettingParam.getJSONObject(Constants.DATA_POOR_PARAM).getJSONArray(Constants.DATA_TDCG_PARAM).get(1));

        if (h2Value >= h2_poor_lower_limit && h2Value <= h2_poor_upper_limit
                || ch4Value >= ch4_poor_lower_limit && ch4Value <= ch4_poor_upper_limit
                || c2h2Value >= c2h2_poor_lower_limit && c2h2Value <= c2h2_poor_upper_limit
                || c2h4Value >= c2h4_poor_lower_limit && c2h4Value <= c2h4_poor_upper_limit
                || c2h6Value >= c2h6_poor_lower_limit && c2h6Value <= c2h6_poor_upper_limit
                || coValue >= co_poor_lower_limit && coValue <= co_poor_upper_limit
                || tdcgValue >= tdcg_poor_lower_limit && tdcgValue <= tdcg_poor_upper_limit
        ) {
            jsonObject.put(Constants.DATA_CONDITION_INDICATOR_PARAM, IndicatorEnum.Poor.name());
            jsonObject.put(Constants.DATA_STATUS_PARAM, "Condition 3");
            return true;
        }
        return false;
    }

    private boolean checkBadIndicator(JSONObject jsonObject, Float h2Value, Float ch4Value, Float c2h2Value, Float c2h4Value, Float c2h6Value, Float coValue, Float tdcgValue) throws JSONException {
        JSONObject conditionIndicatorSettingParam = jsonObject.getJSONObject(Constants.DATA_CONDITION_INDICATOR_SETTING_PARAM);

        Float h2_bad_limit = Float.valueOf((Integer) conditionIndicatorSettingParam.getJSONObject(Constants.DATA_BAD_PARAM).getJSONArray(Constants.DATA_H2_PARAM).get(1));
        Float c2h2_bad_limit = Float.valueOf((Integer) conditionIndicatorSettingParam.getJSONObject(Constants.DATA_BAD_PARAM).getJSONArray(Constants.DATA_C2H2_PARAM).get(1));
        Float c2h4_bad_limit = Float.valueOf((Integer) conditionIndicatorSettingParam.getJSONObject(Constants.DATA_BAD_PARAM).getJSONArray(Constants.DATA_C2H4_PARAM).get(1));
        Float c2h6_bad_limit = Float.valueOf((Integer) conditionIndicatorSettingParam.getJSONObject(Constants.DATA_BAD_PARAM).getJSONArray(Constants.DATA_C2H6_PARAM).get(1));
        Float ch4_bad_limit = Float.valueOf((Integer) conditionIndicatorSettingParam.getJSONObject(Constants.DATA_BAD_PARAM).getJSONArray(Constants.DATA_CH4_PARAM).get(1));
        Float co_bad_limit = Float.valueOf((Integer) conditionIndicatorSettingParam.getJSONObject(Constants.DATA_BAD_PARAM).getJSONArray(Constants.DATA_CO_PARAM).get(1));
        Float tdcg_bad_limit = Float.valueOf((Integer) conditionIndicatorSettingParam.getJSONObject(Constants.DATA_BAD_PARAM).getJSONArray(Constants.DATA_TDCG_PARAM).get(1));

        if (h2Value > h2_bad_limit
                || ch4Value > ch4_bad_limit
                || c2h2Value > c2h2_bad_limit
                || c2h4Value > c2h4_bad_limit
                || c2h6Value > c2h6_bad_limit
                || coValue > co_bad_limit
                || tdcgValue > tdcg_bad_limit
        ) {
            jsonObject.put(Constants.DATA_CONDITION_INDICATOR_PARAM, IndicatorEnum.Bad.name());
            jsonObject.put(Constants.DATA_STATUS_PARAM, "Condition 4");
            return true;
        }
        return false;
    }

    private void updateTableIndicatorByLowerAndUpperLimit(JSONArray tableJsonArray,
                                                          Float good_upper_limit,
                                                          Float fair_lower_limit,
                                                          Float fair_upper_limit,
                                                          Float poor_lower_limit,
                                                          Float poor_upper_limit,
                                                          Float bad_lower_limit,
                                                          String updatedTableParam,
                                                          String comparedTableParam

    ) throws JSONException {
        for (int i = 0; i < tableJsonArray.length(); i++) {
            JSONObject tableElementJsonObject = tableJsonArray.getJSONObject(i);
            Float df_meas = Math.abs(handleNullValue(tableElementJsonObject.getString(comparedTableParam)));

            if (df_meas > bad_lower_limit) {
                tableElementJsonObject.put(updatedTableParam, IndicatorEnum.Bad.name());
            } else if (df_meas > poor_lower_limit && df_meas <= poor_upper_limit) {
                tableElementJsonObject.put(updatedTableParam, IndicatorEnum.Poor.name());
            } else if (df_meas > fair_lower_limit && df_meas <= fair_upper_limit) {
                tableElementJsonObject.put(updatedTableParam, IndicatorEnum.Fair.name());
            } else if (df_meas <= good_upper_limit) {
                tableElementJsonObject.put(updatedTableParam, IndicatorEnum.Good.name());
            } else {
                char[] cArr = {};
                tableElementJsonObject.put(updatedTableParam, String.valueOf(cArr));
            }
        }
    }

    private void updateTableIndicatorDCWindingResistance(JSONObject jsonObject, JSONArray tableJsonArray) throws JSONException {
        JSONObject conditionIndicatorSettingParam = jsonObject.getJSONObject(Constants.DATA_CONDITION_INDICATOR_SETTING_PARAM);

        Float good_upper_limit = Float.valueOf((String) conditionIndicatorSettingParam.getJSONObject(Constants.DATA_GOOD_PARAM).getJSONArray(Constants.DATA_ERROR_BETWEEN_PHASE_PARAM).get(0));
        Float fair_lower_limit = Float.valueOf((String) conditionIndicatorSettingParam.getJSONObject(Constants.DATA_FAIR_PARAM).getJSONArray(Constants.DATA_ERROR_BETWEEN_PHASE_PARAM).get(0));
        Float fair_upper_limit = Float.valueOf((String) conditionIndicatorSettingParam.getJSONObject(Constants.DATA_FAIR_PARAM).getJSONArray(Constants.DATA_ERROR_BETWEEN_PHASE_PARAM).get(1));
        Float poor_lower_limit = Float.valueOf((String) conditionIndicatorSettingParam.getJSONObject(Constants.DATA_POOR_PARAM).getJSONArray(Constants.DATA_ERROR_BETWEEN_PHASE_PARAM).get(0));
        Float poor_upper_limit = Float.valueOf((String) conditionIndicatorSettingParam.getJSONObject(Constants.DATA_POOR_PARAM).getJSONArray(Constants.DATA_ERROR_BETWEEN_PHASE_PARAM).get(1));
        Float bad_lower_limit = Float.valueOf((String) conditionIndicatorSettingParam.getJSONObject(Constants.DATA_BAD_PARAM).getJSONArray(Constants.DATA_ERROR_BETWEEN_PHASE_PARAM).get(1));

        for (int i = 0; i < tableJsonArray.length(); i++) {
            JSONObject tableElementJsonObject = tableJsonArray.getJSONObject(i);
            Float error_between_phase = Math.abs(handleNullValue(tableElementJsonObject.getString(Constants.TABLE_ERROR_BETWEEN_PHASE_PARAM)));
            Float error_r_ref = Math.abs(handleNullValue(tableElementJsonObject.getString(Constants.TABLE_ERROR_R_REF_PARAM)));

            if (!Float.isNaN(error_r_ref)) {
                if (error_r_ref > bad_lower_limit) {
                    tableElementJsonObject.put(Constants.TABLE_CONDITION_INDICATOR_PARAM, IndicatorEnum.Bad.name());
                } else if (error_r_ref > poor_lower_limit && error_r_ref <= poor_upper_limit) {
                    tableElementJsonObject.put(Constants.TABLE_CONDITION_INDICATOR_PARAM, IndicatorEnum.Poor.name());
                } else if (error_r_ref > fair_lower_limit && error_r_ref <= fair_upper_limit) {
                    tableElementJsonObject.put(Constants.TABLE_CONDITION_INDICATOR_PARAM, IndicatorEnum.Fair.name());
                } else if (error_r_ref <= good_upper_limit) {
                    tableElementJsonObject.put(Constants.TABLE_CONDITION_INDICATOR_PARAM, IndicatorEnum.Good.name());
                } else {
                    char[] cArr = {};
                    tableElementJsonObject.put(Constants.TABLE_CONDITION_INDICATOR_PARAM, String.valueOf(cArr));
                }
            } else {

                if ((error_between_phase > bad_lower_limit)
                        || (error_r_ref > bad_lower_limit)
                ) {
                    tableElementJsonObject.put(Constants.TABLE_CONDITION_INDICATOR_PARAM, IndicatorEnum.Bad.name());
                } else if ((error_between_phase > poor_lower_limit && error_between_phase <= poor_upper_limit)
                        || (error_r_ref > poor_lower_limit && error_r_ref <= poor_upper_limit)
                ) {
                    tableElementJsonObject.put(Constants.TABLE_CONDITION_INDICATOR_PARAM, IndicatorEnum.Poor.name());
                } else if ((error_between_phase > fair_lower_limit && error_between_phase <= fair_upper_limit)
                        || (error_r_ref > fair_lower_limit && error_r_ref <= fair_upper_limit)
                ) {
                    tableElementJsonObject.put(Constants.TABLE_CONDITION_INDICATOR_PARAM, IndicatorEnum.Fair.name());
                } else if (error_between_phase <= good_upper_limit || error_r_ref <= good_upper_limit) {
                    tableElementJsonObject.put(Constants.TABLE_CONDITION_INDICATOR_PARAM, IndicatorEnum.Good.name());
                } else {
                    char[] cArr = {};
                    tableElementJsonObject.put(Constants.TABLE_CONDITION_INDICATOR_PARAM, String.valueOf(cArr));
                }
            }
        }
    }

    private void updateTableIndicatorBushingPrimDF(JSONObject jsonObject, JSONArray tableJsonArray) throws JSONException {
        JSONObject conditionIndicatorSettingParam = jsonObject.getJSONObject(Constants.DATA_CONDITION_INDICATOR_DF_PARAM);

        Float good_upper_limit_df_meas = Float.valueOf((String) conditionIndicatorSettingParam.getJSONObject(Constants.DATA_GOOD_PARAM).getJSONArray(Constants.DATA_DF_MEAS_PARAM).get(0));
        Float fair_lower_limit_df_meas = Float.valueOf((String) conditionIndicatorSettingParam.getJSONObject(Constants.DATA_FAIR_PARAM).getJSONArray(Constants.DATA_DF_MEAS_PARAM).get(0));
        Float fair_upper_limit_df_meas = Float.valueOf((String) conditionIndicatorSettingParam.getJSONObject(Constants.DATA_FAIR_PARAM).getJSONArray(Constants.DATA_DF_MEAS_PARAM).get(1));
        Float poor_lower_limit_df_meas = Float.valueOf((String) conditionIndicatorSettingParam.getJSONObject(Constants.DATA_POOR_PARAM).getJSONArray(Constants.DATA_DF_MEAS_PARAM).get(0));
        Float poor_upper_limit_df_meas = Float.valueOf((String) conditionIndicatorSettingParam.getJSONObject(Constants.DATA_POOR_PARAM).getJSONArray(Constants.DATA_DF_MEAS_PARAM).get(1));
        Float bad_lower_limit_df_meas = Float.valueOf((String) conditionIndicatorSettingParam.getJSONObject(Constants.DATA_BAD_PARAM).getJSONArray(Constants.DATA_DF_MEAS_PARAM).get(1));

        Float good_upper_limit_df_change = Float.valueOf((String) conditionIndicatorSettingParam.getJSONObject(Constants.DATA_GOOD_PARAM).getJSONArray(Constants.DATA_DF_CHANGE_PARAM).get(0));
        Float fair_lower_limit_df_change = Float.valueOf((String) conditionIndicatorSettingParam.getJSONObject(Constants.DATA_FAIR_PARAM).getJSONArray(Constants.DATA_DF_CHANGE_PARAM).get(0));
        Float fair_upper_limit_df_change = Float.valueOf((String) conditionIndicatorSettingParam.getJSONObject(Constants.DATA_FAIR_PARAM).getJSONArray(Constants.DATA_DF_CHANGE_PARAM).get(1));
        Float poor_lower_limit_df_change = Float.valueOf((String) conditionIndicatorSettingParam.getJSONObject(Constants.DATA_POOR_PARAM).getJSONArray(Constants.DATA_DF_CHANGE_PARAM).get(0));
        Float poor_upper_limit_df_change = Float.valueOf((String) conditionIndicatorSettingParam.getJSONObject(Constants.DATA_POOR_PARAM).getJSONArray(Constants.DATA_DF_CHANGE_PARAM).get(1));
        Float bad_lower_limit_df_change = Float.valueOf((String) conditionIndicatorSettingParam.getJSONObject(Constants.DATA_BAD_PARAM).getJSONArray(Constants.DATA_DF_CHANGE_PARAM).get(1));

        for (int i = 0; i < tableJsonArray.length(); i++) {
            JSONObject tableElementJsonObject = tableJsonArray.getJSONObject(i);
            Float df_meas = Math.abs(handleNullValue(tableElementJsonObject.getString(Constants.TABLE_DF_MEAS_PARAM)));
            Float df_change = Math.abs(handleNullValue(tableElementJsonObject.getString(Constants.TABLE_DF_CHANGE_PARAM)));

            if (df_meas > bad_lower_limit_df_meas
                    || df_change > bad_lower_limit_df_change
            ) {
                tableElementJsonObject.put(Constants.TABLE_CONDITION_INDICATOR_DF_PARAM, IndicatorEnum.Bad.name());
            } else if (df_meas > poor_lower_limit_df_meas && df_meas <= poor_upper_limit_df_meas
                    || df_change > poor_lower_limit_df_change && df_change <= poor_upper_limit_df_change
            ) {
                tableElementJsonObject.put(Constants.TABLE_CONDITION_INDICATOR_DF_PARAM, IndicatorEnum.Poor.name());
            } else if (df_meas > fair_lower_limit_df_meas && df_meas <= fair_upper_limit_df_meas
                    || df_change > fair_lower_limit_df_change && df_change <= fair_upper_limit_df_change
            ) {
                tableElementJsonObject.put(Constants.TABLE_CONDITION_INDICATOR_DF_PARAM, IndicatorEnum.Fair.name());
            } else if (df_meas <= good_upper_limit_df_meas || df_change <= good_upper_limit_df_change) {
                tableElementJsonObject.put(Constants.TABLE_CONDITION_INDICATOR_DF_PARAM, IndicatorEnum.Good.name());
            } else {
                char[] cArr = {};
                tableElementJsonObject.put(Constants.TABLE_CONDITION_INDICATOR_DF_PARAM, String.valueOf(cArr));
            }
        }
    }

    private Map<String, Float> calculateWorstScore(String data) {
        Map<String, Float> dictionary = new HashMap<String, Float>();
        Float worstScore = Float.valueOf(0);
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(data);
            JSONArray tableJsonArray = null;
            if (jsonObject.has(Constants.DATA_TABLE_PARAM)) {
                tableJsonArray = jsonObject.getJSONArray(Constants.DATA_TABLE_PARAM);
            }
            if (TestTypeEnum.WINDINGDFCAP.name().equalsIgnoreCase(jsonObject.getString(Constants.DATA_CODE_PARAM))
                    || TestTypeEnum.BUSHINGPRIMC1.name().equalsIgnoreCase(jsonObject.getString(Constants.DATA_CODE_PARAM))
                    || TestTypeEnum.BUSHINGPRIMC2.name().equalsIgnoreCase(jsonObject.getString(Constants.DATA_CODE_PARAM))
            ) {
                List<Float> dfScoreList = new ArrayList<>();
                List<Float> cScoreList = new ArrayList<>();
                Float worstDFScore = Float.valueOf(0);
                Float worstCScore = Float.valueOf(0);

                for (int i = 0; i < tableJsonArray.length(); i++) {
                    JSONObject tableItem = tableJsonArray.getJSONObject(i);
                    dfScoreList.add((float) indicatorToScore(tableItem.getString(Constants.TABLE_CONDITION_INDICATOR_DF_PARAM)));
                    cScoreList.add((float) indicatorToScore(tableItem.getString(Constants.TABLE_CONDITION_INDICATOR_C_PARAM)));
                }
                dfScoreList = dfScoreList.stream().filter(aFloat -> aFloat != -1).collect(Collectors.toList());
                cScoreList = cScoreList.stream().filter(aFloat -> aFloat != -1).collect(Collectors.toList());


                worstDFScore = dfScoreList == null || dfScoreList.isEmpty() ? Float.valueOf(0) : Collections.min(dfScoreList);
                worstCScore = cScoreList == null || cScoreList.isEmpty() ? Float.valueOf(0) : Collections.min(cScoreList);
                dictionary.put("worstDFScore", worstDFScore);
                dictionary.put("worstCScore", worstCScore);
                dictionary.put("worstScore", worstScore);


            } else if (TestTypeEnum.DCWINDINGPRIM.name().equalsIgnoreCase(jsonObject.getString(Constants.DATA_CODE_PARAM))
                    || TestTypeEnum.DCWINDINGSEC.name().equalsIgnoreCase(jsonObject.getString(Constants.DATA_CODE_PARAM))
                    || TestTypeEnum.EXCITINGCURRENT.name().equalsIgnoreCase(jsonObject.getString(Constants.DATA_CODE_PARAM))
                    || TestTypeEnum.RATIOPRIMSEC.name().equalsIgnoreCase(jsonObject.getString(Constants.DATA_CODE_PARAM))
                    || TestTypeEnum.INSULATIONRESISTANCE.name().equalsIgnoreCase(jsonObject.getString(Constants.DATA_CODE_PARAM))
            ) {
                List<Float> scoreList = new ArrayList<>();
                for (int i = 0; i < tableJsonArray.length(); i++) {
                    JSONObject tableItem = tableJsonArray.getJSONObject(i);
                    scoreList.add((float) indicatorToScore(tableItem.getString(Constants.TABLE_CONDITION_INDICATOR_PARAM)));
                }
                if (scoreList != null && !scoreList.isEmpty()) {
//                    for (Float i : scoreList) {
//                        if (i == -1) {
//                            scoreList.remove(i);
//                        }
//                    }
                    scoreList = scoreList.stream().filter(aFloat -> aFloat != -1).collect(Collectors.toList());

//                    worstScore = Collections.min(scoreList);
                    worstScore = scoreList == null || scoreList.isEmpty() ? Float.valueOf(0) : Collections.min(scoreList);

                    Float worstDFScore = Float.valueOf(0);
                    Float worstCScore = Float.valueOf(0);
                    dictionary.put("worstScore", worstScore);
                    dictionary.put("worstDFScore", worstDFScore);
                    dictionary.put("worstCScore", worstCScore);

                }
            } else if (TestTypeEnum.DGA.name().equalsIgnoreCase(jsonObject.getString(Constants.DATA_CODE_PARAM))
                    || TestTypeEnum.MEASUREMENTOFOIL.name().equalsIgnoreCase(jsonObject.getString(Constants.DATA_CODE_PARAM))
            ) {
                float score = indicatorToScore(jsonObject.getString(Constants.DATA_CONDITION_INDICATOR_PARAM));
                if(score == -1) {
                    score = 0;
                }


                Float worstDFScore = Float.valueOf(0);
                Float worstCScore = Float.valueOf(0);
                dictionary.put("worstScore", score);
                dictionary.put("worstDFScore", worstDFScore);
                dictionary.put("worstCScore", worstCScore);

            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error(e.toString());
            throw new InvalidTestException("Invalid test data");
        }

        return dictionary;
    }

    private Map<String, Float> calculateAverageScore(String data) {
        Map<String, Float> dictionary = new HashMap<String, Float>();
        Float averageScore = Float.valueOf(0);
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(data);
            JSONArray tableJsonArray = null;
            if (jsonObject.has(Constants.DATA_TABLE_PARAM)) {
                tableJsonArray = jsonObject.getJSONArray(Constants.DATA_TABLE_PARAM);
            }
            if (TestTypeEnum.WINDINGDFCAP.name().equalsIgnoreCase(jsonObject.getString(Constants.DATA_CODE_PARAM))
                    || TestTypeEnum.BUSHINGPRIMC1.name().equalsIgnoreCase(jsonObject.getString(Constants.DATA_CODE_PARAM))
                    || TestTypeEnum.BUSHINGPRIMC2.name().equalsIgnoreCase(jsonObject.getString(Constants.DATA_CODE_PARAM))
            ) {
                List<Float> dfScoreList = new ArrayList<>();
                List<Float> cScoreList = new ArrayList<>();
                Float averageDFScore = Float.valueOf(0);
                Float averageCScore = Float.valueOf(0);

                for (int i = 0; i < tableJsonArray.length(); i++) {
                    JSONObject tableItem = tableJsonArray.getJSONObject(i);
                    dfScoreList.add((float) indicatorToScore(tableItem.getString(Constants.TABLE_CONDITION_INDICATOR_DF_PARAM)));
                    cScoreList.add((float) indicatorToScore(tableItem.getString(Constants.TABLE_CONDITION_INDICATOR_C_PARAM)));
                }
                dfScoreList = dfScoreList.stream().filter(aFloat -> aFloat != -1).collect(Collectors.toList());
                cScoreList = cScoreList.stream().filter(aFloat -> aFloat != -1).collect(Collectors.toList());

                averageDFScore = (float) dfScoreList.stream().mapToDouble(f -> f).average().orElse(0);
                averageCScore = (float) cScoreList.stream().mapToDouble(f -> f).average().orElse(0);
                dictionary.put("averageDFScore", averageDFScore);
                dictionary.put("averageCScore", averageCScore);
                dictionary.put("averageScore", averageScore);
                return dictionary;
            } else if (TestTypeEnum.DCWINDINGPRIM.name().equalsIgnoreCase(jsonObject.getString(Constants.DATA_CODE_PARAM))
                    || TestTypeEnum.DCWINDINGSEC.name().equalsIgnoreCase(jsonObject.getString(Constants.DATA_CODE_PARAM))
                    || TestTypeEnum.EXCITINGCURRENT.name().equalsIgnoreCase(jsonObject.getString(Constants.DATA_CODE_PARAM))
                    || TestTypeEnum.RATIOPRIMSEC.name().equalsIgnoreCase(jsonObject.getString(Constants.DATA_CODE_PARAM))
                    || TestTypeEnum.INSULATIONRESISTANCE.name().equalsIgnoreCase(jsonObject.getString(Constants.DATA_CODE_PARAM))
            ) {
                List<Float> scoreList = new ArrayList<>();
                for (int i = 0; i < tableJsonArray.length(); i++) {
                    JSONObject tableItem = tableJsonArray.getJSONObject(i);
                    scoreList.add((float) indicatorToScore(tableItem.getString(Constants.TABLE_CONDITION_INDICATOR_PARAM)));
                }
                scoreList = scoreList.stream().filter(aFloat -> aFloat != -1).collect(Collectors.toList());
                Float averageDFScore = Float.valueOf(0);
                Float averageCScore = Float.valueOf(0);
                if (scoreList != null && !scoreList.isEmpty()) {
                    averageScore = (float) scoreList.stream().mapToDouble(f -> f).average().orElse(0);
                }

                dictionary.put("averageScore", averageScore);
                dictionary.put("averageDFScore", averageDFScore);
                dictionary.put("averageCScore", averageCScore);
            } else if (TestTypeEnum.DGA.name().equalsIgnoreCase(jsonObject.getString(Constants.DATA_CODE_PARAM))
                    || TestTypeEnum.MEASUREMENTOFOIL.name().equalsIgnoreCase(jsonObject.getString(Constants.DATA_CODE_PARAM))
            ) {
                float score = indicatorToScore(jsonObject.getString(Constants.DATA_CONDITION_INDICATOR_PARAM));
                if(score == -1) {
                    score = 0;
                }

                Float averageDFScore = Float.valueOf(0);
                Float averageCScore = Float.valueOf(0);
                dictionary.put("averageScore", score);
                dictionary.put("averageDFScore", averageDFScore);
                dictionary.put("averageCScore", averageCScore);
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error(e.toString());
            throw new InvalidTestException("Invalid test data");
        }
        return dictionary;
    }

    private int indicatorToScore(String indicatorStatus) {
        if (IndicatorEnum.Good.name().equalsIgnoreCase(indicatorStatus)) {
            return 3;
        } else if (IndicatorEnum.Fair.name().equalsIgnoreCase(indicatorStatus)) {
            return 2;
        } else if (IndicatorEnum.Poor.name().equalsIgnoreCase(indicatorStatus)) {
            return 1;
        } else if (IndicatorEnum.Bad.name().equalsIgnoreCase(indicatorStatus)) {
            return 0;
        } else {
            return -1;
        }
    }

    private void updateIndicatorStatusInsulationRes(JSONArray tableJsonArray,
                                                    Float good_kht_upper, Float fair_kht_upper, Float fair_kht_lower, Float poor_kht_upper, Float poor_kht_lower, Float bad_kht_lower,
                                                    Float good_r60_hve_upper, Float fair_r60_hve_upper, Float fair_r60_hve_lower, Float poor_r60_hve_upper, Float poor_r60_hve_lower, Float bad_r60_hve_lower,
                                                    Float good_r60_lve_upper, Float fair_r60_lve_upper, Float fair_r60_lve_lower, Float poor_r60_lve_upper, Float poor_r60_lve_lower, Float bad_r60_lve_lower,
                                                    String updatedTableParam, String comparedTableParam_kht, String comparedTableParam_r60) throws JSONException {

        for (int i = 0; i < tableJsonArray.length(); i++) {
            JSONObject tableElementJsonObject = tableJsonArray.getJSONObject(i);
            Float kht = Math.abs(handleNullValue(tableElementJsonObject.getString(comparedTableParam_kht)));
            Float r60_hve = Float.NaN;
            if (String.valueOf(tableElementJsonObject.getString(Constants.TYPE_COMMON)).equals(Constants.TYPE_HVE_PARAM)) {
                r60_hve = Math.abs(handleNullValue(tableElementJsonObject.getString(comparedTableParam_r60)));
            }
            Float r60_lve = Float.NaN;
            if (String.valueOf(tableElementJsonObject.getString(Constants.TYPE_COMMON)).equals(Constants.TYPE_LVE_PARAM)) {
                r60_lve = Math.abs(handleNullValue(tableElementJsonObject.getString(comparedTableParam_r60)));
            }


            if (kht <= bad_kht_lower || r60_hve <= bad_r60_hve_lower || r60_lve <= bad_r60_lve_lower) {
                tableElementJsonObject.put(updatedTableParam, IndicatorEnum.Bad.name());
            } else if ((poor_kht_upper > kht && kht >= poor_kht_lower) || (poor_r60_hve_upper > r60_hve && r60_hve >= poor_r60_hve_lower) || (poor_r60_lve_upper > r60_lve && r60_lve >= poor_r60_lve_lower)) {
                tableElementJsonObject.put(updatedTableParam, IndicatorEnum.Poor.name());
            } else if ((fair_kht_upper > kht && kht >= fair_kht_lower) || (fair_r60_hve_upper > r60_hve && r60_hve >= fair_r60_hve_lower) || (fair_r60_lve_upper > r60_lve && r60_lve >= fair_r60_lve_lower)) {
                tableElementJsonObject.put(updatedTableParam, IndicatorEnum.Fair.name());
            } else if (kht >= good_kht_upper || r60_hve >= good_r60_hve_upper || r60_lve >= good_r60_lve_upper) {
                tableElementJsonObject.put(updatedTableParam, IndicatorEnum.Good.name());
            } else {
                tableElementJsonObject.put(updatedTableParam, "");
            }
        }
    }

    private Float handleNullValue(@NotNull String value) {
        if (value.isEmpty()) {
            return Float.NaN;
        } else {
            return Float.valueOf(value);
        }
    }

}
