package vn.automationandtesting.atproject.service.impl;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import vn.automationandtesting.atproject.controller.dto.GroupDto;
import vn.automationandtesting.atproject.controller.dto.mapper.GroupMapper;
import vn.automationandtesting.atproject.entity.Group;
import vn.automationandtesting.atproject.entity.GroupPermission;
import vn.automationandtesting.atproject.entity.GroupPermissionPK;
import vn.automationandtesting.atproject.entity.Permission;
import vn.automationandtesting.atproject.entity.enumm.PermissionEnum;
import vn.automationandtesting.atproject.exception.ExistedGroupNameException;
import vn.automationandtesting.atproject.exception.GroupNotFoundException;
import vn.automationandtesting.atproject.exception.PermissionNotFoundException;
import vn.automationandtesting.atproject.repository.GroupPermissionRepository;
import vn.automationandtesting.atproject.repository.GroupRepository;
import vn.automationandtesting.atproject.repository.PermissionRepository;
import vn.automationandtesting.atproject.service.GroupService;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class GroupServiceImpl implements GroupService {
    @Autowired
    private GroupRepository groupRepository;
    @Autowired
    private PermissionRepository permissionRepository;
    @Autowired
    private GroupPermissionRepository groupPermissionRepository;
    @Autowired
    private GroupMapper groupMapper;

    @Override
    public List<GroupDto> getAllGroups() {
        List<Group> groups = groupRepository.findByIsDeleted(false);
        List<GroupDto> groupDtos = groups.stream().map(group -> groupMapper.groupToGroupDto(group)).collect(Collectors.toList());
        return groupDtos;
    }

    @Override
    public GroupDto createNewGroup(GroupDto groupDto) {
        if (groupRepository.existsByGroupNameIgnoreCase(groupDto.getGroupName())) {
            throw new ExistedGroupNameException();
        }
        Group group = groupMapper.groupDtoToGroup(groupDto);
        group.setId(UUID.randomUUID());
        group.setGroupName(group.getGroupName().toUpperCase());
        Group savedGroup = groupRepository.save(group);
        return groupMapper.groupToGroupDto(savedGroup);
    }

    @Override
    @Transactional
    public GroupDto updateGroup(GroupDto groupDto, UUID groupID) {
        //TODO check group id
        Group savedGroup = groupRepository.findByIdAndIsDeleted(groupID, false);
        if(savedGroup == null) {
            throw new GroupNotFoundException();
        }
        BeanUtils.copyProperties(groupDto, savedGroup, "id", "groupName");
        List<Permission> permissions;
        try {
            permissions = getPermission(groupDto.getPermissions());
        } catch (IllegalArgumentException e) {
            throw new PermissionNotFoundException();
        }
        //update permission of group
        List<GroupPermission> groupPermissionList = permissions.stream().map(permission ->
                        new GroupPermission(new GroupPermissionPK(savedGroup.getId(), permission.getId()), permission, savedGroup))
                .collect(Collectors.toList());
        //remove current relationship bt group and permission
        groupPermissionRepository.deleteByGroup(savedGroup);
        //save new group-permission relationship
        List<GroupPermission> savedGroupPermissions = groupPermissionRepository.saveAll(groupPermissionList);

        savedGroup.getGroupPermissions().clear();
        savedGroup.getGroupPermissions().addAll(savedGroupPermissions);

        Group group = groupRepository.save(savedGroup);
        return groupMapper.groupToGroupDto(group);
    }

    @Override
    public GroupDto updateGroupLite(GroupDto groupDto, UUID groupID) {
        Group savedGroup = groupRepository.findByIdAndIsDeleted(groupID, false);
        if(savedGroup == null) {
            throw new GroupNotFoundException();
        }
        BeanUtils.copyProperties(groupDto, savedGroup,"id");
        Group group = groupRepository.save(savedGroup);
        return groupMapper.groupToGroupDto(group);
    }

    @Override
    public void deleteGroup(String groupID) {
        UUID id = UUID.fromString(groupID);
        Group group = groupRepository.findById(id).orElseThrow(() -> new GroupNotFoundException());
        group.setIsDeleted(true);
        groupRepository.save(group);
    }

    @Override
    public GroupDto getGroupById(String groupID) {
        UUID id = UUID.fromString(groupID);
        Group group = groupRepository.findByIdAndIsDeleted(id, false);
        return groupMapper.groupToGroupDto(group);
    }

    private List<Permission> getPermission(Set<String> permissions) {
        List<Permission> permissionList = new ArrayList<>();
        for (String permission : permissions
        ) {
            PermissionEnum permissionEnum = PermissionEnum.valueOf(permission);
            Permission permissionEntity = permissionRepository.findByPermissionName(permissionEnum);
            if (permissionEntity == null) {
                throw new PermissionNotFoundException();
            }
            permissionList.add(permissionEntity);
        }
        return permissionList;
    }
}
