package vn.automationandtesting.atproject.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vn.automationandtesting.atproject.controller.dto.cim.TapChangerDto;
import vn.automationandtesting.atproject.controller.dto.mapper.TapChangerMapper;
import vn.automationandtesting.atproject.entity.cim.Asset;
import vn.automationandtesting.atproject.entity.cim.TapChanger;
import vn.automationandtesting.atproject.exception.AssetNotFoundException;
import vn.automationandtesting.atproject.exception.TapChangerNotFoundException;
import vn.automationandtesting.atproject.repository.AssetRepository;
import vn.automationandtesting.atproject.repository.TapChangerRepository;
import vn.automationandtesting.atproject.service.TapChangerService;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

/**
 * @author tund on 8/9/2022
 * @project at-project-server
 */

@Service
public class TapChangerServiceImpl implements TapChangerService {

    @Autowired
    private TapChangerRepository tapChangerRepository;

    @Autowired
    private TapChangerMapper tapChangerMapper;

    @Autowired
    private AssetRepository assetRepository;

    @Override
    public TapChangerDto createNewTapChanger(TapChangerDto tapChangerDto) {
        Asset asset = assetRepository.findByMridAndIsDeleted(tapChangerDto.getAsset_id(), false)
                .orElseThrow(() -> new AssetNotFoundException("Asset id not existed."));
        TapChanger tapChanger = tapChangerMapper.tapChangerDtoToTapChanger(tapChangerDto);
        if(tapChangerDto.getId() != null) {
            tapChanger.setMrid(tapChangerDto.getId());
        } else {
            tapChanger.setMrid(UUID.randomUUID());
        }
        tapChanger.setAssets(asset);
        tapChanger = tapChangerRepository.save(tapChanger);
        return tapChangerMapper.tapChangerToTapChangerDto(tapChanger);
    }

    @Override
    public TapChangerDto updateTapChanger(TapChangerDto tapChangerDto, UUID id) {
        TapChanger tapChanger = tapChangerRepository.findByMridAndIsDeleted(id, false)
                .orElseThrow(() -> new TapChangerNotFoundException("TapChanger id not existed"));
        Asset asset = assetRepository.findByMridAndIsDeleted(tapChangerDto.getAsset_id(), false)
                .orElseThrow(() -> new AssetNotFoundException("Asset id not existed."));
        tapChanger = tapChangerMapper.copyTapChangerDtoToTapChanger(tapChangerDto, tapChanger, "id");
        tapChanger.setAssets(asset);
        TapChanger updateTapChanger = tapChangerRepository.save(tapChanger);
        return tapChangerMapper.tapChangerToTapChangerDto(updateTapChanger);
    }

    @Override
    public void deleteTapChanger(UUID id) {
        TapChanger tapChanger = tapChangerRepository.findByMridAndIsDeleted(id, false)
                .orElseThrow(() -> new TapChangerNotFoundException("TapChanger id not existed."));
        tapChanger.setIsDeleted(true);
        tapChangerRepository.save(tapChanger);
    }

    @Override
    public List<TapChangerDto> findTapChangersByAssetId(UUID assetId) {
        List<TapChangerDto> tapChangerDtoList = new ArrayList<>();
        List<TapChanger> tapChangers = tapChangerRepository.findByAssetId(assetId, false);
        for (TapChanger tapChanger : tapChangers) {
            TapChangerDto tapChangerDto = tapChangerMapper.tapChangerToTapChangerDto(tapChanger);
            tapChangerDtoList.add(tapChangerDto);
        }
        return tapChangerDtoList;
    }

    @Override
    public List<TapChangerDto> findTapChangersByLocationId(UUID locationId) {
        List<TapChangerDto> tapChangerDtoList = new ArrayList<>();
        List<TapChanger> tapChangers = tapChangerRepository.findByLocationId(locationId, false);
        for (TapChanger tapChanger : tapChangers){
            TapChangerDto tapChangerDto = tapChangerMapper.tapChangerToTapChangerDto(tapChanger);
            tapChangerDtoList.add(tapChangerDto);
        }
        return tapChangerDtoList;
    }

    @Override
    public boolean checkTapChangerExisted(UUID id) {
        Optional<TapChanger> tapChanger = tapChangerRepository.findByMridAndIsDeleted(id, false);
        return tapChanger.isPresent();
    }
}
