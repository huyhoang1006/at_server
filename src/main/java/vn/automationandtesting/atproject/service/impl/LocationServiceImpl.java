package vn.automationandtesting.atproject.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import vn.automationandtesting.atproject.config.auth.BearerContextHolder;
import vn.automationandtesting.atproject.controller.dto.ListIdDto;
import vn.automationandtesting.atproject.controller.dto.ResourceFullDto;
import vn.automationandtesting.atproject.controller.dto.ResponseObject;
import vn.automationandtesting.atproject.controller.dto.cim.LocationDto;
import vn.automationandtesting.atproject.controller.dto.mapper.LocationMapper;
import vn.automationandtesting.atproject.entity.User;
import vn.automationandtesting.atproject.entity.cim.Job;
import vn.automationandtesting.atproject.entity.cim.Location;
import vn.automationandtesting.atproject.exception.LocationLockedException;
import vn.automationandtesting.atproject.exception.LocationNotFoundException;
import vn.automationandtesting.atproject.exception.UserNotFoundException;
import vn.automationandtesting.atproject.repository.LocationRepository;
import vn.automationandtesting.atproject.repository.UserRepository;
import vn.automationandtesting.atproject.service.LocationService;
import vn.automationandtesting.atproject.util.search.LocationSearchSpecification;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * @author tridv on 5/9/2022
 * @project at-project-server
 */
@Service
public class LocationServiceImpl implements LocationService {
    @Autowired
    private LocationRepository locationRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private LocationMapper locationMapper;

    @Override
    public LocationDto createLocation(LocationDto locationDto) {
        Location location = locationMapper.locationDtoToLocation(locationDto);
        String currentUserId = BearerContextHolder.getContext().getUserId();
        if (locationDto.getId() != null) {
            location.setMrid(locationDto.getId());
        } else {
            location.setMrid(UUID.randomUUID());
        }
        User user = userRepository
                .findByIdAndIsDeleted(UUID.fromString(currentUserId), false)
                .orElseThrow(() -> new UserNotFoundException("Invalid user's id."));
        location.setUser(user);
        Location savedLocation = locationRepository.save(location);
        return locationMapper.locationToLocationDto(savedLocation);
    }

    @Override
    public Location insertIfNotExist(LocationDto locationDto) {
        Location location = locationMapper.locationDtoToLocation(locationDto);
        UUID locationId = location.getMrid();

        Location locationExist = locationRepository.findById(locationId).orElse(null);
        if (locationExist == null) {
            UUID userId = location.getUser_id();
            User user = userRepository.findById(userId).get();
            location.setUser(user);
            return locationRepository.save(location);
        }
        else {
            if (!locationExist.isLocked()) {
                locationExist = locationMapper.copyLocationDtoToLocation(locationDto,
                        locationExist, "id", "user_id",
                        "collabs");
                return locationRepository.save(locationExist);
            }
            else{ 
                return locationExist;
            }
        }

    }

    @Override
    public void updateJobCollabs(String locationId, ListIdDto listIdDto) {
        String userId = BearerContextHolder.getContext().getUserId();
        Location location = getLocationByIdAndLoggedInUserID(UUID.fromString(locationId), UUID.fromString(userId));
        List<String> userIdList = listIdDto.getListId().stream().map(UUID::toString).collect(Collectors.toList());
        String commaSeperatedUserIdString = String.join(",", userIdList);
        location.setCollabs(commaSeperatedUserIdString);
        locationRepository.save(location);
    }

    @Override
    public LocationDto updateLocation(UUID id, LocationDto locationDto) {
        String userID = BearerContextHolder.getContext().getUserId();
        Location savedLocation = getLocationByIdAndLoggedInUserID(id, UUID.fromString(userID));

        if (savedLocation.isLocked()) {
            if (!savedLocation.getLocked_by().toString().equals(userID)) {
                throw new LocationLockedException("Location already locked by another user.");
            }
        } else {
            if (locationDto.isLocked()) {
                savedLocation.setLocked_by(UUID.fromString(userID));
            }
        }
        savedLocation = locationMapper.copyLocationDtoToLocation(locationDto, savedLocation, "id", "user_id",
                "collabs", "locked");
        Location updatedLocation = locationRepository.save(savedLocation);
        return locationMapper.locationToLocationDto(updatedLocation);
    }

    @Override
    public ResponseObject lock(Boolean locked, List<UUID> listId) {
        List<Location> locations = locationRepository.findAllById(listId);
        String userID = BearerContextHolder.getContext().getUserId();
        for (Location location : locations) {
            if (!(location.isLocked() && !location.getLocked_by().toString().equals(userID))) {
                location.setLocked(locked);
                if (locked) {
                    location.setLocked_by(UUID.fromString(userID));
                }
            }
        }
        locationRepository.saveAll(locations);

        return ResponseObject.returnWithData(true);
    }

    @Override
    public ResponseObject download(List<UUID> listId) {
        List<Location> locations = locationRepository.findAllById(listId);
        List<ResourceFullDto> res = locations.stream()
                .map(location -> new ResourceFullDto(null,null,null, locationMapper.locationToLocationDto(
                        location), null,null) )
                .collect(Collectors.toList());
        return ResponseObject.returnWithData(res);
    }

    @Override
    public ResponseObject upload(ResourceFullDto resourceFullDto) {
        String currentUserId = BearerContextHolder.getContext().getUserId();
        User user = userRepository.findById(UUID.fromString(currentUserId)).get();

        List<LocationDto> listLocation = resourceFullDto.getListLocation();
        List<UUID> locationId = listLocation.stream()
                .map(location -> location.getId())
                .collect(Collectors.toList());

        List<Location> listLocationExist = locationRepository.findAllById(locationId);
        List<UUID> locationIdExist = listLocationExist.stream()
                .map(location -> location.getMrid())
                .collect(Collectors.toList());

        List<LocationDto> listLocationInsert = listLocation.stream()
                .filter(location -> !locationIdExist.contains(location.getId()))
                .collect(Collectors.toList());

        List<Location> locations = listLocationInsert.stream()
                .map(location -> locationMapper.locationDtoToLocation(location))
                .collect(Collectors.toList());

        for (Location location : locations) {
            location.setUser(user);
        }
        
        // location không bị lock
        List<Location> listLocationUpdate = listLocationExist.stream()
                .filter(location -> !location.isLocked())
                .collect(Collectors.toList());
        // id location không bị lock
        List<UUID> listIdLocationUpdate = listLocationUpdate.stream()
                .map(location -> location.getMrid())
                .collect(Collectors.toList());
        // location dto
        List<LocationDto> listLocationDtoUpdate = listLocation.stream()
                .filter(locationDto -> listIdLocationUpdate.contains(locationDto.getId()))
                .collect(Collectors.toList());
        // update
        List<Location> listUpdate = new ArrayList<>();
        for (LocationDto locationDto : listLocationDtoUpdate) {
            Location newLocation = locationRepository.findById(locationDto.getId()).get();
            newLocation = locationMapper.copyLocationDtoToLocation(locationDto,
                    newLocation, "id", "user_id", "collabs", "locked");
            listUpdate.add(newLocation);
        }

        locationRepository.saveAll(listUpdate);
        locationRepository.saveAll(locations);
        
        return ResponseObject.returnWithData(true);
    }

    @Override
    public void deleteLocation(UUID id) {
        String userID = BearerContextHolder.getContext().getUserId();
        Location location = getLocationByIdAndLoggedInUserID(id, UUID.fromString(userID));
        // Không lock mới được phép xóa
        if (!location.isLocked()) {
            locationRepository.delete(location);
        }
        else {
            throw new LocationLockedException("Please unlock location");
        }
    }

    @Override
    public LocationDto getLocationById(UUID id) {
        String userID = BearerContextHolder.getContext().getUserId();
        Location location = getLocationByIdAndLoggedInUserID(id, UUID.fromString(userID));
        return locationMapper.locationToLocationDto(location);
    }

    @Override
    public List<LocationDto> getLocationByRefId(UUID id) {
        List<Location> locations = locationRepository.findAllByRefId(id.toString());
        List<LocationDto> locationDtoList = new ArrayList<>();
        for(Location item : locations) {
            LocationDto locationDto = locationMapper.locationToLocationDto(item);
            locationDtoList.add(locationDto);
        }
        return locationDtoList;
    }

    @Override
    public List<LocationDto> searchLocations(UUID id, Pageable pageable) {
        User user = null;
        if (id != null) {
            user = new User();
            user.setId(id);
        }
        String userID = BearerContextHolder.getContext().getUserId();
        LocationSearchSpecification locationSearchSpecification = new LocationSearchSpecification(user, false,
                UUID.fromString(userID), userID);
        List<Location> locations = locationRepository.findAll(locationSearchSpecification);
        List<LocationDto> locationDtoList = locations.stream()
                .map(location -> locationMapper.locationToLocationDto(location))
                .collect(Collectors.toList());
        return locationDtoList;
    }

    private Location getLocationByIdAndLoggedInUserID(UUID locationId, UUID loggedInUserId) {
        Optional<Location> location = locationRepository.findByMridAndIsDeletedAndCreatedBy(locationId, false,
                loggedInUserId);
        if (location.isEmpty()) {
            location = locationRepository.findByMridAndIsDeletedAndCollabsContains(locationId, false,
                    loggedInUserId.toString());
        }
        return location.orElseThrow(() -> new LocationNotFoundException("Location not found."));
    }
}
