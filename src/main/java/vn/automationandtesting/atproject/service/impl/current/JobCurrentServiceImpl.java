package vn.automationandtesting.atproject.service.impl.current;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vn.automationandtesting.atproject.config.auth.BearerContextHolder;
import vn.automationandtesting.atproject.controller.dto.circuit.JobCircuitDto;
import vn.automationandtesting.atproject.controller.dto.current.JobCurrentDto;
import vn.automationandtesting.atproject.controller.dto.mapper.circuit.JobCircuitMapper;
import vn.automationandtesting.atproject.controller.dto.mapper.current.JobCurrentMapper;
import vn.automationandtesting.atproject.entity.circuit.Jobdata;
import vn.automationandtesting.atproject.entity.current.JobsCurrent;
import vn.automationandtesting.atproject.repository.circuit.JobCircuitRepository;
import vn.automationandtesting.atproject.repository.current.JobCurrentRepository;
import vn.automationandtesting.atproject.service.circuit.JobCircuitService;
import vn.automationandtesting.atproject.service.current.JobCurrentService;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
public class JobCurrentServiceImpl implements JobCurrentService {

    @Autowired
    private JobCurrentRepository repository;

    @Autowired
    private JobCurrentMapper mapper;

    @Override
    public List<JobCurrentDto> findAllJobByAssetId(UUID asset_id) {
        String userId = BearerContextHolder.getContext().getUserId();
        UUID user_id = UUID.fromString(userId);
        List<JobsCurrent> jobs = repository.findAllJobByAssetId(asset_id, user_id, userId);
        List<JobCurrentDto> jobsDto = new ArrayList<>();
        if(jobs.size() != 0) {
            for(JobsCurrent item : jobs) {
                JobCurrentDto jobDto = mapper.JobToJobDto(item);
                jobsDto.add(jobDto);
            }
        }
        return jobsDto;
    }

    @Override
    public List<JobCurrentDto> findJobById(UUID id) {
        List<JobsCurrent> jobs = repository.findJobById(id);
        List<JobCurrentDto> jobsDto = new ArrayList<>();
        if(jobs.size() != 0) {
            for(JobsCurrent item : jobs) {
                JobCurrentDto jobDto = mapper.JobToJobDto(item);
                jobsDto.add(jobDto);
            }
        }
        return jobsDto;
    }

    @Override
    public List<JobCurrentDto> findJobByName(String name) {
        List<JobsCurrent> jobs = repository.findJobByName(name);
        List<JobCurrentDto> jobsDto = new ArrayList<>();
        if(jobs.size() != 0) {
            for(JobsCurrent item : jobs) {
                JobCurrentDto jobDto = mapper.JobToJobDto(item);
                jobsDto.add(jobDto);
            }
        }
        return jobsDto;
    }

    @Override
    public List<JobCurrentDto> findJobByNameAndAsset(String name, UUID asset_id) {
        List<JobsCurrent> jobs = repository.findJobByNameAndAsset(name, asset_id);
        List<JobCurrentDto> jobsDto = new ArrayList<>();
        if(jobs.size() != 0) {
            for(JobsCurrent item : jobs) {
                JobCurrentDto jobDto = mapper.JobToJobDto(item);
                jobsDto.add(jobDto);
            }
        }
        return jobsDto;
    }

    @Override
    public List<JobCurrentDto> findAll() {
        List<JobsCurrent> jobs = repository.findAll();
        List<JobCurrentDto> jobsDto = new ArrayList<>();
        if(jobs.size() != 0) {
            for(JobsCurrent item : jobs) {
                JobCurrentDto jobDto = mapper.JobToJobDto(item);
                jobsDto.add(jobDto);
            }
        }
        return jobsDto;
    }

    @Override
    public List<JobCurrentDto> findByAssetIdAndCollabsContaining(UUID asset_id, String userId) {
        List<JobsCurrent> jobs = repository.findByAssetIdAndCollabsContaining(asset_id, userId);
        List<JobCurrentDto> jobsDto = new ArrayList<>();
        if(jobs.size() != 0) {
            for(JobsCurrent item : jobs) {
                JobCurrentDto jobDto = mapper.JobToJobDto(item);
                jobsDto.add(jobDto);
            }
        }
        return jobsDto;
    }

    @Override
    public void saveAll(List<JobsCurrent> jobdataList) {
        repository.saveAll(jobdataList);
    }

    @Override
    public void deleteAll(List<JobsCurrent> jobdataList) {
        repository.deleteAll(jobdataList);
    }
}
