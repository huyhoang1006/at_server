package vn.automationandtesting.atproject.service.impl.power;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vn.automationandtesting.atproject.config.auth.BearerContextHolder;
import vn.automationandtesting.atproject.controller.dto.mapper.power.PowerCableMapper;
import vn.automationandtesting.atproject.controller.dto.power.PowerCableDto;
import vn.automationandtesting.atproject.entity.power.PowerCable;
import vn.automationandtesting.atproject.repository.power.PowerRepository;
import vn.automationandtesting.atproject.service.power.PowerCableService;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
public class PowerCableServiceImpl implements PowerCableService {

    @Autowired
    private PowerRepository repository;

    @Autowired
    private PowerCableMapper mapper;


    @Override
    public List<PowerCableDto> findAllByLocationId(UUID location_id) {
        String userId = BearerContextHolder.getContext().getUserId();
        UUID user_id = UUID.fromString(userId);
        List<PowerCable> list = repository.findAllByLocationId(location_id, user_id, userId);
        List<PowerCableDto> dtoList = new ArrayList<>();
        if(!list.isEmpty()) {
            for(PowerCable item : list) {
                PowerCableDto itemDto = mapper.assetToAssetDto(item);
                dtoList.add(itemDto);
            }
        }
        return dtoList;
    }

    @Override
    public List<PowerCableDto> findAssetById(UUID id) {
        List<PowerCable> list = repository.findAssetById(id);
        List<PowerCableDto> dtoList = new ArrayList<>();
        if(!list.isEmpty()) {
            for(PowerCable item : list) {
                PowerCableDto itemDto = mapper.assetToAssetDto(item);
                dtoList.add(itemDto);
            }
        }
        return dtoList;
    }

    @Override
    public List<PowerCableDto> findBySerial(String serial) {
        List<PowerCable> list = repository.findBySerial(serial);
        List<PowerCableDto> dtoList = new ArrayList<>();
        if(!list.isEmpty()) {
            for(PowerCable item : list) {
                PowerCableDto itemDto = mapper.assetToAssetDto(item);
                dtoList.add(itemDto);
            }
        }
        return dtoList;
    }

    @Override
    public List<PowerCableDto> findBySerialAndLocation(String serial, UUID location_id) {
        List<PowerCable> list = repository.findBySerialAndLocation(serial, location_id);
        List<PowerCableDto> dtoList = new ArrayList<>();
        if(!list.isEmpty()) {
            for(PowerCable item : list) {
                PowerCableDto itemDto = mapper.assetToAssetDto(item);
                dtoList.add(itemDto);
            }
        }
        return dtoList;
    }

    @Override
    public List<PowerCableDto> findAll() {
        List<PowerCable> list = repository.findAll();
        List<PowerCableDto> dtoList = new ArrayList<>();
        if(!list.isEmpty()) {
            for(PowerCable item : list) {
                PowerCableDto itemDto = mapper.assetToAssetDto(item);
                dtoList.add(itemDto);
            }
        }
        return dtoList;
    }

    @Override
    public List<PowerCableDto> findByLocationIdAndCollabsContaining(UUID location_id, String userId) {
        List<PowerCable> list = repository.findByLocationIdAndCollabsContaining(location_id, userId);
        List<PowerCableDto> dtoList = new ArrayList<>();
        if(!list.isEmpty()) {
            for(PowerCable item : list) {
                PowerCableDto itemDto = mapper.assetToAssetDto(item);
                dtoList.add(itemDto);
            }
        }
        return dtoList;
    }

    @Override
    public void saveAll(List<PowerCable> list) {
        repository.saveAll(list);
    }

    @Override
    public void deleteAll(List<PowerCable> list) {
        repository.deleteAll(list);
    }
}
