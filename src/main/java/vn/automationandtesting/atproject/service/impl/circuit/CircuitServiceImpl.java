package vn.automationandtesting.atproject.service.impl.circuit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vn.automationandtesting.atproject.config.auth.BearerContextHolder;
import vn.automationandtesting.atproject.controller.dto.circuit.CircuitDto;
import vn.automationandtesting.atproject.controller.dto.mapper.circuit.CircuitMapper;
import vn.automationandtesting.atproject.entity.circuit.Circuit;
import vn.automationandtesting.atproject.repository.circuit.CircuitRepository;
import vn.automationandtesting.atproject.service.circuit.CircuitService;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
public class CircuitServiceImpl implements CircuitService {

    @Autowired
    private CircuitRepository circuitRepository;

    @Autowired
    private CircuitMapper circuitMapper;

    @Override
    public List<CircuitDto> findAllByLocationId(UUID location_id) {
        String userId = BearerContextHolder.getContext().getUserId();
        UUID user_id = UUID.fromString(userId);
        List<Circuit> circuitList = circuitRepository.findAllByLocationId(location_id, user_id, userId);
        List<CircuitDto> circuitDtoList = new ArrayList<>();
        if(!circuitList.isEmpty()) {
            for(Circuit circuit : circuitList) {
                CircuitDto circuitDto = circuitMapper.circuitToCircuitDto(circuit);
                circuitDtoList.add(circuitDto);
            }
        }
        return circuitDtoList;
    }

    @Override
    public List<CircuitDto> findAssetById(UUID id) {
        List<Circuit> circuitList = circuitRepository.findAssetById(id);
        List<CircuitDto> circuitDtoList = new ArrayList<>();
        if(!circuitList.isEmpty()) {
            for(Circuit circuit : circuitList) {
                CircuitDto circuitDto = circuitMapper.circuitToCircuitDto(circuit);
                circuitDtoList.add(circuitDto);
            }
        }
        return circuitDtoList;
    }

    @Override
    public List<CircuitDto> findBySerial(String serial) {
        List<Circuit> circuitList = circuitRepository.findBySerial(serial);
        List<CircuitDto> circuitDtoList = new ArrayList<>();
        if(!circuitList.isEmpty()) {
            for(Circuit circuit : circuitList) {
                CircuitDto circuitDto = circuitMapper.circuitToCircuitDto(circuit);
                circuitDtoList.add(circuitDto);
            }
        }
        return circuitDtoList;
    }

    @Override
    public List<CircuitDto> findBySerialAndLocation(String serial, UUID location_id) {
        List<Circuit> circuitList = circuitRepository.findBySerialAndLocation(serial, location_id);
        List<CircuitDto> circuitDtoList = new ArrayList<>();
        if(!circuitList.isEmpty()) {
            for(Circuit circuit : circuitList) {
                CircuitDto circuitDto = circuitMapper.circuitToCircuitDto(circuit);
                circuitDtoList.add(circuitDto);
            }
        }
        return circuitDtoList;
    }

    @Override
    public List<CircuitDto> findAll() {
        List<Circuit> circuitList = circuitRepository.findAll();
        List<CircuitDto> circuitDtoList = new ArrayList<>();
        if(!circuitList.isEmpty()) {
            for(Circuit circuit : circuitList) {
                CircuitDto circuitDto = circuitMapper.circuitToCircuitDto(circuit);
                circuitDtoList.add(circuitDto);
            }
        }
        return circuitDtoList;
    }

    @Override
    public List<CircuitDto> findByLocationIdAndCollabsContaining(UUID location_id, String userId) {
        List<Circuit> circuitList = circuitRepository.findByLocationIdAndCollabsContaining(location_id, userId);
        List<CircuitDto> circuitDtoList = new ArrayList<>();
        if(!circuitList.isEmpty()) {
            for(Circuit circuit : circuitList) {
                CircuitDto circuitDto = circuitMapper.circuitToCircuitDto(circuit);
                circuitDtoList.add(circuitDto);
            }
        }
        return circuitDtoList;
    }

    @Override
    public void saveAll(List<Circuit> circuits) {
        circuitRepository.saveAll(circuits);
    }

    @Override
    public void deleteAll(List<Circuit> circuits) {
        circuitRepository.deleteAll(circuits);
    }

}
