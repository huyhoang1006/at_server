package vn.automationandtesting.atproject.service.impl.surge;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vn.automationandtesting.atproject.config.auth.BearerContextHolder;
import vn.automationandtesting.atproject.controller.dto.mapper.surge.SurgeMapper;
import vn.automationandtesting.atproject.controller.dto.surge.SurgeDto;
import vn.automationandtesting.atproject.entity.surge.Surge;
import vn.automationandtesting.atproject.repository.surge.SurgeRepository;
import vn.automationandtesting.atproject.service.surge.SurgeService;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
public class SurgeServiceImpl implements SurgeService {

    @Autowired
    private SurgeRepository repository;

    @Autowired
    private SurgeMapper mapper;


    @Override
    public List<SurgeDto> findAllByLocationId(UUID location_id) {
        String userId = BearerContextHolder.getContext().getUserId();
        UUID user_id = UUID.fromString(userId);
        List<Surge> list = repository.findAllByLocationId(location_id, user_id, userId);
        List<SurgeDto> dtoList = new ArrayList<>();
        if(!list.isEmpty()) {
            for(Surge item : list) {
                SurgeDto itemDto = mapper.assetToAssetDto(item);
                dtoList.add(itemDto);
            }
        }
        return dtoList;
    }

    @Override
    public List<SurgeDto> findAssetById(UUID id) {
        List<Surge> list = repository.findAssetById(id);
        List<SurgeDto> dtoList = new ArrayList<>();
        if(!list.isEmpty()) {
            for(Surge item : list) {
                SurgeDto itemDto = mapper.assetToAssetDto(item);
                dtoList.add(itemDto);
            }
        }
        return dtoList;
    }

    @Override
    public List<SurgeDto> findBySerial(String serial) {
        List<Surge> list = repository.findBySerial(serial);
        List<SurgeDto> dtoList = new ArrayList<>();
        if(!list.isEmpty()) {
            for(Surge item : list) {
                SurgeDto itemDto = mapper.assetToAssetDto(item);
                dtoList.add(itemDto);
            }
        }
        return dtoList;
    }

    @Override
    public List<SurgeDto> findBySerialAndLocation(String serial, UUID location_id) {
        List<Surge> list = repository.findBySerialAndLocation(serial, location_id);
        List<SurgeDto> dtoList = new ArrayList<>();
        if(!list.isEmpty()) {
            for(Surge item : list) {
                SurgeDto itemDto = mapper.assetToAssetDto(item);
                dtoList.add(itemDto);
            }
        }
        return dtoList;
    }

    @Override
    public List<SurgeDto> findAll() {
        List<Surge> list = repository.findAll();
        List<SurgeDto> dtoList = new ArrayList<>();
        if(!list.isEmpty()) {
            for(Surge item : list) {
                SurgeDto itemDto = mapper.assetToAssetDto(item);
                dtoList.add(itemDto);
            }
        }
        return dtoList;
    }

    @Override
    public List<SurgeDto> findByLocationIdAndCollabsContaining(UUID location_id, String userId) {
        List<Surge> list = repository.findByLocationIdAndCollabsContaining(location_id, userId);
        List<SurgeDto> dtoList = new ArrayList<>();
        if(!list.isEmpty()) {
            for(Surge item : list) {
                SurgeDto itemDto = mapper.assetToAssetDto(item);
                dtoList.add(itemDto);
            }
        }
        return dtoList;
    }

    @Override
    public void saveAll(List<Surge> list) {
        repository.saveAll(list);
    }

    @Override
    public void deleteAll(List<Surge> list) {
        repository.deleteAll(list);
    }
}
