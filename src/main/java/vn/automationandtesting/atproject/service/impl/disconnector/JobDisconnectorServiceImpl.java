package vn.automationandtesting.atproject.service.impl.disconnector;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vn.automationandtesting.atproject.config.auth.BearerContextHolder;
import vn.automationandtesting.atproject.controller.dto.disconnector.JobDisconnectorDto;
import vn.automationandtesting.atproject.controller.dto.mapper.disconnector.JobDisconnectorMapper;
import vn.automationandtesting.atproject.entity.disconnector.JobsDisconnector;
import vn.automationandtesting.atproject.repository.disconnector.JobDisconnectorRepository;
import vn.automationandtesting.atproject.service.disconnector.JobDisconnectorService;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
public class JobDisconnectorServiceImpl implements JobDisconnectorService {

    @Autowired
    private JobDisconnectorRepository repository;

    @Autowired
    private JobDisconnectorMapper mapper;

    @Override
    public List<JobDisconnectorDto> findAllJobByAssetId(UUID asset_id) {
        String userId = BearerContextHolder.getContext().getUserId();
        UUID user_id = UUID.fromString(userId);
        List<JobsDisconnector> jobs = repository.findAllJobByAssetId(asset_id, user_id, userId);
        List<JobDisconnectorDto> jobsDto = new ArrayList<>();
        if(jobs.size() != 0) {
            for(JobsDisconnector item : jobs) {
                JobDisconnectorDto jobDto = mapper.JobToJobDto(item);
                jobsDto.add(jobDto);
            }
        }
        return jobsDto;
    }

    @Override
    public List<JobDisconnectorDto> findJobById(UUID id) {
        List<JobsDisconnector> jobs = repository.findJobById(id);
        List<JobDisconnectorDto> jobsDto = new ArrayList<>();
        if(jobs.size() != 0) {
            for(JobsDisconnector item : jobs) {
                JobDisconnectorDto jobDto = mapper.JobToJobDto(item);
                jobsDto.add(jobDto);
            }
        }
        return jobsDto;
    }

    @Override
    public List<JobDisconnectorDto> findJobByName(String name) {
        List<JobsDisconnector> jobs = repository.findJobByName(name);
        List<JobDisconnectorDto> jobsDto = new ArrayList<>();
        if(jobs.size() != 0) {
            for(JobsDisconnector item : jobs) {
                JobDisconnectorDto jobDto = mapper.JobToJobDto(item);
                jobsDto.add(jobDto);
            }
        }
        return jobsDto;
    }

    @Override
    public List<JobDisconnectorDto> findJobByNameAndAsset(String name, UUID asset_id) {
        List<JobsDisconnector> jobs = repository.findJobByNameAndAsset(name, asset_id);
        List<JobDisconnectorDto> jobsDto = new ArrayList<>();
        if(jobs.size() != 0) {
            for(JobsDisconnector item : jobs) {
                JobDisconnectorDto jobDto = mapper.JobToJobDto(item);
                jobsDto.add(jobDto);
            }
        }
        return jobsDto;
    }

    @Override
    public List<JobDisconnectorDto> findAll() {
        List<JobsDisconnector> jobs = repository.findAll();
        List<JobDisconnectorDto> jobsDto = new ArrayList<>();
        if(jobs.size() != 0) {
            for(JobsDisconnector item : jobs) {
                JobDisconnectorDto jobDto = mapper.JobToJobDto(item);
                jobsDto.add(jobDto);
            }
        }
        return jobsDto;
    }

    @Override
    public List<JobDisconnectorDto> findByAssetIdAndCollabsContaining(UUID asset_id, String userId) {
        List<JobsDisconnector> jobs = repository.findByAssetIdAndCollabsContaining(asset_id, userId);
        List<JobDisconnectorDto> jobsDto = new ArrayList<>();
        if(jobs.size() != 0) {
            for(JobsDisconnector item : jobs) {
                JobDisconnectorDto jobDto = mapper.JobToJobDto(item);
                jobsDto.add(jobDto);
            }
        }
        return jobsDto;
    }

    @Override
    public void saveAll(List<JobsDisconnector> jobdataList) {
        repository.saveAll(jobdataList);
    }

    @Override
    public void deleteAll(List<JobsDisconnector> jobdataList) {
        repository.deleteAll(jobdataList);
    }
}
