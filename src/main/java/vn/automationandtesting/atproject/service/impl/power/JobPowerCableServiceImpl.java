package vn.automationandtesting.atproject.service.impl.power;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vn.automationandtesting.atproject.config.auth.BearerContextHolder;
import vn.automationandtesting.atproject.controller.dto.mapper.power.JobPowerCableMapper;
import vn.automationandtesting.atproject.controller.dto.power.JobPowerCableDto;
import vn.automationandtesting.atproject.entity.power.JobsPowerCable;
import vn.automationandtesting.atproject.repository.power.JobPowerRepository;
import vn.automationandtesting.atproject.service.power.JobPowerCableService;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
public class JobPowerCableServiceImpl implements JobPowerCableService {

    @Autowired
    private JobPowerRepository repository;

    @Autowired
    private JobPowerCableMapper mapper;

    @Override
    public List<JobPowerCableDto> findAllJobByAssetId(UUID asset_id) {
        String userId = BearerContextHolder.getContext().getUserId();
        UUID user_id = UUID.fromString(userId);
        List<JobsPowerCable> jobs = repository.findAllJobByAssetId(asset_id, user_id, userId);
        List<JobPowerCableDto> jobsDto = new ArrayList<>();
        if(jobs.size() != 0) {
            for(JobsPowerCable item : jobs) {
                JobPowerCableDto jobDto = mapper.JobToJobDto(item);
                jobsDto.add(jobDto);
            }
        }
        return jobsDto;
    }

    @Override
    public List<JobPowerCableDto> findJobById(UUID id) {
        List<JobsPowerCable> jobs = repository.findJobById(id);
        List<JobPowerCableDto> jobsDto = new ArrayList<>();
        if(jobs.size() != 0) {
            for(JobsPowerCable item : jobs) {
                JobPowerCableDto jobDto = mapper.JobToJobDto(item);
                jobsDto.add(jobDto);
            }
        }
        return jobsDto;
    }

    @Override
    public List<JobPowerCableDto> findJobByName(String name) {
        List<JobsPowerCable> jobs = repository.findJobByName(name);
        List<JobPowerCableDto> jobsDto = new ArrayList<>();
        if(jobs.size() != 0) {
            for(JobsPowerCable item : jobs) {
                JobPowerCableDto jobDto = mapper.JobToJobDto(item);
                jobsDto.add(jobDto);
            }
        }
        return jobsDto;
    }

    @Override
    public List<JobPowerCableDto> findJobByNameAndAsset(String name, UUID asset_id) {
        List<JobsPowerCable> jobs = repository.findJobByNameAndAsset(name, asset_id);
        List<JobPowerCableDto> jobsDto = new ArrayList<>();
        if(jobs.size() != 0) {
            for(JobsPowerCable item : jobs) {
                JobPowerCableDto jobDto = mapper.JobToJobDto(item);
                jobsDto.add(jobDto);
            }
        }
        return jobsDto;
    }

    @Override
    public List<JobPowerCableDto> findAll() {
        List<JobsPowerCable> jobs = repository.findAll();
        List<JobPowerCableDto> jobsDto = new ArrayList<>();
        if(jobs.size() != 0) {
            for(JobsPowerCable item : jobs) {
                JobPowerCableDto jobDto = mapper.JobToJobDto(item);
                jobsDto.add(jobDto);
            }
        }
        return jobsDto;
    }

    @Override
    public List<JobPowerCableDto> findByAssetIdAndCollabsContaining(UUID asset_id, String userId) {
        List<JobsPowerCable> jobs = repository.findByAssetIdAndCollabsContaining(asset_id, userId);
        List<JobPowerCableDto> jobsDto = new ArrayList<>();
        if(jobs.size() != 0) {
            for(JobsPowerCable item : jobs) {
                JobPowerCableDto jobDto = mapper.JobToJobDto(item);
                jobsDto.add(jobDto);
            }
        }
        return jobsDto;
    }

    @Override
    public void saveAll(List<JobsPowerCable> jobdataList) {
        repository.saveAll(jobdataList);
    }

    @Override
    public void deleteAll(List<JobsPowerCable> jobdataList) {
        repository.deleteAll(jobdataList);
    }
}
