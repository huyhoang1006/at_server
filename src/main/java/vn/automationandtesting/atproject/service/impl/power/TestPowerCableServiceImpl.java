package vn.automationandtesting.atproject.service.impl.power;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vn.automationandtesting.atproject.controller.dto.mapper.power.TestPowerCableMapper;
import vn.automationandtesting.atproject.controller.dto.power.TestPowerCableDto;
import vn.automationandtesting.atproject.entity.power.TestsPowerCable;
import vn.automationandtesting.atproject.repository.power.TestPowerRepository;
import vn.automationandtesting.atproject.service.power.TestPowerCableService;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
public class TestPowerCableServiceImpl implements TestPowerCableService {

    @Autowired
    private TestPowerRepository repository;

    @Autowired
    private TestPowerCableMapper mapper;


    @Override
    public List<TestPowerCableDto> findAllTestByJobId(UUID job_id) {
        List<TestsPowerCable> tests = repository.findAllTestByJobId(job_id);
        List<TestPowerCableDto> testsDto = new ArrayList<>();
        if(tests.size() != 0) {
            for(TestsPowerCable item : tests) {
                TestPowerCableDto testDto = mapper.testToTestDto(item);
                testsDto.add(testDto);
            }
        }
        return testsDto;
    }

    @Override
    public List<TestPowerCableDto> findTestById(UUID id) {
        List<TestsPowerCable> tests = repository.findTestById(id);
        List<TestPowerCableDto> testsDto = new ArrayList<>();
        if(tests.size() != 0) {
            for(TestsPowerCable item : tests) {
                TestPowerCableDto testDto = mapper.testToTestDto(item);
                testsDto.add(testDto);
            }
        }
        return testsDto;
    }

    @Override
    public List<TestPowerCableDto> findTestByType(UUID type_id) {
        List<TestsPowerCable> tests = repository.findTestByType(type_id);
        List<TestPowerCableDto> testsDto = new ArrayList<>();
        if(tests.size() != 0) {
            for(TestsPowerCable item : tests) {
                TestPowerCableDto testDto = mapper.testToTestDto(item);
                testsDto.add(testDto);
            }
        }
        return testsDto;
    }

    @Override
    public List<TestPowerCableDto> findTestByTypeAndAsset(UUID type_id, UUID asset_id) {
        List<TestsPowerCable> tests = repository.findTestByTypeAndAsset(type_id, asset_id);
        List<TestPowerCableDto> testsDto = new ArrayList<>();
        if(tests.size() != 0) {
            for(TestsPowerCable item : tests) {
                TestPowerCableDto testDto = mapper.testToTestDto(item);
                testsDto.add(testDto);
            }
        }
        return testsDto;
    }

    @Override
    public void saveAll(List<TestsPowerCable> tests) {
        repository.saveAll(tests);
    }

    @Override
    public void deleteAll(List<TestsPowerCable> tests) {
        repository.deleteAll(tests);
    }
}
