package vn.automationandtesting.atproject.service.circuit;

import vn.automationandtesting.atproject.controller.dto.circuit.TestCircuitTypeDto;
import vn.automationandtesting.atproject.entity.circuit.TestCircuitType;

import java.util.List;

public interface TestCircuitTypeService {
    List<TestCircuitTypeDto> findAll();
    List<TestCircuitTypeDto> findTestTypeByCode(String code);
    List<TestCircuitTypeDto> findTestTypeById(String code);
    void saveAll(List<TestCircuitType> testCircuitTypes);

    void deleteAll(List<TestCircuitType> testCircuitTypes);
}
