package vn.automationandtesting.atproject.service.circuit;

import vn.automationandtesting.atproject.controller.dto.circuit.CircuitDto;
import vn.automationandtesting.atproject.entity.circuit.Circuit;

import java.util.List;
import java.util.UUID;

public interface CircuitService {
    List<CircuitDto> findAllByLocationId(UUID location_id);

    List<CircuitDto> findAssetById(UUID id);

    List<CircuitDto> findBySerial(String serial);

    List<CircuitDto> findBySerialAndLocation(String serial, UUID location_id);

    List<CircuitDto> findAll();

    List<CircuitDto> findByLocationIdAndCollabsContaining(UUID location_id, String userId);

    void saveAll(List<Circuit> circuits);

    void deleteAll(List<Circuit> circuits);
}
