package vn.automationandtesting.atproject.service.circuit;

import vn.automationandtesting.atproject.controller.dto.circuit.JobCircuitDto;
import vn.automationandtesting.atproject.entity.circuit.Jobdata;

import java.util.List;
import java.util.UUID;

public interface JobCircuitService {

    List<JobCircuitDto> findAllJobByAssetId(UUID asset_id);

    List<JobCircuitDto> findJobById(UUID id);

    List<JobCircuitDto> findJobByName(String name);

    List<JobCircuitDto> findJobByNameAndAsset(String name, UUID asset_id);

    List<JobCircuitDto> findAll();

    List<JobCircuitDto> findByAssetIdAndCollabsContaining(UUID asset_id, String userId);

    void saveAll(List<Jobdata> jobdataList);

    void deleteAll(List<Jobdata> jobdataList);
}
