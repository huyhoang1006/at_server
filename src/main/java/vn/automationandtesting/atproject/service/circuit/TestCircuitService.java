package vn.automationandtesting.atproject.service.circuit;

import vn.automationandtesting.atproject.controller.dto.circuit.TestCircuitDto;
import vn.automationandtesting.atproject.entity.circuit.TestCircuit;

import java.util.List;
import java.util.UUID;

public interface TestCircuitService {
    List<TestCircuitDto> findAllTestByJobId(UUID job_id);
    List<TestCircuitDto> findTestById(UUID id);
    List<TestCircuitDto> findTestByType(UUID type_id);
    List<TestCircuitDto> findTestByTypeAndAsset(UUID type_id, UUID asset_id);
    void saveAll(List<TestCircuit> testCircuits);

    void deleteAll(List<TestCircuit> testCircuits);
}
