package vn.automationandtesting.atproject.service;

import vn.automationandtesting.atproject.controller.dto.ListIdDto;
import vn.automationandtesting.atproject.controller.dto.ResourceFullDto;
import vn.automationandtesting.atproject.controller.dto.ResponseObject;
import vn.automationandtesting.atproject.controller.dto.cim.JobDto;
import vn.automationandtesting.atproject.entity.cim.Job;

import java.util.List;
import java.util.UUID;

/**
 * @author tund on 5/9/2022
 * @project at-project-server
 */
public interface JobService {

    JobDto createNewJob(JobDto jobDto);

    JobDto updateJob(JobDto jobDto, UUID id);

    void deleteJob(UUID id);

//    List<JobDto> getAllJobsByRole(UUID id);

    List<JobDto> getAllJobs();

    List<JobDto> findJobsByAssetSerialNo(String assetSerialNo);

    List<JobDto> findJobsByAssetId(UUID id);

    List<JobDto> findJobsByName(String jobName);

    boolean checkJobExisted(UUID id);

    ResponseObject lock(Boolean locked, List<UUID> listId);

    ResponseObject upload(ResourceFullDto resourceFullDto);

    ResponseObject download(List<UUID> listId);

    Job getJobWithHealthIndex(UUID jobId, String fmecaTableCalculate);

    void updateJobCollabs(String jobId, ListIdDto listIdDto);
}
