package vn.automationandtesting.atproject.service;

import org.springframework.data.domain.Pageable;
import vn.automationandtesting.atproject.controller.dto.AssetFullDto;
import vn.automationandtesting.atproject.controller.dto.ResourceFullDto;
import vn.automationandtesting.atproject.controller.dto.ResponseObject;
import vn.automationandtesting.atproject.controller.dto.cim.AssetDto;
import vn.automationandtesting.atproject.entity.cim.Asset;

import java.util.List;
import java.util.UUID;

/**
 * @author tridv on 3/9/2022
 * @project at-project-server
 */
public interface AssetService {
    List<AssetDto> getAllAssets();

    AssetDto createNewAsset(AssetDto assetDto);

    Asset insertIfNotExist(AssetFullDto assetFullDto);

    AssetDto updateAsset(AssetDto assetDto, UUID id);

    void deleteAsset(UUID id);

    AssetDto getAssetByIdAndUserIdAndCollabId(UUID id, UUID loggedInUserId);

    List<AssetDto> searchAssets(String locationId, String serialNo, Pageable pageable);

    ResponseObject lock(Boolean locked, List<UUID> listId);

    ResponseObject download(List<UUID> listId);

    ResponseObject upload(ResourceFullDto resourceFullDto);

    List<AssetDto> getAssetByLocationIdAndUserIdAndCollabId(UUID loctionID, UUID loggedInUserId);
}
