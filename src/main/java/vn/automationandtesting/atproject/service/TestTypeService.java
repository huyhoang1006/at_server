package vn.automationandtesting.atproject.service;

import vn.automationandtesting.atproject.controller.dto.cim.TestTypeDto;
import vn.automationandtesting.atproject.entity.cim.TestType;

/**
 * @author tund on 7/9/2022
 * @project at-project-server
 */
public interface TestTypeService {

//    TestTypeDto createNewTestType(TestTypeDto testTypeDto);

//    TestTypeDto updateTestType(TestTypeDto testTypeDto, UUID id);

//    void deleteTestType(UUID id);

    TestTypeDto findTestTypesByCode(String testTypeCode);

    //    List<TestTypeDto> getAllTestTypesByCode(String code);
    TestType insertIfNotExist(TestType testType);
}
