package vn.automationandtesting.atproject.service;

import org.springframework.data.domain.Pageable;
import vn.automationandtesting.atproject.controller.dto.ListIdDto;
import vn.automationandtesting.atproject.controller.dto.ResourceFullDto;
import vn.automationandtesting.atproject.controller.dto.ResponseObject;
import vn.automationandtesting.atproject.controller.dto.cim.LocationDto;
import vn.automationandtesting.atproject.entity.cim.Location;

import java.util.List;
import java.util.UUID;

/**
 * @author tridv on 5/9/2022
 * @project at-project-server
 */
public interface LocationService {
    LocationDto createLocation(LocationDto locationDto);

    LocationDto updateLocation(UUID id, LocationDto locationDto);

    void deleteLocation(UUID id);

    LocationDto getLocationById(UUID id);

    List<LocationDto> getLocationByRefId(UUID id);

    List<LocationDto> searchLocations(UUID id, Pageable pageable);

    ResponseObject lock(Boolean locked, List<UUID> listId);

    ResponseObject download(List<UUID> listId);

    ResponseObject upload(ResourceFullDto resourceFullDto);

    Location insertIfNotExist(LocationDto locationDto);

    void updateJobCollabs(String locationId, ListIdDto listIdDto);
}
