package vn.automationandtesting.atproject.service;

import vn.automationandtesting.atproject.controller.dto.cim.TapChangerDto;

import java.util.List;
import java.util.UUID;

/**
 * @author tund on 8/9/2022
 * @project at-project-server
 */
public interface TapChangerService {

    TapChangerDto createNewTapChanger(TapChangerDto tapChangerDto);

    TapChangerDto updateTapChanger(TapChangerDto tapChangerDto, UUID id);

    void deleteTapChanger(UUID id);

    List<TapChangerDto> findTapChangersByLocationId(UUID locationId);

    List<TapChangerDto> findTapChangersByAssetId(UUID assetId);

    boolean checkTapChangerExisted(UUID id);
}
