package vn.automationandtesting.atproject.service;

import vn.automationandtesting.atproject.controller.dto.ResponseObject;
import vn.automationandtesting.atproject.controller.dto.cim.TestDto;
import vn.automationandtesting.atproject.entity.cim.Test;

import java.util.List;
import java.util.UUID;

/**
 * @author tund on 7/9/2022
 * @project at-project-server
 */
public interface TestService {

    TestDto createNewTest(TestDto testDto);

    TestDto updateTest(TestDto testDto, UUID id);

    void deleteTest(UUID id);

    List<TestDto> getAllTests();

    List<TestDto> findAllTestsByJobName(String jobName);

    List<TestDto> findAllByJobId(String jobName);

    List<TestDto> findAllTestsByCodeType(String codeType);

    List<TestDto> findAllTestsByAssetSerialNo(String serialNo);

    void addMetaData(Test test);

    ResponseObject calculate(List<TestDto> listTestDto);
}
