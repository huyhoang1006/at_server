package vn.automationandtesting.atproject.service;

import org.springframework.boot.configurationprocessor.json.JSONException;
import org.springframework.boot.configurationprocessor.json.JSONObject;
import vn.automationandtesting.atproject.controller.dto.MonitoringDto;

import java.util.List;

/**
 * @author tridv on 22/9/2022
 * @project at-project-server
 */
public interface MonitoringService {
    List<MonitoringDto> findMonitoringByAssetId(String assetId);

    List<MonitoringDto> findMonitoringdescByAssetId(String assetId);

    List<MonitoringDto> findLastMonitoringdescByAssetId(String assetId);

    List<MonitoringDto> getAllMonitoring();

    MonitoringDto findMonitoringById(String id);

    MonitoringDto createMonitoring(MonitoringDto monitoringDto);

    MonitoringDto createMonitoring(JSONObject monitorObject) throws JSONException;


    MonitoringDto updateMonitoring(String id, MonitoringDto monitoringDto);

    void deleteMonitoringById(String id);

}
