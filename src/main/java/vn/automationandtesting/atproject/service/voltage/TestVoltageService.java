package vn.automationandtesting.atproject.service.voltage;

import vn.automationandtesting.atproject.controller.dto.voltage.TestVoltageDto;
import vn.automationandtesting.atproject.entity.voltage.TestsVoltage;

import java.util.List;
import java.util.UUID;

public interface TestVoltageService {
    List<TestVoltageDto> findAllTestByJobId(UUID job_id);
    List<TestVoltageDto> findTestById(UUID id);
    List<TestVoltageDto> findTestByType(UUID type_id);
    List<TestVoltageDto> findTestByTypeAndAsset(UUID type_id, UUID asset_id);
    void saveAll(List<TestsVoltage> tests);
    void deleteAll(List<TestsVoltage> tests);
}
