package vn.automationandtesting.atproject.service.voltage;

import vn.automationandtesting.atproject.controller.dto.surge.SurgeDto;
import vn.automationandtesting.atproject.controller.dto.voltage.VoltageDto;
import vn.automationandtesting.atproject.entity.voltage.Voltage;

import java.util.List;
import java.util.UUID;

public interface VoltageService {
    List<VoltageDto> findAllByLocationId(UUID location_id);

    List<VoltageDto> findAssetById(UUID id);

    List<VoltageDto> findBySerial(String serial);

    List<VoltageDto> findBySerialAndLocation(String serial, UUID location_id);

    List<VoltageDto> findAll();

    List<VoltageDto> findByLocationIdAndCollabsContaining(UUID location_id, String userId);

    void saveAll(List<Voltage> assets);

    void deleteAll(List<Voltage> assets);
}
