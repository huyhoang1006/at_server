package vn.automationandtesting.atproject.service;

import org.springframework.stereotype.Service;
import vn.automationandtesting.atproject.controller.dto.GroupDto;

import java.util.List;
import java.util.UUID;

@Service
public interface GroupService {

    List<GroupDto> getAllGroups();

    GroupDto createNewGroup(GroupDto groupDto);

    GroupDto updateGroup(GroupDto groupDto, UUID groupID);

    GroupDto updateGroupLite(GroupDto groupDto, UUID groupID);

    GroupDto getGroupById(String groupID);

    void deleteGroup(String groupID);
}
