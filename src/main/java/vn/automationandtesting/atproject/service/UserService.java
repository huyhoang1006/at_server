package vn.automationandtesting.atproject.service;

import vn.automationandtesting.atproject.controller.dto.ResponseObject;
import vn.automationandtesting.atproject.controller.dto.request.UserReqDto;
import vn.automationandtesting.atproject.entity.User;

import java.util.List;
import java.util.UUID;

public interface UserService {
    User getUserByUsername(String username);
    
    ResponseObject changePassword(UUID userID, String oldPassword, String newPassword);

    ResponseObject saveNewUser(UserReqDto userReqDto);

    List<UserReqDto> getAllUsers();

    UserReqDto getUserById(UUID id);

    UserReqDto updateUser(UserReqDto userReqDto, UUID id);

    UserReqDto updateUserFull(UserReqDto userReqDto, UUID id);

    UserReqDto changeGroupUser(List<String> groupName, UUID id);

    void deleteUserById(UUID id);

    UserReqDto updateUserGroupsForAdmin(List<String> groupNames, UUID id);
}
