package vn.automationandtesting.atproject.service.disconnector;

import vn.automationandtesting.atproject.controller.dto.circuit.CircuitDto;
import vn.automationandtesting.atproject.controller.dto.disconnector.DisconnectorDto;
import vn.automationandtesting.atproject.entity.disconnector.Disconnector;

import java.util.List;
import java.util.UUID;

public interface DisconnectorService {
    List<DisconnectorDto> findAllByLocationId(UUID location_id);

    List<DisconnectorDto> findAssetById(UUID id);

    List<DisconnectorDto> findBySerial(String serial);

    List<DisconnectorDto> findBySerialAndLocation(String serial, UUID location_id);

    List<DisconnectorDto> findAll();

    List<DisconnectorDto> findByLocationIdAndCollabsContaining(UUID location_id, String userId);

    void saveAll(List<Disconnector> assets);

    void deleteAll(List<Disconnector> assets);
}
