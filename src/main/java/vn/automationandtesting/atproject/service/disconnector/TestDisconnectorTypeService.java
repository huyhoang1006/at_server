package vn.automationandtesting.atproject.service.disconnector;

import vn.automationandtesting.atproject.controller.dto.disconnector.TestDisconnectorTypeDto;
import vn.automationandtesting.atproject.entity.disconnector.TestDisconnectorType;

import java.util.List;

public interface TestDisconnectorTypeService {
    List<TestDisconnectorTypeDto> findAll();
    List<TestDisconnectorTypeDto> findTestTypeByCode(String code);
    List<TestDisconnectorTypeDto> findTestTypeById(String code);
    void saveAll(List<TestDisconnectorType> testTypes);

    void deleteAll(List<TestDisconnectorType> testTypes);
}
