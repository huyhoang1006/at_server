package vn.automationandtesting.atproject.service.disconnector;

import vn.automationandtesting.atproject.controller.dto.disconnector.TestDisconnectorDto;
import vn.automationandtesting.atproject.entity.disconnector.TestsDisconnector;

import java.util.List;
import java.util.UUID;

public interface TestDisconnectorService {
    List<TestDisconnectorDto> findAllTestByJobId(UUID job_id);
    List<TestDisconnectorDto> findTestById(UUID id);
    List<TestDisconnectorDto> findTestByType(UUID type_id);
    List<TestDisconnectorDto> findTestByTypeAndAsset(UUID type_id, UUID asset_id);
    void saveAll(List<TestsDisconnector> tests);

    void deleteAll(List<TestsDisconnector> tests);
}
