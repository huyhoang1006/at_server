package vn.automationandtesting.atproject.service.surge;

import vn.automationandtesting.atproject.controller.dto.power.PowerCableDto;
import vn.automationandtesting.atproject.controller.dto.surge.SurgeDto;
import vn.automationandtesting.atproject.entity.surge.Surge;

import java.util.List;
import java.util.UUID;

public interface SurgeService {
    List<SurgeDto> findAllByLocationId(UUID location_id);

    List<SurgeDto> findAssetById(UUID id);

    List<SurgeDto> findBySerial(String serial);

    List<SurgeDto> findBySerialAndLocation(String serial, UUID location_id);

    List<SurgeDto> findAll();

    List<SurgeDto> findByLocationIdAndCollabsContaining(UUID location_id, String userId);

    void saveAll(List<Surge> assets);

    void deleteAll(List<Surge> assets);
}
