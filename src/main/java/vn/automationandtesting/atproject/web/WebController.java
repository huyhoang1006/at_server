package vn.automationandtesting.atproject.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import vn.automationandtesting.atproject.controller.dto.GroupDto;
import vn.automationandtesting.atproject.controller.dto.request.UserReqDto;
import vn.automationandtesting.atproject.entity.Permission;
import vn.automationandtesting.atproject.repository.GroupPermissionRepository;
import vn.automationandtesting.atproject.repository.PermissionRepository;
import vn.automationandtesting.atproject.service.impl.GroupServiceImpl;
import vn.automationandtesting.atproject.service.impl.UserServiceImpl;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Controller
public class WebController {
    @Autowired
    private UserServiceImpl userService;

    @Autowired
    private GroupServiceImpl groupService;

    @Autowired
    private PermissionRepository permissionRepository;

    @Autowired
    private GroupPermissionRepository groupPermissionRepository;

    @GetMapping("/login")
    public String login() {
        return "login";
    }

    @GetMapping("/")
    public String defaultLogin() {
        return "login";
    }

    @GetMapping("/admin/dashboard")
    public String dashboard() {
        return "dashboard";
    }

    @GetMapping("/admin/groupManager")
    public String groupManager(Model model) {
        model.addAttribute("groups", groupService.getAllGroups());
        List<Permission> listPer = permissionRepository.findAllByIsDeleted(false);
        List<Permission> listPerJob = new ArrayList<Permission>();
        List<Permission> listPerAsset = new ArrayList<Permission>();
        List<Permission> listPerLocation = new ArrayList<Permission>();
        //Create list permission of job
        for (Permission p : listPer) {
            if (p.getPermissionName().toString().contains("VIEW_JOB")) {
                listPerJob.add(p);
                break;
            }
        }
        for (Permission p : listPer) {
            if (p.getPermissionName().toString().contains("CREATE_JOB")) {
                listPerJob.add(p);
                break;
            }
        }
        for (Permission p : listPer) {
            if (p.getPermissionName().toString().contains("UPDATE_JOB")) {
                listPerJob.add(p);
                break;
            }
        }
        for (Permission p : listPer) {
            if (p.getPermissionName().toString().contains("DELETE_JOB")) {
                listPerJob.add(p);
                break;
            }
        }

        //Create list permission of asset
        for (Permission p : listPer) {
            if (p.getPermissionName().toString().contains("VIEW_ASSET")) {
                listPerAsset.add(p);
                break;
            }
        }
        for (Permission p : listPer) {
            if (p.getPermissionName().toString().contains("CREATE_ASSET")) {
                listPerAsset.add(p);
                break;
            }
        }
        for (Permission p : listPer) {
            if (p.getPermissionName().toString().contains("UPDATE_ASSET")) {
                listPerAsset.add(p);
                break;
            }
        }
        for (Permission p : listPer) {
            if (p.getPermissionName().toString().contains("DELETE_ASSET")) {
                listPerAsset.add(p);
                break;
            }
        }

        //Create list permission of location
        for (Permission p : listPer) {
            if (p.getPermissionName().toString().contains("VIEW_LOCATION")) {
                listPerLocation.add(p);
                break;
            }
        }
        for (Permission p : listPer) {
            if (p.getPermissionName().toString().contains("CREATE_LOCATION")) {
                listPerLocation.add(p);
                break;
            }
        }
        for (Permission p : listPer) {
            if (p.getPermissionName().toString().contains("UPDATE_LOCATION")) {
                listPerLocation.add(p);
                break;
            }
        }
        for (Permission p : listPer) {
            if (p.getPermissionName().toString().contains("DELETE_LOCATION")) {
                listPerLocation.add(p);
                break;
            }
        }
        model.addAttribute("permissionsJob", listPerJob);
        model.addAttribute("permissionsAsset", listPerAsset);
        model.addAttribute("permissionsLocation", listPerLocation);
        return "group_manager";
    }

    @GetMapping("/admin/userManager")
    public String userManager(Model model) {
        model.addAttribute("users", userService.getAllUsers());
        model.addAttribute("groups", groupService.getAllGroups());
        return "user_manager";
    }

    @GetMapping("/admin/userManager/error")
    public String userManagerError(Model model) {
        model.addAttribute("users", userService.getAllUsers());
        model.addAttribute("groups", groupService.getAllGroups());
        model.addAttribute("error",1);
        return "user_manager";
    }

    @GetMapping("/403")
    public String accessDenied() {
        return "403";
    }

    // User Controller
    @GetMapping("/admin/users/{userId}")
    @ResponseBody
    public UserReqDto getUserById(@PathVariable String userId) {
        UUID id = UUID.fromString(userId);
        UserReqDto userReqDto = userService.getUserById(id);
        return userReqDto;
    }

    @PostMapping("/admin/users")
    public String addUser(UserReqDto userReqDto, Model model) {
        LocalDate date = LocalDate.now();
        userReqDto.setBirthDate(date);
        try {
            userService.saveNewUser(userReqDto);
            return "redirect:/admin/userManager";
        }catch (Exception e){
            return "redirect:/admin/userManager/error";
        }
    }

    @RequestMapping(value = "admin/deleteUser/{userId}", method = {RequestMethod.DELETE, RequestMethod.GET})
    public String deleteUser(@PathVariable String userId) {
        UUID id = UUID.fromString(userId);
        userService.deleteUserById(id);
        return "redirect:/admin/userManager";
    }

    @PostMapping("/admin/updateUser")
    public String updateUser(UserReqDto userReqDto, @RequestParam String id) {
        UUID userId = UUID.fromString(id);
        LocalDate date = LocalDate.now();
        userReqDto.setBirthDate(date);
        userService.updateUserFull(userReqDto, userId);
        return "redirect:/admin/userManager";
    }

    @PostMapping("/admin/changeGroupUser/{userId}")
    public String changerGroupUser(@PathVariable String userId, @RequestParam List<String> groupName) {
        UUID id = UUID.fromString(userId);
        userService.changeGroupUser(groupName, id);
        return "user_manager";
    }

    //Group Controller
    @PostMapping("/admin/groups")
    public String addGroup(GroupDto groupDto) {
        groupService.createNewGroup(groupDto);
        return "redirect:/admin/groupManager";
    }

    @GetMapping("/admin/groups/{groupId}")
    @ResponseBody
    public GroupDto getGroupById(@PathVariable String groupId) {
        GroupDto groupDto = groupService.getGroupById(groupId);
        return groupDto;
    }

    @RequestMapping(value = "admin/deleteGroup/{groupId}", method = {RequestMethod.DELETE, RequestMethod.GET})
    public String deleteGroup(@PathVariable String groupId) {
        groupService.deleteGroup(groupId);
        return "redirect:/admin/groupManager";
    }

    @PostMapping("/admin/updateGroup")
    public String updateGroup(GroupDto groupDto, @RequestParam String id) {
        UUID groupId = UUID.fromString(id);
        groupService.updateGroupLite(groupDto, groupId);
        return "redirect:/admin/groupManager";
    }

    @GetMapping("/admin/permissionGroup/{groupId}")
    @ResponseBody
    public List<String> getIdPermissionOfGroup(@PathVariable String groupId){
        UUID id = UUID.fromString(groupId);
        return groupPermissionRepository.findPermissionByGroupId(id);
    }

    @PostMapping("/admin/changePermissionGroup/{groupId}")
    public String changerPermissionGroup(@PathVariable String groupId, @RequestParam List<String> listPermissionId) {
        System.out.println(listPermissionId);
        UUID id = UUID.fromString(groupId);
        groupPermissionRepository.deltePermissionByGroupId(id);
        for (String permissionId : listPermissionId){
            UUID permission = UUID.fromString(permissionId);
            groupPermissionRepository.insertPermission(permission, id);
        }
        return "group_manager";
    }
}
