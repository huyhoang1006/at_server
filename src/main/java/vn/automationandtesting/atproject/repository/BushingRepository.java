package vn.automationandtesting.atproject.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import vn.automationandtesting.atproject.entity.cim.Asset;
import vn.automationandtesting.atproject.entity.cim.Bushing;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface BushingRepository extends JpaRepository<Bushing, UUID> {
    Optional<Bushing> findByIdAndIsDeleted(UUID bushingId, boolean isDeleted);
    List<Bushing> findAllByAssetAndIsDeleted(Asset asset, boolean isDeleted);
    @Query(
            value = "select * from bushing b "+
            "left join asset a on a.mrid = b.asset_id "+
            "left join location l on l.mrid = a.location_id "+
            "where l.mrid = ?1 and b.is_deleted = ?2", nativeQuery = true)
    List<Bushing> findByLocationIdAndIsDeleted(UUID locationId, boolean isDeleted);

    @Query(value = "select * from bushing b " +
            "where b.asset_id = ?1 and b.is_deleted = false", nativeQuery = true)
    Bushing findFirstByAssetId(UUID assetId);
}
