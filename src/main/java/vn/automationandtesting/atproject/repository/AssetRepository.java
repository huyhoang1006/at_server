package vn.automationandtesting.atproject.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import vn.automationandtesting.atproject.entity.cim.Asset;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface AssetRepository extends JpaRepository<Asset, UUID>, JpaSpecificationExecutor<Asset> {
    List<Asset> findAllByIsDeleted(boolean isDeleted);

    Optional<Asset> findByMridAndIsDeleted(UUID assetId, boolean isDeleted);

    Optional<Asset> findByMridAndIsDeletedAndCreatedBy(UUID assetId, boolean isDeleted, UUID createdById);

    Optional<Asset> findByMridAndIsDeletedAndCollabsContains(UUID assetId, boolean isDeleted, String collabId);

    List<Asset> findAllByIsDeletedAndMridInAndCreatedByOrIsDeletedAndMridInAndCollabsContaining(
            boolean isDeleted1,
            List<UUID> assetId1,
            UUID createdById,
            boolean isDeleted2,
            List<UUID> assetId2,
            UUID collabId
    );

    @Query(
            value = "select  * from asset at " +
                    "where at.location_id = ?1 and at.is_deleted = ?2 and at.created_by = ?3 order by at.created_on DESC", nativeQuery = true
    )
    List<Asset> findAllByLocation_idAndIsDeletedAndCreatedBy(UUID location_id, boolean isDeleted, UUID createdById);

    @Query(
            value = "select  * from asset at " +
                    "where at.location_id = ?1 and at.is_deleted = ?2 and at.collabs Like %?3% order by at.created_on DESC", nativeQuery = true
    )
    List<Asset> findAllByLocation_idAndIsDeletedAndCollabs(UUID location_id, boolean isDeleted, String createdById);
}
