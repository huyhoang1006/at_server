package vn.automationandtesting.atproject.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import vn.automationandtesting.atproject.entity.cim.TapChanger;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface TapChangerRepository extends JpaRepository<TapChanger, UUID> {
    Optional<TapChanger> findByMridAndIsDeleted(UUID tapChangerId, boolean isDeleted);

    @Query(
            value = "select * from tap_changer_info tc "+
                    "left join asset a on tc.asset_id = a.mrid "+
                    "where tc.asset_id = ?1 and tc.is_deleted = ?2 and a.is_deleted = ?2", nativeQuery = true
    )
    List<TapChanger> findByAssetId(UUID assetId, boolean isDeleted);

    @Query(value = "select * from tap_changer_info tc " +
                    "where tc.asset_id = ?1 and tc.is_deleted = false", nativeQuery = true)
    TapChanger findFirstByAssetId(UUID assetId);

    @Query(
            value = "select * from tap_changer_info tc "+
                    "left join asset a on tc.asset_id = a.mrid "+
                    "left join location l on a.location_id = l.mrid "+
                    "where l.mrid = ?1 and tc.is_deleted = ?2 and a.is_deleted = ?2 and l.is_deleted = ?2", nativeQuery = true
    )
    List<TapChanger> findByLocationId(UUID locationId, boolean isDeleted);
}
