package vn.automationandtesting.atproject.repository.current;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import vn.automationandtesting.atproject.entity.circuit.Circuit;
import vn.automationandtesting.atproject.entity.current.Current;

import java.util.List;
import java.util.UUID;

@Repository
public interface CurrentRepository extends JpaRepository<Current, UUID> {
    @Query(
            value = "select * from current_voltage at " +
                    "where at.location_id = ?1 and (at.created_by = ?2 or at.collabs like %?3%)", nativeQuery = true
    )
    List<Current> findAllByLocationId(UUID location_id, UUID user_id, String userId);

    @Query(
            value = "select * from current_voltage at " +
                    "where at.id = ?1", nativeQuery = true
    )
    List<Current> findAssetById(UUID id);

    @Query(
            value = "select * from current_voltage at " +
                    "where at.serial_no = ?1", nativeQuery = true
    )
    List<Current> findBySerial(String serial);

    @Query(
            value = "select * from current_voltage at " +
                    "where at.serial_no = ?1 and at.location_id = ?2", nativeQuery = true
    )
    List<Current> findBySerialAndLocation(String serial, UUID location_id);

    @Query(
            value = "select * from current_voltage", nativeQuery = true
    )
    List<Current> findAll();

    @Query(
            value = "select * from current_voltage at where at.location_id = ?1 and at.collabs like %?2%", nativeQuery = true
    )
    List<Current> findByLocationIdAndCollabsContaining(UUID location_id, String userId);
}
