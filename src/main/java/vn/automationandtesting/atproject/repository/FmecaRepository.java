package vn.automationandtesting.atproject.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import vn.automationandtesting.atproject.entity.Fmeca;

import java.util.Optional;
import java.util.UUID;

/**
 * @author tridv on 19/9/2022
 * @project at-project-server
 */
@Repository
public interface FmecaRepository extends JpaRepository<Fmeca, UUID> {
    Optional<Fmeca> findByIdAndIsDeleted(UUID id, boolean isDeleted);
}
