package vn.automationandtesting.atproject.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import vn.automationandtesting.atproject.entity.cim.Location;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

/**
 * @author tridv on 3/9/2022
 * @project at-project-server
 */
@Repository
public interface LocationRepository extends JpaRepository<Location, UUID>, JpaSpecificationExecutor<Location> {
    Optional<Location> findByMridAndIsDeleted(UUID locationId, boolean isDeleted);

    Optional<Location> findByMridAndIsDeletedAndCreatedBy(UUID locationId, boolean isDeleted, UUID createdById);

    Optional<Location> findByMridAndIsDeletedAndCollabsContains(UUID locationId, boolean isDeleted, String collabId);
    List<Location> findAllByIsDeletedAndMridInAndCreatedByOrIsDeletedAndMridInAndCollabsContaining(
            boolean isDeleted1,
            List<UUID> locationId1,
            UUID createdById,
            boolean isDeleted2,
            List<UUID> locationId2,
            UUID collabId
    );

    @Query(
            value = "select * from location l where l.ref_id = ?1", nativeQuery = true
    )
    List<Location> findAllByRefId(String id);
}
