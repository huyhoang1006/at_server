package vn.automationandtesting.atproject.repository.surge;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import vn.automationandtesting.atproject.entity.circuit.Circuit;
import vn.automationandtesting.atproject.entity.surge.Surge;

import java.util.List;
import java.util.UUID;

@Repository
public interface SurgeRepository extends JpaRepository<Surge, UUID> {
    @Query(
            value = "select * from surge_arrester at " +
                    "where at.location_id = ?1 and (at.created_by = ?2 or at.collabs like %?3%)", nativeQuery = true
    )
    List<Surge> findAllByLocationId(UUID location_id, UUID user_id, String userId);

    @Query(
            value = "select * from surge_arrester at " +
                    "where at.id = ?1", nativeQuery = true
    )
    List<Surge> findAssetById(UUID id);

    @Query(
            value = "select * from surge_arrester at " +
                    "where at.serial_no = ?1", nativeQuery = true
    )
    List<Surge> findBySerial(String serial);

    @Query(
            value = "select * from surge_arrester at " +
                    "where at.serial_no = ?1 and at.location_id = ?2", nativeQuery = true
    )
    List<Surge> findBySerialAndLocation(String serial, UUID location_id);

    @Query(
            value = "select * from surge_arrester", nativeQuery = true
    )
    List<Surge> findAll();

    @Query(
            value = "select * from surge_arrester at where at.location_id = ?1 and at.collabs like %?2%", nativeQuery = true
    )
    List<Surge> findByLocationIdAndCollabsContaining(UUID location_id, String userId);
}
