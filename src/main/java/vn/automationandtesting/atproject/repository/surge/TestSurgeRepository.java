package vn.automationandtesting.atproject.repository.surge;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import vn.automationandtesting.atproject.entity.surge.TestsSurge;

import java.util.List;
import java.util.UUID;

@Repository
public interface TestSurgeRepository extends JpaRepository<TestsSurge, UUID> {
    @Query(
            value = "select * from testssurge at " +
                    "where at.job_id = ?1", nativeQuery = true
    )
    List<TestsSurge> findAllTestByJobId(UUID job_id);

    @Query(
            value = "select * from testssurge at " +
                    "where at.id = ?1", nativeQuery = true
    )
    List<TestsSurge> findTestById(UUID id);

    @Query(
            value = "select * from testssurge at " +
                    "where at.type_id = ?1", nativeQuery = true
    )
    List<TestsSurge> findTestByType(UUID type_id);

    @Query(
            value = "select * from testssurge at " +
                    "where at.type_id = ?1 and at.asset_id = ?2", nativeQuery = true
    )
    List<TestsSurge> findTestByTypeAndAsset(UUID type_id, UUID asset_id);
}
