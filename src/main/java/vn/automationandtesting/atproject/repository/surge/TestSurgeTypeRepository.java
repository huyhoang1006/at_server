package vn.automationandtesting.atproject.repository.surge;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import vn.automationandtesting.atproject.entity.surge.TestSurgeType;

import java.util.List;
import java.util.UUID;

@Repository
public interface TestSurgeTypeRepository extends JpaRepository<TestSurgeType, UUID> {
    @Query(
            value = "select * from testsurge_type", nativeQuery = true
    )
    List<TestSurgeType> findAll();

    @Query(
            value = "select * from testsurge_type at " +
                    "where at.code = ?1", nativeQuery = true
    )
    List<TestSurgeType> findTestTypeByCode(String code);

    @Query(
            value = "select * from testsurge_type at " +
                    "where at.id = ?1", nativeQuery = true
    )
    List<TestSurgeType> findTestTypeById(String code);
}
