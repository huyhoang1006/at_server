package vn.automationandtesting.atproject.repository.disconnector;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import vn.automationandtesting.atproject.entity.circuit.Circuit;
import vn.automationandtesting.atproject.entity.disconnector.JobsDisconnector;

import java.util.List;
import java.util.UUID;

@Repository
public interface JobDisconnectorRepository extends JpaRepository<JobsDisconnector, UUID> {
    @Query(
            value = "select * from jobsdisconnect at " +
                    "where at.asset_id = ?1 and (at.created_by = ?2 or at.collabs like %?3%)" , nativeQuery = true
    )
    List<JobsDisconnector> findAllJobByAssetId(UUID asset_id, UUID user_id, String userId);

    @Query(
            value = "select * from jobsdisconnect at " +
                    "where at.id = ?1", nativeQuery = true
    )
    List<JobsDisconnector> findJobById(UUID id);

    @Query(
            value = "select * from jobsdisconnect at " +
                    "where at.name = ?1", nativeQuery = true
    )
    List<JobsDisconnector> findJobByName(String name);

    @Query(
            value = "select * from jobsdisconnect at " +
                    "where at.name = ?1 and at.asset_id = ?2", nativeQuery = true
    )
    List<JobsDisconnector> findJobByNameAndAsset(String name, UUID asset_id);

    @Query(
            value = "select * from jobsdisconnect", nativeQuery = true
    )
    List<JobsDisconnector> findAll();

    @Query(
            value = "select * from jobsdisconnect at where at.asset_id = ?1 and at.collabs like %?2%", nativeQuery = true
    )
    List<JobsDisconnector> findByAssetIdAndCollabsContaining(UUID asset_id, String userId);
}
