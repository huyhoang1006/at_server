package vn.automationandtesting.atproject.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import vn.automationandtesting.atproject.entity.Group;

import java.util.List;
import java.util.UUID;

@Repository
public interface GroupRepository extends JpaRepository<Group, UUID> {
    List<Group> findByIsDeleted(boolean isDeleted);
    boolean existsByGroupNameIgnoreCase(String groupName);
    Group findByIdAndIsDeleted(UUID id, boolean isDeleted);
}
