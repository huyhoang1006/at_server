package vn.automationandtesting.atproject.repository.voltage;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import vn.automationandtesting.atproject.entity.voltage.TestVoltageType;

import java.util.List;
import java.util.UUID;

@Repository
public interface TestVoltageTypeRepository extends JpaRepository<TestVoltageType, UUID> {
    @Query(
            value = "select * from testvoltage_type", nativeQuery = true
    )
    List<TestVoltageType> findAll();

    @Query(
            value = "select * from testvoltage_type at " +
                    "where at.code = ?1", nativeQuery = true
    )
    List<TestVoltageType> findTestTypeByCode(String code);

    @Query(
            value = "select * from testvoltage_type at " +
                    "where at.id = ?1", nativeQuery = true
    )
    List<TestVoltageType> findTestTypeById(String code);
}
