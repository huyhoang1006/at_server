package vn.automationandtesting.atproject.repository.voltage;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import vn.automationandtesting.atproject.entity.circuit.Circuit;
import vn.automationandtesting.atproject.entity.voltage.Voltage;

import java.util.List;
import java.util.UUID;

@Repository
public interface VoltageRepository extends JpaRepository<Voltage, UUID> {
    @Query(
            value = "select * from voltage_trans at " +
                    "where at.location_id = ?1 and (at.created_by = ?2 or at.collabs like %?3%)", nativeQuery = true
    )
    List<Voltage> findAllByLocationId(UUID location_id, UUID user_id, String userId);

    @Query(
            value = "select * from voltage_trans at " +
                    "where at.id = ?1", nativeQuery = true
    )
    List<Voltage> findAssetById(UUID id);

    @Query(
            value = "select * from voltage_trans at " +
                    "where at.serial_no = ?1", nativeQuery = true
    )
    List<Voltage> findBySerial(String serial);

    @Query(
            value = "select * from voltage_trans at " +
                    "where at.serial_no = ?1 and at.location_id = ?2", nativeQuery = true
    )
    List<Voltage> findBySerialAndLocation(String serial, UUID location_id);

    @Query(
            value = "select * from voltage_trans", nativeQuery = true
    )
    List<Voltage> findAll();

    @Query(
            value = "select * from voltage_trans at where at.location_id = ?1 and at.collabs like %?2%", nativeQuery = true
    )
    List<Voltage> findByLocationIdAndCollabsContaining(UUID location_id, String userId);
}
