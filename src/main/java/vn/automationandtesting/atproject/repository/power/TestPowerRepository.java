package vn.automationandtesting.atproject.repository.power;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import vn.automationandtesting.atproject.entity.power.TestsPowerCable;

import java.util.List;
import java.util.UUID;

@Repository
public interface TestPowerRepository extends JpaRepository<TestsPowerCable, UUID> {
    @Query(
            value = "select * from testspower at " +
                    "where at.job_id = ?1", nativeQuery = true
    )
    List<TestsPowerCable> findAllTestByJobId(UUID job_id);

    @Query(
            value = "select * from testspower at " +
                    "where at.id = ?1", nativeQuery = true
    )
    List<TestsPowerCable> findTestById(UUID id);

    @Query(
            value = "select * from testspower at " +
                    "where at.type_id = ?1", nativeQuery = true
    )
    List<TestsPowerCable> findTestByType(UUID type_id);

    @Query(
            value = "select * from testspower at " +
                    "where at.type_id = ?1 and at.asset_id = ?2", nativeQuery = true
    )
    List<TestsPowerCable> findTestByTypeAndAsset(UUID type_id, UUID asset_id);
}
