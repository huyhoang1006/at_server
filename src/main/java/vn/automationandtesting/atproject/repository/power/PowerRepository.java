package vn.automationandtesting.atproject.repository.power;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import vn.automationandtesting.atproject.entity.circuit.Circuit;
import vn.automationandtesting.atproject.entity.power.PowerCable;

import java.util.List;
import java.util.UUID;

@Repository
public interface PowerRepository extends JpaRepository<PowerCable, UUID> {
    @Query(
            value = "select * from power_cable at " +
                    "where at.location_id = ?1 and (at.created_by = ?2 or at.collabs like %?3%)", nativeQuery = true
    )
    List<PowerCable> findAllByLocationId(UUID location_id, UUID user_id, String userId);

    @Query(
            value = "select * from power_cable at " +
                    "where at.id = ?1", nativeQuery = true
    )
    List<PowerCable> findAssetById(UUID id);

    @Query(
            value = "select * from power_cable at " +
                    "where at.serial_no = ?1", nativeQuery = true
    )
    List<PowerCable> findBySerial(String serial);

    @Query(
            value = "select * from power_cable at " +
                    "where at.serial_no = ?1 and at.location_id = ?2", nativeQuery = true
    )
    List<PowerCable> findBySerialAndLocation(String serial, UUID location_id);

    @Query(
            value = "select * from power_cable", nativeQuery = true
    )
    List<PowerCable> findAll();

    @Query(
            value = "select * from power_cable at where at.location_id = ?1 and at.collabs like %?2%", nativeQuery = true
    )
    List<PowerCable> findByLocationIdAndCollabsContaining(UUID location_id, String userId);
}
