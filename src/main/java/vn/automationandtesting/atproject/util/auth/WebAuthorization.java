package vn.automationandtesting.atproject.util.auth;

import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import vn.automationandtesting.atproject.config.auth.BearerContext;
import vn.automationandtesting.atproject.config.auth.BearerContextHolder;
import vn.automationandtesting.atproject.entity.enumm.PermissionEnum;
import vn.automationandtesting.atproject.entity.enumm.RoleEnum;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author tridv on 15/9/2022
 * @project at-project-server
 */
@Component
public class WebAuthorization {
    public boolean checkLocationPermission(HttpServletRequest request) {
        List<String> permissions = BearerContextHolder.getContext().getPermissions();
        if (request.getMethod().equals(HttpMethod.GET.name()) && permissions.contains(PermissionEnum.VIEW_LOCATION.name())
                || request.getMethod().equals(HttpMethod.POST.name()) && permissions.contains(PermissionEnum.CREATE_LOCATION.name())
                || request.getMethod().equals(HttpMethod.PUT.name()) && permissions.contains(PermissionEnum.UPDATE_LOCATION.name())
                || request.getMethod().equals(HttpMethod.DELETE.name()) && permissions.contains(PermissionEnum.DELETE_LOCATION.name())
        ) {
            return true;
        }
        return false;
    }

    public boolean checkAssetPermission(HttpServletRequest request) {
        List<String> permissions = BearerContextHolder.getContext().getPermissions();
        if (request.getMethod().equals(HttpMethod.GET.name()) && permissions.contains(PermissionEnum.VIEW_ASSET.name())
                || request.getMethod().equals(HttpMethod.POST.name()) && permissions.contains(PermissionEnum.CREATE_ASSET.name())
                || request.getMethod().equals(HttpMethod.PUT.name()) && permissions.contains(PermissionEnum.UPDATE_ASSET.name())
                || request.getMethod().equals(HttpMethod.DELETE.name()) && permissions.contains(PermissionEnum.DELETE_ASSET.name())
        ) {
            return true;
        }
        return false;
    }

    public boolean checkJobPermission(HttpServletRequest request) {
        List<String> permissions = BearerContextHolder.getContext().getPermissions();
        if (request.getMethod().equals(HttpMethod.GET.name()) && permissions.contains(PermissionEnum.VIEW_JOB.name())
                || request.getMethod().equals(HttpMethod.POST.name()) && permissions.contains(PermissionEnum.CREATE_JOB.name())
                || request.getMethod().equals(HttpMethod.PUT.name()) && permissions.contains(PermissionEnum.UPDATE_JOB.name())
                || request.getMethod().equals(HttpMethod.DELETE.name()) && permissions.contains(PermissionEnum.DELETE_JOB.name())
        ) {
            return true;
        }
        return false;
    }

    public boolean checkPermissionAPIWithUserID(HttpServletRequest request, String userId) {
        BearerContext bearerContext = BearerContextHolder.getContext();
        if (bearerContext.getRoleName().equalsIgnoreCase(RoleEnum.USER.name())) {
            if (request.getMethod().equals(HttpMethod.DELETE)) {
                return false;
            }
            if (request.getMethod().equals(HttpMethod.GET) || request.getMethod().equals(HttpMethod.PUT)) {
                return bearerContext.getUserId().equals(userId);
            }
        }
        return true;
    }
}
