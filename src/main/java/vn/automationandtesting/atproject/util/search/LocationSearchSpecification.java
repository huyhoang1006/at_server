package vn.automationandtesting.atproject.util.search;

import lombok.AllArgsConstructor;
import org.springframework.data.jpa.domain.Specification;
import vn.automationandtesting.atproject.entity.User;
import vn.automationandtesting.atproject.entity.cim.Location;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * @author tridv on 5/9/2022
 * @project at-project-server
 */
@AllArgsConstructor
public class LocationSearchSpecification implements Specification<Location> {
    private User user;
    private boolean isDeleted;
    private UUID created_by;
    private String collab;

    @Override
    public Predicate toPredicate(Root<Location> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
        List<Predicate> predicates = new ArrayList<>();
        if (this.user != null) {
            predicates.add(criteriaBuilder.equal(root.get("user"), this.user));
        }
        if(created_by != null && collab != null) {
            predicates.add(criteriaBuilder.or(
                    criteriaBuilder.equal(root.get("createdBy"), this.created_by),
                    criteriaBuilder.like(root.get("collabs"), "%"+this.collab+"%")
            ));
        }
        predicates.add(criteriaBuilder.equal(root.get("isDeleted"), this.isDeleted));
        return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
    }
}
