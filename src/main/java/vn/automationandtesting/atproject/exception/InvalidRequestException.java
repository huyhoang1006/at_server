package vn.automationandtesting.atproject.exception;

import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.time.LocalDateTime;

@Getter
@Setter
@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class InvalidRequestException extends RuntimeException {
    private LocalDateTime timestamp;
    private String error;
    private String error_description;
    private HttpStatus httpStatus;

    public InvalidRequestException(LocalDateTime timestamp, String error, String error_description, HttpStatus httpStatus) {
        super(error_description);
        this.timestamp = timestamp;
        this.error = error;
        this.error_description = error_description;
        this.httpStatus = httpStatus;
    }
}