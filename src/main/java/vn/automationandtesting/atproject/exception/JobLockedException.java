package vn.automationandtesting.atproject.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @author tridv on 15/9/2022
 * @project at-project-server
 */
@ResponseStatus(HttpStatus.FORBIDDEN)
public class JobLockedException extends RuntimeException{
    public JobLockedException(String message) {
        super(message);
    }
}
