package vn.automationandtesting.atproject.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @author tund on 3/9/2022
 * @project at-project-server
 */
@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class BushingNotFoundException extends RuntimeException {
    public BushingNotFoundException(String message) {
        super(message);
    }
}
