package vn.automationandtesting.atproject.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class MonitoringNotFoundException extends RuntimeException {
    public MonitoringNotFoundException(String message) {
        super(message);
    }
}
