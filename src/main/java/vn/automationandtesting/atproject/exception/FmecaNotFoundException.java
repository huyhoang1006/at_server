package vn.automationandtesting.atproject.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @author tridv on 19/9/2022
 * @project at-project-server
 */
@ResponseStatus(HttpStatus.BAD_REQUEST)
public class FmecaNotFoundException extends RuntimeException {
    public FmecaNotFoundException(String message) {
        super(message);
    }
}
