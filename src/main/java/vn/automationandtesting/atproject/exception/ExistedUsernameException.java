package vn.automationandtesting.atproject.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(reason = "Username already existed", value = HttpStatus.BAD_REQUEST)
public class ExistedUsernameException extends RuntimeException {
}
